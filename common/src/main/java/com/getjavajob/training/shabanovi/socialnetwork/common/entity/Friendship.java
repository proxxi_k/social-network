package com.getjavajob.training.shabanovi.socialnetwork.common.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "friendship_tbl")
public class Friendship extends Common {

    private int accountId;
    private int friendId;
    private int status;

    public Friendship() {
    }

    public Friendship(int accountId, int friendId, int status) {
        this.accountId = accountId;
        this.friendId = friendId;
        this.status = status;
    }

    public Friendship(int id, int accountId, int friendId, int status) {
        super(id);
        this.accountId = accountId;
        this.friendId = friendId;
        this.status = status;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getFriendId() {
        return friendId;
    }

    public void setFriendId(int friendId) {
        this.friendId = friendId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Friendship)) return false;
        if (!super.equals(o)) return false;
        Friendship that = (Friendship) o;
        return getAccountId() == that.getAccountId() &&
                getFriendId() == that.getFriendId() &&
                getStatus() == that.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAccountId(), getFriendId(), getStatus());
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "id=" + super.getId() +
                ", accountId=" + accountId +
                ", friendId=" + friendId +
                ", status=" + status +
                '}';
    }

}