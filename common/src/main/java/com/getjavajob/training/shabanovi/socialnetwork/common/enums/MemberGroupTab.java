package com.getjavajob.training.shabanovi.socialnetwork.common.enums;

public enum MemberGroupTab {

    ALL_MEMBERS, REQUESTS, USERS, MODERATORS, CREATOR

}