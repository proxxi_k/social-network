package com.getjavajob.training.shabanovi.socialnetwork.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;
import org.hibernate.annotations.WhereJoinTable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "group_tbl")
public class Group extends Common {

    private String name;
    @JsonIgnore
    private String description;
    @JsonIgnore
    @Column(name = "creationDate", updatable = false)
    private Date creationDate;
    @JsonIgnore
    private byte[] image;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creatorId", referencedColumnName = "id", updatable = false)
    private Account creator;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "membership_tbl",
            joinColumns = @JoinColumn(name = "groupId"),
            inverseJoinColumns = @JoinColumn(name = "accountId"))
    @WhereJoinTable(clause = "orderApproved = false")
    private List<Account> orders;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "membership_tbl",
            joinColumns = @JoinColumn(name = "groupId"),
            inverseJoinColumns = @JoinColumn(name = "accountId"))
    @WhereJoinTable(clause = "orderApproved = true AND moderator = false")
    private List<Account> users;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "membership_tbl",
            joinColumns = @JoinColumn(name = "groupId"),
            inverseJoinColumns = @JoinColumn(name = "accountId"))
    @WhereJoinTable(clause = "moderator = true")
    private List<Account> moderators;

    @JsonIgnore
    @OneToMany(mappedBy = "groupId", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @Where(clause = "type = 2")
    private List<Message> wallMessages;

    public Group() {
    }

    public Group(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Group(int id, String name) {
        super(id);
        this.name = name;
    }

    public Group(int id, String name, Date creationDate) {
        super(id);
        this.name = name;
        this.creationDate = creationDate;
    }

    public Group(String name, Date creationDate) {
        this.name = name;
        this.creationDate = creationDate;
    }

    public Group(String name, String description, Date creationDate) {
        this(name, description);
        this.creationDate = creationDate;
    }

    public Group(int id, String name, String description, Date creationDate) {
        super(id);
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
    }

    public Group(int id, String name, String description, Date creationDate, int creatorId) {
        super(id);
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
    }

    public List<Account> getOrders() {
        return orders;
    }

    public void setOrders(List<Account> orders) {
        this.orders = orders;
    }

    public List<Account> getUsers() {
        return users;
    }

    public void setUsers(List<Account> users) {
        this.users = users;
    }

    public List<Account> getModerators() {
        return moderators;
    }

    public void setModerators(List<Account> moderators) {
        this.moderators = moderators;
    }

    public List<Message> getWallMessages() {
        return wallMessages;
    }

    public void setWallMessages(List<Message> wallMessages) {
        this.wallMessages = wallMessages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        if (!super.equals(o)) return false;
        Group group = (Group) o;
        return getName().equals(group.getName()) &&
                Objects.equals(getDescription(), group.getDescription()) &&
                Objects.equals(getCreationDate(), group.getCreationDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getName(), getDescription(), getCreationDate());
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + super.getId() +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", creationDate=" + creationDate +
                ", image=" + Arrays.toString(image) +
                '}';
    }

}