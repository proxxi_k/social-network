package com.getjavajob.training.shabanovi.socialnetwork.common;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.EventType;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class Event {

    private static final AtomicLong generatorUUID = new AtomicLong();

    private long uuid;

    private int senderId;

    private String senderName;

    private String senderSurname;

    private String senderEmail;

    private int recipientId;

    private String recipientName;

    private String recipientSurname;

    private String recipientEmail;

    private Date eventDate;

    private EventType eventType;

    public Event() {
    }

    public Event(Account sender, Account recipient, EventType eventType) {
        this.senderId = sender.getId();
        this.senderName = sender.getName();
        this.senderSurname = sender.getSurname();
        this.senderEmail = sender.getAuth().getEmail();
        this.recipientId = recipient.getId();
        this.recipientName = recipient.getName();
        this.recipientSurname = recipient.getSurname();
        this.recipientEmail = recipient.getAuth().getEmail();
        this.eventType = eventType;
        eventDate = new Date();
        this.uuid = generatorUUID.incrementAndGet();
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderSurname() {
        return senderSurname;
    }

    public void setSenderSurname(String senderSurname) {
        this.senderSurname = senderSurname;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public int getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(int recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public long getUuid() {
        return uuid;
    }

    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    public String getRecipientSurname() {
        return recipientSurname;
    }

    public void setRecipientSurname(String recipientSurname) {
        this.recipientSurname = recipientSurname;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        Event event = (Event) o;
        return getUuid() == event.getUuid() &&
                getSenderId() == event.getSenderId() &&
                getRecipientId() == event.getRecipientId() &&
                getSenderName().equals(event.getSenderName()) &&
                getSenderSurname().equals(event.getSenderSurname()) &&
                getSenderEmail().equals(event.getSenderEmail()) &&
                getRecipientName().equals(event.getRecipientName()) &&
                getRecipientSurname().equals(event.getRecipientSurname()) &&
                getRecipientEmail().equals(event.getRecipientEmail()) &&
                getEventDate().equals(event.getEventDate()) &&
                getEventType() == event.getEventType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUuid(), getSenderId(), getSenderName(), getSenderSurname(), getSenderEmail(),
                getRecipientId(), getRecipientName(), getRecipientSurname(), getRecipientEmail(), getEventDate(),
                getEventType());
    }

    @Override
    public String toString() {
        return "Event{" +
                "uuid=" + uuid +
                ", senderId=" + senderId +
                ", senderName='" + senderName + '\'' +
                ", senderSurname='" + senderSurname + '\'' +
                ", senderEmail='" + senderEmail + '\'' +
                ", recipientId=" + recipientId +
                ", recipientName='" + recipientName + '\'' +
                ", recipientSurname='" + recipientSurname + '\'' +
                ", recipientEmail='" + recipientEmail + '\'' +
                ", eventDate=" + eventDate +
                ", eventType=" + eventType +
                '}';
    }

}