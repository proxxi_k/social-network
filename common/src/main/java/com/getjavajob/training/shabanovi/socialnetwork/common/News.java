package com.getjavajob.training.shabanovi.socialnetwork.common;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Entity
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int friendId;

    private String friendName;

    private String friendSurname;

    private String newsMessage;

    private int newsId;

    private Date newsDate;

    public News() {
    }

    public News(int newsId, Date newsDate) {
        this.newsId = newsId;
        this.newsDate = newsDate;
    }

    public News(int friendId, String friendName, String friendSurname, int newsId, String newsMessage, Date newsDate) {
        this.friendId = friendId;
        this.friendName = friendName;
        this.friendSurname = friendSurname;
        this.newsMessage = newsMessage;
        this.newsId = newsId;
        this.newsDate = newsDate;
    }

    public int getFriendId() {
        return friendId;
    }

    public void setFriendId(int friendId) {
        this.friendId = friendId;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendSurname() {
        return friendSurname;
    }

    public void setFriendSurname(String friendSurname) {
        this.friendSurname = friendSurname;
    }

    public String getNewsMessage() {
        return newsMessage;
    }

    public void setNewsMessage(String newsMessage) {
        this.newsMessage = newsMessage;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public Date getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(Date newsDate) {
        this.newsDate = newsDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof News)) return false;
        News news = (News) o;
        return getFriendId() == news.getFriendId() &&
                getNewsId() == news.getNewsId() &&
                getFriendName().equals(news.getFriendName()) &&
                getFriendSurname().equals(news.getFriendSurname()) &&
                getNewsMessage().equals(news.getNewsMessage()) &&
                getNewsDate().equals(news.getNewsDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFriendId(), getFriendName(), getFriendSurname(), getNewsMessage(), getNewsId(),
                getNewsDate());
    }

    @Override
    public String toString() {
        return "News{" +
                "friendId=" + friendId +
                ", friendName='" + friendName + '\'' +
                ", friendSurname='" + friendSurname + '\'' +
                ", newsMessage='" + newsMessage + '\'' +
                ", newsId=" + newsId +
                ", newsDate=" + newsDate +
                '}';
    }

}