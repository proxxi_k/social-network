package com.getjavajob.training.shabanovi.socialnetwork.common.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "auth_tbl")
public class Auth extends Common implements Serializable {

    private String email;
    private String password;
    private boolean admin;

    @OneToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "accountId", referencedColumnName = "id")
    private Account account;

    public Auth() {
    }

    public Auth(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Auth(int id , String email, String password) {
        super(id);
        this.email = email;
        this.password = password;
    }

    public Auth(int id, String email, String password, boolean admin) {
        this(id, email, password);
        this.admin = admin;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Auth)) return false;
        if (!super.equals(o)) return false;
        Auth that = (Auth) o;
        return getAdmin() == that.getAdmin() &&
                getEmail().equals(that.getEmail()) &&
                getPassword().equals(that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getEmail(), getPassword(), getAdmin());
    }

    @Override
    public String toString() {
        return "Authentication{" +
                "id=" + super.getId() +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", admin=" + admin +
                '}';
    }

}