package com.getjavajob.training.shabanovi.socialnetwork.common.enums;

public enum FriendStatus {

    REQUEST_TO(-1), BLACKLIST(0), REQUEST_FROM(1), FRIEND(2), SOMEONE_BLACKLIST(10);

    int status;

    FriendStatus(int status) {
        this.status = status;
    }

    public int getStatus(){
        return status;
    }

}