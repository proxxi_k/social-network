package com.getjavajob.training.shabanovi.socialnetwork.common.enums;

public enum FriendshipTab {

    FRIENDS, REQUESTS_FROM, REQUESTS_TO, BLACKLISTS

}