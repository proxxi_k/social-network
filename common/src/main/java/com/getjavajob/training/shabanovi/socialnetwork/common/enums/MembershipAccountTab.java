package com.getjavajob.training.shabanovi.socialnetwork.common.enums;

public enum MembershipAccountTab {

    ALL_GROUPS, USER_GROUPS, MODERATOR_GROUPS, CREATED_GROUPS, ORDER_GROUPS

}