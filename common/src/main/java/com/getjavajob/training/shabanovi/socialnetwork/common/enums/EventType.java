package com.getjavajob.training.shabanovi.socialnetwork.common.enums;

public enum EventType {

    FRIENDS_REQUEST, PERSONAL_MESSAGE, ACCOUNT_POST

}