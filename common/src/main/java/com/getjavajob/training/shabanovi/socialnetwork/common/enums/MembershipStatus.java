package com.getjavajob.training.shabanovi.socialnetwork.common.enums;

public enum MembershipStatus {

    REQUEST, USER, MODERATOR, CREATOR;

}