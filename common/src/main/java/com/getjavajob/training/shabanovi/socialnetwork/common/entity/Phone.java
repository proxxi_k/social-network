package com.getjavajob.training.shabanovi.socialnetwork.common.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@XStreamAlias("phone")
@Entity
@Table(name = "phone_tbl")
public class Phone extends Common {

    @XStreamOmitField
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accountId", referencedColumnName = "id")
    private Account account;
    private String type;
    private String number;

    public Phone() {
    }

    public Phone(String type, String number) {
        this.type = type;
        this.number = number;
    }

    public Phone(int id, String type, String number) {
        super(id);
        this.type = type;
        this.number = number;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Phone)) return false;
        if (!super.equals(o)) return false;
        Phone phone = (Phone) o;
        return getType().equals(phone.getType()) &&
                getNumber().equals(phone.getNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getType(), getNumber());
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + super.getId() +
                ", type='" + type + '\'' +
                ", number='" + number + '\'' +
                '}';
    }

}