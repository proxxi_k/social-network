package com.getjavajob.training.shabanovi.socialnetwork.common.enums;

public enum MessageType {

    PERSONAL(0), ACCOUNT_POST(1), GROUP_POST(2);

    int status;

    MessageType(int status) {
        this.status = status;
    }

    public int getStatus(){
        return status;
    }

}