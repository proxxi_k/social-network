package com.getjavajob.training.shabanovi.socialnetwork.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.hibernate.annotations.Where;
import org.hibernate.annotations.WhereJoinTable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@XStreamAlias("account")
@Entity
@Table(name = "account_tbl")
public class Account extends Common implements Serializable {

    @Basic(optional = false)
    private String name;

    private String surname;

    @JsonIgnore
    private String patronymic;

    @JsonIgnore
    private Date birthday;

    @JsonIgnore
    @Column(updatable = false)
    private String homeAddress;

    @JsonIgnore
    private String workAddress;

    @JsonIgnore
    private String skype;

    @JsonIgnore
    private String additionalInfo;

    @XStreamOmitField
    @JsonIgnore
    private Date registrationDate;

    @XStreamOmitField
    @JsonIgnore
    private byte[] avatar;

    @XStreamImplicit(itemFieldName = "phone")
    @JsonIgnore
    @OneToMany(mappedBy = "account", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Phone> phones;

    @XStreamOmitField
    @JsonIgnore
    @OneToOne(mappedBy = "account", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Auth auth;


    @XStreamOmitField
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "friendship_tbl",
            joinColumns = @JoinColumn(name = "accountId"),
            inverseJoinColumns = @JoinColumn(name = "friendId"))
    @WhereJoinTable(clause = "status = 2")
    private List<Account> friends;

    @XStreamOmitField
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "friendship_tbl",
            joinColumns = @JoinColumn(name = "accountId"),
            inverseJoinColumns = @JoinColumn(name = "friendId"))
    @WhereJoinTable(clause = "status = 1")
    private List<Account> requestFrom;

    @XStreamOmitField
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "friendship_tbl",
            joinColumns = @JoinColumn(name = "accountId"),
            inverseJoinColumns = @JoinColumn(name = "friendId"))
    @WhereJoinTable(clause = "status = -1")
    private List<Account> requestTo;

    @XStreamOmitField
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "friendship_tbl",
            joinColumns = @JoinColumn(name = "accountId"),
            inverseJoinColumns = @JoinColumn(name = "friendId"))
    @WhereJoinTable(clause = "status = 0")
    private List<Account> blacklists;

    @XStreamOmitField
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "membership_tbl",
            joinColumns = @JoinColumn(name = "accountId"),
            inverseJoinColumns = @JoinColumn(name = "groupId"))
    @WhereJoinTable(clause = "orderApproved = false")
    private List<Group> requestGroups;

    @XStreamOmitField
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "membership_tbl",
            joinColumns = @JoinColumn(name = "accountId"),
            inverseJoinColumns = @JoinColumn(name = "groupId"))
    @WhereJoinTable(clause = "orderApproved = true AND moderator = false")
    private List<Group> userGroups;

    @XStreamOmitField
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "membership_tbl",
            joinColumns = @JoinColumn(name = "accountId"),
            inverseJoinColumns = @JoinColumn(name = "groupId"))
    @WhereJoinTable(clause = "moderator = true")
    private List<Group> moderatorGroups;

    @XStreamOmitField
    @JsonIgnore
    @OneToMany(mappedBy = "creator", fetch = FetchType.LAZY)
    private List<Group> creatorGroups;

    @XStreamOmitField
    @JsonIgnore
    @OneToMany(mappedBy = "accountId", fetch = FetchType.LAZY)
    @Where(clause = "type = 1")
    private List<Message> wallMessages;

    public Account() {
    }

    public Account(int id, String name, String surname) {
        super(id);
        this.name = name;
        this.surname = surname;
    }

    public Account(int id, String name, String surname, Auth auth) {
        super(id);
        this.name = name;
        this.surname = surname;
        this.auth = auth;
    }

    public Account(String name, String surname, Date registrationDate) {
        this.name = name;
        this.surname = surname;
        this.registrationDate = registrationDate;
    }

    public Account(int id, String name, Date registrationDate) {
        super(id);
        this.name = name;
        this.registrationDate = registrationDate;
    }

    public Account(int id, String name, String surname, Date registrationDate) {
        super(id);
        this.name = name;
        this.surname = surname;
        this.registrationDate = registrationDate;
    }

    public Account(int id, String name, String surname, String patronymic, Date birthday, String homeAddress,
                   String workAddress, String skype, String additionalInfo, Date registrationDate) {
        super(id);
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.birthday = birthday;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
        this.skype = skype;
        this.additionalInfo = additionalInfo;
        this.registrationDate = registrationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public List<Account> getFriends() {
        return friends;
    }

    public void setFriends(List<Account> friends) {
        this.friends = friends;
    }

    public List<Account> getRequestFrom() {
        return requestFrom;
    }

    public void setRequestFrom(List<Account> ordersFrom) {
        this.requestFrom = ordersFrom;
    }

    public List<Account> getRequestTo() {
        return requestTo;
    }

    public void setRequestTo(List<Account> ordersTo) {
        this.requestTo = ordersTo;
    }

    public List<Account> getBlacklists() {
        return blacklists;
    }

    public void setBlacklists(List<Account> blacklists) {
        this.blacklists = blacklists;
    }

    public List<Group> getRequestGroups() {
        return requestGroups;
    }

    public void setRequestGroups(List<Group> orderGroups) {
        this.requestGroups = orderGroups;
    }

    public List<Group> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<Group> userGroups) {
        this.userGroups = userGroups;
    }

    public List<Group> getModeratorGroups() {
        return moderatorGroups;
    }

    public void setModeratorGroups(List<Group> moderatorGroups) {
        this.moderatorGroups = moderatorGroups;
    }

    public List<Group> getCreatorGroups() {
        return creatorGroups;
    }

    public void setCreatorGroups(List<Group> creatorGroups) {
        this.creatorGroups = creatorGroups;
    }

    public List<Message> getWallMessages() {
        return wallMessages;
    }

    public void setWallMessages(List<Message> wallMessage) {
        this.wallMessages = wallMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        if (!super.equals(o)) return false;
        Account account = (Account) o;
        return getName().equals(account.getName()) &&
                Objects.equals(getSurname(), account.getSurname()) &&
                Objects.equals(getPatronymic(), account.getPatronymic()) &&
                Objects.equals(getBirthday(), account.getBirthday()) &&
                Objects.equals(getHomeAddress(), account.getHomeAddress()) &&
                Objects.equals(getWorkAddress(), account.getWorkAddress()) &&
                Objects.equals(getSkype(), account.getSkype()) &&
                Objects.equals(getAdditionalInfo(), account.getAdditionalInfo()) &&
                Objects.equals(getRegistrationDate(), account.getRegistrationDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getName(), getSurname(), getPatronymic(), getBirthday(),
                getHomeAddress(), getWorkAddress(), getSkype(), getAdditionalInfo(), getRegistrationDate());
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + super.getId() +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birthday=" + birthday +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", skype='" + skype + '\'' +
                ", additionalInfo='" + additionalInfo + '\'' +
                ", registrationDate=" + registrationDate +
                '}';
    }

}