package com.getjavajob.training.shabanovi.socialnetwork.common.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "message_tbl")
public class Message extends Common {

    @ManyToOne
    @JoinColumn(name = "senderId", referencedColumnName = "id")
    private Account sender;
    private Integer accountId;
    private Integer groupId;
    private byte[] image;
    private String message;
    private Timestamp date;
    private int type;

    public Message() {
    }

    public Message(Integer accountId, Integer groupId, String message) {
        this.accountId = accountId;
        this.groupId = groupId;
        this.message = message;
    }

    public Message(Integer accountId, Integer groupId, String message, Timestamp date, int type) {
        this(accountId, groupId, message);
        this.date = date;
        this.type = type;
    }

    public Message(int id, Integer accountId, Integer groupId, String message, Timestamp date, int type) {
        super(id);
        this.accountId = accountId;
        this.groupId = groupId;
        this.message = message;
        this.date = date;
        this.type = type;
    }



    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        if (!super.equals(o)) return false;
        Message message1 = (Message) o;
        return getType() == message1.getType() &&
                Objects.equals(getAccountId(), message1.getAccountId()) &&
                Objects.equals(getGroupId(), message1.getGroupId()) &&
                getMessage().equals(message1.getMessage()) &&
                getDate().equals(message1.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAccountId(), getGroupId(), getMessage(), getDate(),
                getType());
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + super.getId() +
                ", accountId=" + accountId +
                ", groupId=" + groupId +
                ", message='" + message + '\'' +
                ", date=" + date +
                ", type=" + type +
                '}';
    }
}