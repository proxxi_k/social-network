package com.getjavajob.training.shabanovi.socialnetwork.common.enums;

public enum InternetPage {

    ACCOUNT_PAGE, FRIENDSHIP_ACCOUNT_PAGE, MEMBERSHIP_ACCOUNT_PAGE, GROUP_PAGE, MEMBER_GROUP_PAGE

}