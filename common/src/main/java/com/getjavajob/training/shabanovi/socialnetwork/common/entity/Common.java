package com.getjavajob.training.shabanovi.socialnetwork.common.entity;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

@MappedSuperclass
public abstract class Common implements Serializable {

    @XStreamAsAttribute
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public Common() {
    }

    public Common(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Common)) return false;
        Common common = (Common) o;
        return getId() == common.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}