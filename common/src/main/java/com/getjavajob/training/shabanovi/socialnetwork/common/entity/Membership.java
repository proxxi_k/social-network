package com.getjavajob.training.shabanovi.socialnetwork.common.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "membership_tbl")
public class Membership extends Common {

    private int accountId;
    private int groupId;
    private boolean orderApproved;
    private boolean moderator;

    public Membership() {
    }

    public Membership(int accountId, int groupId, boolean orderApproved, boolean moderator) {
        this.accountId = accountId;
        this.groupId = groupId;
        this.orderApproved = orderApproved;
        this.moderator = moderator;
    }

    public Membership(int id, int accountId, int groupId, boolean orderApproved, boolean moderator) {
        super(id);
        this.accountId = accountId;
        this.groupId = groupId;
        this.orderApproved = orderApproved;
        this.moderator = moderator;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public boolean getOrderApproved() {
        return orderApproved;
    }

    public void setOrderApproved(boolean orderApproved) {
        this.orderApproved = orderApproved;
    }

    public boolean getModerator() {
        return moderator;
    }

    public void setModerator(boolean moderator) {
        this.moderator = moderator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Membership)) return false;
        if (!super.equals(o)) return false;
        Membership that = (Membership) o;
        return getAccountId() == that.getAccountId() &&
                getGroupId() == that.getGroupId() &&
                getOrderApproved() == that.getOrderApproved() &&
                getModerator() == that.getModerator();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAccountId(), getGroupId(), getOrderApproved(), getModerator());
    }

    @Override
    public String toString() {
        return "Membership{" +
                "id=" + super.getId() +
                ", accountId=" + accountId +
                ", groupId=" + groupId +
                ", orderApproved=" + orderApproved +
                ", admin=" + moderator +
                '}';
    }

}