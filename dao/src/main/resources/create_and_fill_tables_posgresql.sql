-- PostgreSQL

-- account_tbl

CREATE TABLE IF NOT EXISTS account_tbl
(
    id               SERIAL PRIMARY KEY,
    name             VARCHAR(45) DEFAULT 'Неизвестный',
    surname          VARCHAR(45),
    patronymic       VARCHAR(45),
    birthday         DATE,
    homeAddress      VARCHAR(45),
    workAddress      VARCHAR(45),
    skype            VARCHAR(32),
    additionalInfo   VARCHAR(1000),
    registrationDate DATE NOT NULL,
    avatar           BYTEA
);

INSERT INTO account_tbl (name, surname, registrationDate)
VALUES ('Иван', 'Иванов', '2021/01/01'),
       ('Петр', 'Петров', '2021/02/01'),
       ('Сергей', 'Сергеев', '2021/03/01');

INSERT INTO account_tbl (name, surname, patronymic, birthday, homeAddress, workAddress, skype, additionalInfo,
                         registrationDate)
VALUES ('Владимир', 'Банкетный', 'Анатольевич', '1999/01/01', 'г. Москва, ул. Есенина, д. Каруселина',
        'г. Москва, ул. Паркетная, д. 1', 'banketnyj',
        'А сегодня в завтрашний день, не все могут смотреть. Вернее смотреть могут не только лишь все, не каждый
может это делать', '2021/04/01');

INSERT INTO account_tbl (name, surname, registrationDate)
VALUES ('Александр', 'Александров', '2021/04/01'),
       ('Мария', 'Мариева', '2021/06/01'),
       ('Валера', 'Валеров', '2021/07/01'),
       ('Максим', 'Максимов', '2021/08/01');

INSERT INTO account_tbl (registrationDate)
VALUES ('2021/09/01');

-- auth_tbl

CREATE TABLE IF NOT EXISTS auth_tbl
(
    id        SERIAL PRIMARY KEY,
    accountId INT          NOT NULL,
    email     VARCHAR(255) NOT NULL,
    password  VARCHAR(255) NOT NULL,
    admin     BOOLEAN      NOT NULL DEFAULT FALSE,
    FOREIGN KEY (accountId) REFERENCES account_tbl (id),
    UNIQUE (email)
);

INSERT INTO auth_tbl (accountId, email, password)
VALUES (1, 'ivanov@yandex.ru', '$2a$12$vR9AlBvziQaf1Hv6BlL.9OUT2a0zHSGWKaSlZRzmbGnMjKXMXhQ2K'),
       (2, 'petrov@mail.ru', '$2a$12$mudcp4Yw5mlOxeIyRpMWSO0iz5xPKtMZKuDZaYLtGB0DvZbxFCiEm');

INSERT INTO auth_tbl (accountId, email, password, admin)
VALUES (3, 'serg@gmail.com', '$2a$12$P.JRyBDyYlnasTLbmCyoTuQcZ75yKyOJHqqvNfUCbcLK99.QmEIOa', true);

INSERT INTO auth_tbl (accountId, email, password)
VALUES (4, 'banketnyj@mail.ru', '$2a$12$OWJYKqY6YSmOZtZaBba7a.klg8taVfbzoPm2LuLlyWqWQJ.O9fS5S'),
       (5, 'aleksandrov@yandex.ru', '$2a$12$llMlx0b83kQjHkKAvMcoDu1ncQLtUAJ2CYkmqDELYCYtxLtr5duV2'),
       (6, 'marieva@gmail.com', '$2a$12$.UPz61QBdspVRWrLiNPaRu2.RD29E9cfBgzuxr3OK/s8LPNHRdv7a'),
       (7, 'valerov@mail.ru', '$2a$12$bWMkAQIdaAV9og0zgMyyiuDz8NTOhCmupWRwf31o/ZGCOGw9dqrp6'),
       (8, 'mksimov@yandex.ru', '$2a$12$OElTFt7RedV2oohqm68xv..GjZFOz5O/SGhFKU4oZ9.9ja6lP0VBy'),
       (9, 'unknown@yandex.ru', '$2a$12$tY0fmToSJ4HaauupT5IxQelM3wNJgXcOpv2fipOz3u1ytrWsQHqA6');

-- phone_tbl

CREATE TABLE IF NOT EXISTS phone_tbl
(
    id        SERIAL PRIMARY KEY,
    accountId INT         NOT NULL,
    type      VARCHAR(20) NOT NULL,
    number    VARCHAR(18) NOT NULL,
    FOREIGN KEY (accountId) REFERENCES account_tbl (id)
);

INSERT INTO phone_tbl (accountId, type, number)
VALUES (1, 'личный', '+79991111111'),
       (2, 'рабочий', '+79992222222'),
       (3, 'личный', '+79993333333'),
       (3, 'рабочий', '+79994444444'),
       (3, 'домашний', '+79995555555');

-- friendship_tbl
-- 0 - blacklist
-- 1 - order
-- 2 - blacklist

CREATE TABLE IF NOT EXISTS friendship_tbl
(
    id        SERIAL PRIMARY KEY,
    accountId INT NOT NULL,
    friendId  INT NOT NULL,
    status    INT NOT NULL,
    UNIQUE (accountId, friendId),
    FOREIGN KEY (accountId) REFERENCES account_tbl (id),
    FOREIGN KEY (friendId) REFERENCES account_tbl (id)
);

INSERT INTO friendship_tbl (accountId, friendId, status)
VALUES (1, 3, 1),
       (3, 1, -1),
       (2, 3, -1),
       (3, 2, 1),
       (3, 4, 1),
       (4, 3, -1),
       (3, 5, -1),
       (5, 3, 1),
       (3, 6, 2),
       (6, 3, 2),
       (3, 7, 0);

-- group_tbl

CREATE TABLE IF NOT EXISTS group_tbl
(
    id           SERIAL PRIMARY KEY,
    name         VARCHAR(45) NOT NULL,
    description  VARCHAR(1000),
    creationDate DATE        NOT NULL,
    creatorId    INT         NOT NULL,
    image        BYTEA,
    FOREIGN KEY (creatorId) REFERENCES account_tbl (id),
    UNIQUE (name)
);

INSERT INTO group_tbl (name, description, creationDate, creatorId, image)
VALUES ('Группа №1', 'лучшая группа', '2021/01/02', 3, NULL),
       ('Группа №2', 'классная группа', '2021/02/02', 1, NULL),
       ('Группа №3', NULL, '2021/03/02', 1, NULL),
       ('Группа №4', NULL, '2021/04/02', 1, NULL),
       ('Группа №5', NULL, '2021/05/02', 3, NULL),
       ('Группа №6', NULL, '2021/06/02', 1, NULL);

-- membership_tbl

CREATE TABLE IF NOT EXISTS membership_tbl
(
    id            SERIAL PRIMARY KEY,
    accountId     INT     NOT NULL,
    groupId       INT     NOT NULL,
    orderApproved BOOLEAN NOT NULL DEFAULT FALSE,
    moderator     BOOLEAN NOT NULL DEFAULT FALSE,
    FOREIGN KEY (accountId) REFERENCES account_tbl (id),
    FOREIGN KEY (groupId) REFERENCES group_tbl (id),
    UNIQUE (accountId, groupId)
);

INSERT INTO membership_tbl (accountId, groupId, orderApproved, moderator)
VALUES (3, 2, TRUE, TRUE),
       (3, 3, FALSE, FALSE),
       (3, 4, TRUE, FALSE),
       (3, 6, TRUE, TRUE),
       (1, 5, TRUE, TRUE),
       (1, 1, TRUE, TRUE),
       (2, 1, TRUE, FALSE),
       (4, 1, FALSE, FALSE);

-- message_tbl
-- type:
-- 0 - personal
-- 1 - account
-- 2 - group

CREATE TABLE IF NOT EXISTS message_tbl
(
    id        SERIAL PRIMARY KEY,
    senderId  INT       NOT NULL,
    accountId INT,
    groupId   INT,
    image     BYTEA,
    message   VARCHAR(1000),
    date      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    type      SMALLINT  NOT NULL,
    FOREIGN KEY (senderId) REFERENCES account_tbl (id),
    FOREIGN KEY (accountId) REFERENCES account_tbl (id),
    FOREIGN KEY (groupId) REFERENCES group_tbl (id)
);

INSERT INTO message_tbl (senderId, accountId, groupId, image, message, date, type)
VALUES (1, 2, NULL, NULL, 'Привет!', '2021/03/02 13:13:13', 0),
       (2, 1, NULL, NULL, 'Ну привет!', '2021/03/02 14:14:14', 0),
       (1, 2, NULL, NULL, 'Как дела?', '2021/03/02 15:14:14', 0),
       (3, 1, NULL, NULL, 'Привет! Добавь в друзья!', '2021/03/02 15:15:15', 0),
       (1, 4, NULL, NULL, 'Оооо, а я тебя знаю)!', '2021/03/02 16:16:16', 0),
       (1, 1, NULL, NULL, 'Дела отлично!', '2021/03/02 13:13:13', 1),
       (1, 1, NULL, NULL, 'И сегодня тоже!', '2021/04/03 14:14:14', 1),
       (1, NULL, 1, NULL, 'Группа огонь!', '2021/03/02 13:13:13', 2),
       (2, NULL, 1, NULL, 'Мне тоже нравится!', '2021/03/02 14:14:14', 2),
       (2, 1, NULL, NULL, 'Как дела?', '2021/03/01 14:14:14', 1),
       (1, 3, NULL, NULL, 'Что нового?', '2021/03/01 15:15:15', 1),
       (3, 3, NULL, NULL, 'Все по старому', '2021/03/01 16:16:16', 1);