-- PostgreSQL

DROP TABLE IF EXISTS friendship_tbl;
DROP TABLE IF EXISTS membership_tbl;
DROP TABLE IF EXISTS message_tbl;
DROP TABLE IF EXISTS group_tbl;
DROP TABLE IF EXISTS phone_tbl;
DROP TABLE IF EXISTS auth_tbl;
DROP TABLE IF EXISTS account_tbl;