package com.getjavajob.training.shabanovi.socialnetwork.dao.data;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AuthDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Primary
@Repository
public class AuthDaoSpringData extends AbstractDaoSpringData<Auth> implements AuthDao {

    private final AuthRepository authRepository;

    @Autowired
    public AuthDaoSpringData(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    @Override
    public Auth getByEmail(String email) {
        return authRepository.getByEmail(email);
    }

    @Override
    public String getEmailByAccountId(int accountId) {
        return authRepository.getEmailByAccountId(accountId);
    }

}