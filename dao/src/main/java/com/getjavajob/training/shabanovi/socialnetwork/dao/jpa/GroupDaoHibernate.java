package com.getjavajob.training.shabanovi.socialnetwork.dao.jpa;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.GroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class GroupDaoHibernate extends AbstractDaoHibernate<Group> implements GroupDao {

    @Autowired
    public GroupDaoHibernate() {
        setEntityClass(Group.class);
    }

    @Override
    public int getAllGroupsQty() {
        return 0;
    }

    @Override
    public byte[] getImageById(int id) {
        List<byte[]> images = entityManager.createQuery(
                "SELECT g.image FROM Group g " +
                        "WHERE g.id=:id", byte[].class)
                .setParameter("id", id)
                .getResultList();
        return images.isEmpty() ? null : images.get(0);
    }

    @Override
    public Group getByName(String name) {
        try {
            return entityManager.createQuery(
                    "SELECT g " +
                            "FROM Group g " +
                            "WHERE g.name=:name", Group.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Group getWithCreatorById(int id) {
        return null;
    }

    @Override
    public List<Group> getCreatedGroups(int accountId) {
        return null;
    }

    @Override
    public int getCountCreatedGroups(int accountId) {
        return 0;
    }

    @Override
    public List<Integer> getCreatedGroupsIdByAccountId(int accountId) {
        return entityManager.createQuery(
                "SELECT g.id FROM Group g " +
                        "WHERE g.creator.id=:accountId", Integer.class)
                .setParameter("accountId", accountId)
                .getResultList();
    }

    @Override
    public int updateCreator(int newCreatorId, int groupId) {
        return entityManager.createQuery(
                "UPDATE Group g " +
                        "SET g.creator.id=:creatorId " +
                        "WHERE g.id=:groupId")
                .setParameter("groupId", groupId)
                .setParameter("creatorId", newCreatorId)
                .executeUpdate();
    }

    @Override
    public List<Group> searchByName(String value, int numPage) {
        return entityManager.createQuery(
                "SELECT g FROM Group g " +
                        "WHERE LOWER(g.name) LIKE :likeValue " +
                        "ORDER BY g.name", Group.class)
                .setParameter("likeValue", "%" + value.toLowerCase() + "%")
                .setFirstResult(numPage * 5)
                .setMaxResults(6)
                .getResultList();
    }

    @Override
    public int getSearchGroupsQty(String value) {
        return 0;
    }

}