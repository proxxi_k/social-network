package com.getjavajob.training.shabanovi.socialnetwork.dao.data;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.GroupDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;

@Primary
@Repository
public class GroupDaoSpringData extends AbstractDaoSpringData<Group> implements GroupDao {

    private final GroupRepository groupRepository;

    @Autowired
    public GroupDaoSpringData(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public int getAllGroupsQty() {
        return groupRepository.getAllGroupsQty();
    }

    @Override
    public byte[] getImageById(int id) {
        return groupRepository.getImageById(id);
    }

    @Override
    public Group getByName(String name) {
        return groupRepository.getByName(name);
    }

    @Override
    public Group getWithCreatorById(int id) {
        return groupRepository.getWithCreatorById(id);
    }

    @Override
    public List<Group> getCreatedGroups(int accountId) {
        return groupRepository.getCreatedGroups(accountId);
    }

    @Override
    public int getCountCreatedGroups(int accountId) {
        return groupRepository.getCountCreatedGroups(accountId);
    }

    @Override
    public Group update(Group group) {
        groupRepository.update(group);
        return group;
    }

    @Override
    public List<Integer> getCreatedGroupsIdByAccountId(int accountId) {
        return groupRepository.getCreatedGroupsIdByAccountId(accountId);
    }

    @Override
    public int updateCreator(int newCreatorId, int groupId) {
        return groupRepository.updateCreator(newCreatorId, groupId);
    }

    @Override
    public List<Group> searchByName(String value, int numPage) {
        return groupRepository.searchByPartNameWithPagination(value, PageRequest.of(numPage, 5, Sort.by("name")));
    }

    @Override
    public int getSearchGroupsQty(String value) {
        return groupRepository.getSearchGroupsQty(value);
    }

}