package com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Membership;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus;

import java.util.List;

public interface MembershipDao extends GenericDao<Membership> {

    Membership getMembership(int accountId, int groupId);

    int updateModeratorStatus(int accountId, int groupId, MembershipStatus membershipStatus);

    int deleteMembership(int accountId, int groupId);

    List<Account> getRequests(int groupId);

    List<Account> getUsers(int groupId);

    List<Account> getModerators(int groupId);

    List<Group> getRequestGroups(int accountId);

    List<Group> getUserGroups(int accountId);

    List<Group> getModeratorGroups(int accountId);

}