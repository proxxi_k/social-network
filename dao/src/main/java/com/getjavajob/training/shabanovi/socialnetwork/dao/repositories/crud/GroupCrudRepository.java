package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.crud;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;

public interface GroupCrudRepository extends GenericCrudRepository<Group> {
}