package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends CrudRepository<Auth, Integer> {

    Auth getByEmail(String email);

    @Query("SELECT a.email FROM Auth a WHERE a.account.id=:accountId")
    String getEmailByAccountId(@Param("accountId") int accountId);

}