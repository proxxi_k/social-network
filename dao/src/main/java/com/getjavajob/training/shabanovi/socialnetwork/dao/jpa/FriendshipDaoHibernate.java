package com.getjavajob.training.shabanovi.socialnetwork.dao.jpa;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Friendship;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.FriendshipDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class FriendshipDaoHibernate extends AbstractDaoHibernate<Friendship> implements FriendshipDao {

    @Autowired
    public FriendshipDaoHibernate() {
        setEntityClass(Friendship.class);
    }

    @Override
    public Friendship getFriendship(int accountId, int friendId) {
        try {
            return entityManager.createQuery(
                    "SELECT f FROM Friendship f " +
                            "WHERE f.accountId=:accountId AND f.friendId=:friendId", Friendship.class)
                    .setParameter("accountId", accountId)
                    .setParameter("friendId", friendId)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public int updateStatus(int accountId, int friendId, FriendStatus friendStatus) {
        return entityManager.createQuery(
                "UPDATE Friendship f SET f.status=:status " +
                        "WHERE f.accountId=:accountId AND f.friendId=:friendId " +
                        "OR f.accountId=:friendId AND f.friendId=:accountId")
                .setParameter("status", friendStatus.getStatus())
                .setParameter("accountId", accountId)
                .setParameter("friendId", friendId)
                .executeUpdate();
    }

    @Override
    public int deleteFriendshipsByAccountId(int accountId) {
        return entityManager.createQuery(
                "DELETE FROM Friendship f " +
                        "WHERE f.accountId=:accountId " +
                        "OR f.friendId=:accountId")
                .setParameter("accountId", accountId)
                .executeUpdate();
    }

    @Override
    public int deleteFriendship(int accountId, int friendId) {
        return entityManager.createQuery(
                "DELETE FROM Friendship f " +
                        "WHERE f.accountId=:accountId AND f.friendId=:friendId OR f.accountId=:friendId AND " +
                        "f.friendId=:accountId")
                .setParameter("accountId", accountId)
                .setParameter("friendId", friendId)
                .executeUpdate();
    }

    @Override
    public int getFriendsQty(int accountId) {
        return 0;
    }

    @Override
    public int getRequestsFromQty(int accountId) {
        return 0;
    }

    @Override
    public int getRequestsToQty(int accountId) {
        return 0;
    }

    @Override
    public int getBlacklistsQty(int accountId) {
        return 0;
    }

    @Override
    public List<Account> getFriends(int accountId, int numPage) {
        return null;
    }

    @Override
    public List<Integer> getFriendsId(int accountId) {
        return null;
    }

    @Override
    public List<Account> getRequestsFrom(int accountId, int numPage) {
        return null;
    }

    @Override
    public List<Account> getRequestsTo(int accountId, int numPage) {
        return null;
    }

    @Override
    public List<Account> getBlacklists(int accountId, int numPage) {
        return null;
    }

}