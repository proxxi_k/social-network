package com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Friendship;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus;

import java.util.List;

public interface FriendshipDao extends GenericDao<Friendship> {

    Friendship getFriendship(int accountId, int friendId);

    int updateStatus(int accountId, int friendId, FriendStatus friendStatus);

    int deleteFriendshipsByAccountId(int accountId);

    int deleteFriendship(int accountId, int friendId);

    int getFriendsQty(int accountId);

    int getRequestsFromQty(int accountId);

    int getRequestsToQty(int accountId);

    int getBlacklistsQty(int accountId);

    List<Account> getFriends(int accountId, int numPage);

    List<Integer> getFriendsId (int accountId);

    List<Account> getRequestsFrom(int accountId, int numPage);

    List<Account> getRequestsTo(int accountId, int numPage);

    List<Account> getBlacklists(int accountId, int numPage);

}