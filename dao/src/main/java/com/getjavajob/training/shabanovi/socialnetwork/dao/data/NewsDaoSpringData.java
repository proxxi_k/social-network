package com.getjavajob.training.shabanovi.socialnetwork.dao.data;

import com.getjavajob.training.shabanovi.socialnetwork.common.News;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.NewsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Repository
public class NewsDaoSpringData implements NewsDao {

    private final EntityManager entityManager;

    @Autowired
    public NewsDaoSpringData(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<News> getByCollectionNewsId(Collection<Integer> newsId) {
        return entityManager.createQuery(
                "SELECT new News(a.id, a.name, a.surname, m.id, m.message, m.date) FROM Message m " +
                        "LEFT JOIN Account a ON a.id = m.accountId " +
                        "WHERE m.id IN (:newsId)  " +
                        "ORDER BY m.date DESC", News.class)
                .setParameter("newsId", newsId)
                .getResultList();
    }

    @Override
    public List<News> getByAccountId(int accountId, int numPage) {
        return entityManager.createQuery(
                "SELECT new News(a.id, a.name, a.surname, m.id, m.message, m.date) FROM Message m " +
                        "LEFT JOIN Account a ON a.id = m.accountId " +
                        "WHERE m.type = 1 AND m.sender.id = m.accountId AND m.sender.id IN " +
                        "(SELECT f.friendId FROM Friendship f " +
                        "LEFT JOIN Account a ON a.id = f.accountId " +
                        "WHERE a.id =:accountId AND f.status = 2)" +
                        "ORDER BY m.date DESC", News.class)
                .setParameter("accountId", accountId)
                .setFirstResult(numPage * 5)
                .setMaxResults(5)
                .getResultList();
    }

    @Override
    public List<News> getCacheByAccountId(int accountId, int sizeCache) {
        return entityManager.createQuery(
                "SELECT new News(m.id, m.date) FROM Message m " +
                        "WHERE m.type = 1 AND m.sender.id = m.accountId AND m.sender.id IN " +
                        "(SELECT f.friendId FROM Friendship f " +
                        "LEFT JOIN Account a ON a.id = f.accountId " +
                        "WHERE a.id =:accountId AND f.status = 2)" +
                        "ORDER BY m.date DESC", News.class)
                .setParameter("accountId", accountId)
                .setMaxResults(sizeCache)
                .getResultList();
    }

    @Override
    public Long getByAccountIdQty(int accountId) {
        return entityManager.createQuery(
                "SELECT COUNT(m) FROM Message m " +
                        "LEFT JOIN Account a ON a.id = m.accountId " +
                        "WHERE m.type = 1 AND m.sender.id = m.accountId AND m.sender.id IN " +
                        "(SELECT f.friendId FROM Friendship f " +
                        "LEFT JOIN Account a ON a.id = f.accountId " +
                        "WHERE a.id =:accountId AND f.status = 2)", Long.class)
                .setParameter("accountId", accountId)
                .getSingleResult();
    }

}