package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.crud;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Friendship;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendshipCrudRepository extends GenericCrudRepository<Friendship> {
}