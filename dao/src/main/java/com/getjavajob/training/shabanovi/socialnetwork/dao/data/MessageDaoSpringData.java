package com.getjavajob.training.shabanovi.socialnetwork.dao.data;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MessageDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;

@Primary
@Repository
public class MessageDaoSpringData extends AbstractDaoSpringData<Message> implements MessageDao {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageDaoSpringData(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public List<Message> getPersonalMessages(int accountId, int interlocutorId) {
        return messageRepository.getPersonalMessages(accountId, interlocutorId, Sort.by("date"));
    }

    @Override
    public List<Message> getAccountWallMessages(int accountId) {
        return messageRepository.getAccountWallMessages(accountId);
    }

    @Override
    public List<Message> getGroupWallMessages(int groupId) {
        return messageRepository.getGroupWallMessages(groupId);
    }

    @Override
    public List<Account> getAccountsInterlocutors(int accountId) {
        return messageRepository.getAccountsInterlocutors(accountId, Sort.by("surname", "name", "id"));
    }

    @Override
    public int deletePersonalMessages(int senderId, int accountId) {
        return messageRepository.deletePersonalMessages(senderId, accountId);
    }

    @Override
    public int deleteAllMessagesByAccountId(int accountId) {
        return messageRepository.deleteAllMessagesByAccountId(accountId);
    }

}