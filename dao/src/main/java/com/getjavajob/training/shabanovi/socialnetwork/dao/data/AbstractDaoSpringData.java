package com.getjavajob.training.shabanovi.socialnetwork.dao.data;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Common;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.GenericDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.crud.GenericCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractDaoSpringData<T extends Common> implements GenericDao<T> {

    private GenericCrudRepository<T> genericRepository;

    @Autowired
    public void setGenericRepository(GenericCrudRepository<T> genericRepository) {
        this.genericRepository = genericRepository;
    }

    @Override
    public T create(T entity) {
        return genericRepository.save(entity);
    }

    @Override
    public List<T> getAll(int numPage) {
        String simpleNameClass =
                ((Class<?>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getSimpleName();
        String[] sortParameters;
        // for Accounts
        if (simpleNameClass.equals(Account.class.getSimpleName())) {
            sortParameters = new String[]{"surname", "name", "id"};
            //for Groups
        } else if (simpleNameClass.equals(Group.class.getSimpleName())) {
            sortParameters = new String[]{"name"};
        } else {
            sortParameters = new String[]{"id"};
        }
        return genericRepository.findAll(PageRequest.of(numPage, 5, Sort.by(sortParameters))).getContent();
    }

    @Override
    public T getById(int id) {
        return genericRepository.findById(id).orElse(null);
    }

    @Override
    public T update(T entity) {
        return genericRepository.save(entity);
    }

    @Override
    public void deleteById(int id) {
        genericRepository.deleteById(id);
    }

}