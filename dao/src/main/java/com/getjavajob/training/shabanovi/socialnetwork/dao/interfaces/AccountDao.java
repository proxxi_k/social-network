package com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;

import java.util.List;

public interface AccountDao extends GenericDao<Account> {

    int getAllAccountsQty();

    Account getWithPhonesById(int id);

    Account getByEmail(String email);

    byte[] getAvatarById(int id);

    int getSearchAccountsQty(String value);

    List<Account> searchBySurnameOrName(String value, int numPage);

}