package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Friendship;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendshipRepository extends CrudRepository<Friendship, Integer> {

    @Query("SELECT f FROM Friendship f " +
            "WHERE f.accountId=:accountId AND f.friendId=:friendId")
    Friendship getFriendship(@Param("accountId") int accountId, @Param("friendId") int friendId);

    @Modifying
    @Query("UPDATE Friendship f SET f.status=:status " +
            "WHERE f.accountId=:accountId AND f.friendId=:friendId OR f.accountId=:friendId AND f.friendId=:accountId")
    int updateStatus(@Param("accountId") int accountId, @Param("friendId") int friendId,
                     @Param("status") int status);

    @Modifying
    @Query("DELETE FROM Friendship f WHERE f.accountId=:accountId OR f.friendId=:accountId")
    int deleteByAccountId(@Param("accountId") int accountId);

    @Modifying
    @Query("DELETE FROM Friendship f " +
            "WHERE f.accountId=:accountId AND f.friendId=:friendId OR " +
            "f.accountId=:friendId AND f.friendId=:accountId")
    int deleteFriendship(@Param("accountId") int accountId, @Param("friendId") int friendId);

    @Query("SELECT COUNT(a) FROM Account a LEFT JOIN  Friendship f ON f.friendId = a.id " +
            "WHERE f.accountId=:accountId AND f.status = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus).FRIEND.getStatus()}")
    int getFriendsQty(@Param("accountId") int accountId);

    @Query("SELECT COUNT(a) FROM Account a LEFT JOIN  Friendship f ON f.friendId = a.id " +
            "WHERE f.accountId=:accountId AND f.status = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus).REQUEST_FROM.getStatus()}")
    int getRequestsFromQty(@Param("accountId") int accountId);

    @Query("SELECT COUNT(a) FROM Account a LEFT JOIN  Friendship f ON f.friendId = a.id " +
            "WHERE f.accountId=:accountId AND f.status = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus).REQUEST_TO.getStatus()}")
    int getRequestsToQty(@Param("accountId") int accountId);

    @Query("SELECT COUNT(a) FROM Account a LEFT JOIN  Friendship f ON f.friendId = a.id " +
            "WHERE f.accountId=:accountId AND f.status = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus).BLACKLIST.getStatus()}")
    int getBlacklistsQty(@Param("accountId") int accountId);

    @Query("SELECT a FROM Account a LEFT JOIN  Friendship f ON f.friendId = a.id " +
            "WHERE f.accountId=:accountId AND f.status = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus).FRIEND.getStatus()}")
    List<Account> getFriends(@Param("accountId") int accountId, Pageable pageable);

    @Query("SELECT a.id FROM Account a LEFT JOIN  Friendship f ON f.friendId = a.id " +
            "WHERE f.accountId=:accountId AND f.status = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus).FRIEND.getStatus()}")
    List<Integer> getFriendsId(@Param("accountId") int accountId);

    @Query("SELECT a FROM Account a LEFT JOIN Friendship f ON f.friendId = a.id " +
            "WHERE f.accountId=:accountId AND f.status = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus).REQUEST_FROM.getStatus()}")
    List<Account> getRequestsFrom(@Param("accountId") int accountId, Pageable pageable);

    @Query("SELECT a FROM Account a LEFT JOIN  Friendship f ON f.friendId = a.id " +
            "WHERE f.accountId=:accountId AND f.status = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus).REQUEST_TO.getStatus()}")
    List<Account> getRequestsTo(@Param("accountId") int accountId, Pageable pageable);

    @Query("SELECT a FROM Account a LEFT JOIN  Friendship f ON f.friendId = a.id " +
            "WHERE f.accountId=:accountId AND f.status = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus).BLACKLIST.getStatus()}")
    List<Account> getBlacklists(@Param("accountId") int accountId, Pageable pageable);

}