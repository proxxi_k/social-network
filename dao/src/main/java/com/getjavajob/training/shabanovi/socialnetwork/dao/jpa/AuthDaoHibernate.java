package com.getjavajob.training.shabanovi.socialnetwork.dao.jpa;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AuthDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class AuthDaoHibernate extends AbstractDaoHibernate<Auth> implements AuthDao {

    @Autowired
    public AuthDaoHibernate() {
        setEntityClass(Auth.class);
    }

    @Override
    public Auth getByEmail(String email) {
        try {
            return entityManager.createQuery(
                    "SELECT a FROM Auth a " +
                            "WHERE a.email=:email", Auth.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public String getEmailByAccountId(int accountId) {
        try {
            return entityManager.createQuery(
                    "SELECT a.email FROM Auth a " +
                            "WHERE a.account.id=:accountId", String.class)
                    .setParameter("accountId", accountId)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}