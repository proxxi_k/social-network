package com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces;

import com.getjavajob.training.shabanovi.socialnetwork.common.News;

import java.util.Collection;
import java.util.List;

public interface NewsDao {

    List<News> getByCollectionNewsId(Collection<Integer> newsId);

    List<News> getByAccountId(int accountId, int numPage);

    List<News> getCacheByAccountId(int accountId, int sizeCache);

    Long getByAccountIdQty(int accountId);

}