package com.getjavajob.training.shabanovi.socialnetwork.dao.data;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Membership;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MembershipDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.MembershipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;

@Primary
@Repository
public class MembershipDaoSpringData extends AbstractDaoSpringData<Membership> implements MembershipDao {

    private final MembershipRepository membershipRepository;

    @Autowired
    public MembershipDaoSpringData(MembershipRepository membershipRepository) {
        this.membershipRepository = membershipRepository;
    }

    @Override
    public Membership getMembership(int accountId, int groupId) {
        return membershipRepository.getMembership(accountId, groupId);
    }

    @Override
    public int updateModeratorStatus(int accountId, int groupId, MembershipStatus membershipStatus) {
        return membershipRepository.updateModeratorStatus(accountId, groupId, membershipStatus == MembershipStatus.MODERATOR);
    }

    @Override
    public int deleteMembership(int accountId, int groupId) {
        return membershipRepository.deleteMembership(accountId, groupId);
    }

    @Override
    public List<Account> getRequests(int groupId) {
        return membershipRepository.getRequests(groupId);
    }

    @Override
    public List<Account> getUsers(int groupId) {
        return membershipRepository.getUsers(groupId);
    }

    @Override
    public List<Account> getModerators(int groupId) {
        return membershipRepository.getModerators(groupId);
    }

    @Override
    public List<Group> getRequestGroups(int accountId) {
        return membershipRepository.getRequestGroups(accountId);
    }

    @Override
    public List<Group> getUserGroups(int accountId) {
        return membershipRepository.getUserGroups(accountId);
    }

    @Override
    public List<Group> getModeratorGroups(int accountId) {
        return membershipRepository.getModeratorGroups(accountId);
    }

}