package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {

    @Query("SELECT COUNT(a) FROM Account a")
    int getAllAccountsQty();

    @Query("SELECT a.avatar FROM Account a WHERE a.id=:id")
    byte[] getAvatarById(@Param("id") int id);

    @EntityGraph(attributePaths = {"phones"})
    @Query("SELECT a FROM Account a WHERE a.id=:id")
    Account getWithPhonesById(@Param("id") int id);

    @Query("SELECT a FROM Account a WHERE a.auth.email=:email")
    Account getByEmail(@Param("email") String email);

    @Query("SELECT new Account(a.id, a.name, a.surname) FROM Account a " +
            "WHERE LOWER(a.surname) LIKE CONCAT('%',LOWER(:likeValue) ,'%')" +
            "OR LOWER(a.name) LIKE CONCAT('%',LOWER(:likeValue) ,'%') " +
            "OR CONCAT(LOWER(a.surname),' ', LOWER(a.name)) LIKE CONCAT('%',LOWER(:likeValue) ,'%') " +
            "OR CONCAT(LOWER(a.name),' ', LOWER(a.surname)) LIKE CONCAT('%',LOWER(:likeValue) ,'%')")
    List<Account> searchByPartSurnameOrNameWithPagination(@Param("likeValue") String value, Pageable pageable);

    @Query("SELECT COUNT(a) FROM Account a " +
            "WHERE LOWER(a.surname) LIKE CONCAT('%',LOWER(:likeValue) ,'%')" +
            "OR LOWER(a.name) LIKE CONCAT('%',LOWER(:likeValue) ,'%') " +
            "OR CONCAT(LOWER(a.surname),' ', LOWER(a.name)) LIKE CONCAT('%',LOWER(:likeValue) ,'%') " +
            "OR CONCAT(LOWER(a.name),' ', LOWER(a.surname)) LIKE CONCAT('%',LOWER(:likeValue) ,'%')")
    int getSearchAccountsQty(@Param("likeValue") String value);

}