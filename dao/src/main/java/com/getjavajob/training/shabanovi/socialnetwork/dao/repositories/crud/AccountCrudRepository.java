package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.crud;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountCrudRepository extends GenericCrudRepository<Account> {
}