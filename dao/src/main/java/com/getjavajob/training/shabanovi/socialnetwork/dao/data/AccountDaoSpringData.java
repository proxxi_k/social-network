package com.getjavajob.training.shabanovi.socialnetwork.dao.data;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AccountDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;

@Primary
@Repository
public class AccountDaoSpringData extends AbstractDaoSpringData<Account> implements AccountDao {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountDaoSpringData(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public int getAllAccountsQty() {
        return accountRepository.getAllAccountsQty();
    }

    @Override
    public Account getWithPhonesById(int id) {
        return accountRepository.getWithPhonesById(id);
    }

    @Override
    public Account getByEmail(String email) {
        return accountRepository.getByEmail(email);
    }

    @Override
    public byte[] getAvatarById(int id) {
        return accountRepository.getAvatarById(id);
    }

    @Override
    public int getSearchAccountsQty(String value) {
        return accountRepository.getSearchAccountsQty(value);
    }

    @Override
    public List<Account> searchBySurnameOrName(String value, int numPage) {
        return accountRepository.searchByPartSurnameOrNameWithPagination(value,
                PageRequest.of(numPage, 5, Sort.by("surname", "name", "id")));
    }

}