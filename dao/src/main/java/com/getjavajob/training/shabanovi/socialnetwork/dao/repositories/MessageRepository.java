package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends CrudRepository<Message, Integer> {

    @Query("SELECT m FROM Message m " +
            "WHERE ((m.sender.id=:accountId AND m.accountId=:interlocutorId) OR " +
            "(m.sender.id=:interlocutorId AND m.accountId=:accountId)) AND m.type = " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MessageType).PERSONAL.getStatus()}")
    List<Message> getPersonalMessages(@Param("accountId") int accountId,
                                      @Param("interlocutorId") int interlocutorId, Sort sort);

    @Query("SELECT m FROM Message m " +
            "WHERE m.accountId=:accountId AND m.type= " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MessageType).ACCOUNT_POST.getStatus()}" +
            " ORDER BY m.date DESC")
    List<Message> getAccountWallMessages(@Param("accountId") int accountId);

    @Query("SELECT m FROM Message m " +
            "WHERE m.groupId=:groupId AND m.type= " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MessageType).GROUP_POST.getStatus()} " +
            "ORDER BY m.date DESC")
    List<Message> getGroupWallMessages(@Param("groupId") int groupId);

    @Query("SELECT a FROM Account a " +
            "WHERE EXISTS " +
            "(SELECT m FROM Message m WHERE m.type= " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MessageType).PERSONAL.getStatus()} " +
            "AND ((m.sender.id=:accountId AND m.accountId=a.id) " +
            "OR (m.accountId=:accountId AND m.sender.id=a.id)))")
    List<Account> getAccountsInterlocutors(@Param("accountId") int accountId, Sort sort);

    @Modifying
    @Query("DELETE FROM Message m " +
            "WHERE m.type= " +
            "?#{T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MessageType).PERSONAL.getStatus()} " +
            "AND (m.sender.id=:senderId AND m.accountId=:accountId OR " +
            "m.sender.id=:accountId AND m.accountId=:senderId)")
    int deletePersonalMessages(@Param("senderId") int senderId, @Param("accountId") int accountId);

    @Modifying
    @Query("DELETE FROM Message m " +
            "WHERE m.sender.id=:accountId OR m.accountId=:accountId")
    int deleteAllMessagesByAccountId(@Param("accountId") int accountId);

}