package com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;

import java.util.List;

public interface MessageDao extends GenericDao<Message> {

    List<Message> getPersonalMessages(int accountId, int interlocutorId);

    List<Message> getAccountWallMessages(int accountId);

    List<Message> getGroupWallMessages(int groupId);

    List<Account> getAccountsInterlocutors(int accountId);

    int deletePersonalMessages(int senderId, int accountId);

    int deleteAllMessagesByAccountId(int accountId);

}