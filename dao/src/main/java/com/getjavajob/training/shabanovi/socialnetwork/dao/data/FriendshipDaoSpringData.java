package com.getjavajob.training.shabanovi.socialnetwork.dao.data;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Friendship;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.FriendshipDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.FriendshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;

@Primary
@Repository
public class FriendshipDaoSpringData extends AbstractDaoSpringData<Friendship> implements FriendshipDao {

    private final FriendshipRepository friendshipRepository;

    @Autowired
    public FriendshipDaoSpringData(FriendshipRepository friendshipRepository) {
        this.friendshipRepository = friendshipRepository;
    }

    @Override
    public Friendship getFriendship(int accountId, int friendId) {
        return friendshipRepository.getFriendship(accountId, friendId);
    }

    @Override
    public int updateStatus(int accountId, int friendId, FriendStatus friendStatus) {
        return friendshipRepository.updateStatus(accountId, friendId, friendStatus.getStatus());
    }

    @Override
    public int deleteFriendshipsByAccountId(int accountId) {
        return friendshipRepository.deleteByAccountId(accountId);
    }

    @Override
    public int deleteFriendship(int accountId, int friendId) {
        return friendshipRepository.deleteFriendship(accountId, friendId);
    }

    @Override
    public int getFriendsQty(int accountId) {
        return friendshipRepository.getFriendsQty(accountId);
    }

    @Override
    public int getRequestsFromQty(int accountId) {
        return friendshipRepository.getRequestsFromQty(accountId);
    }

    @Override
    public int getRequestsToQty(int accountId) {
        return friendshipRepository.getRequestsToQty(accountId);
    }

    @Override
    public int getBlacklistsQty(int accountId) {
        return friendshipRepository.getBlacklistsQty(accountId);
    }

    @Override
    public List<Account> getFriends(int accountId, int numPage) {
        return friendshipRepository.getFriends(accountId,
                PageRequest.of(numPage, 5, Sort.by("surname", "name", "id")));
    }

    @Override
    public List<Integer> getFriendsId(int accountId) {
        return friendshipRepository.getFriendsId(accountId);
    }

    @Override
    public List<Account> getRequestsFrom(int accountId, int numPage) {
        return friendshipRepository.getRequestsFrom(accountId,
                PageRequest.of(numPage, 5, Sort.by("surname", "name", "id")));
    }

    @Override
    public List<Account> getRequestsTo(int accountId, int numPage) {
        return friendshipRepository.getRequestsTo(accountId,
                PageRequest.of(numPage, 5, Sort.by("surname", "name", "id")));
    }

    @Override
    public List<Account> getBlacklists(int accountId, int numPage) {
        return friendshipRepository.getBlacklists(accountId,
                PageRequest.of(numPage, 5, Sort.by("surname", "name", "id")));
    }

}