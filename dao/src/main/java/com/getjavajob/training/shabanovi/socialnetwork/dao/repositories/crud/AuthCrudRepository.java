package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.crud;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthCrudRepository extends GenericCrudRepository<Auth> {
}