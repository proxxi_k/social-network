package com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;

import java.util.List;

public interface GroupDao extends GenericDao<Group> {

    int getAllGroupsQty();

    byte[] getImageById(int id);

    Group getByName(String name);

    Group getWithCreatorById(int id);

    List<Group> getCreatedGroups(int accountId);

    int getCountCreatedGroups(int accountId);

    List<Integer> getCreatedGroupsIdByAccountId(int accountId);

    int updateCreator(int newCreatorId, int groupId);

    List<Group> searchByName(String value, int numPage);

    int getSearchGroupsQty(String value);

}