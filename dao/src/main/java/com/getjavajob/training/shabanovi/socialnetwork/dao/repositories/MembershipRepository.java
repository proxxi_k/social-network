package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Membership;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MembershipRepository extends CrudRepository<Membership, Integer> {

    @Query("SELECT m FROM Membership m " +
            "WHERE m.accountId=:accountId AND m.groupId=:groupId")
    Membership getMembership(@Param("accountId") int accountId, @Param("groupId") int groupId);

    @Modifying
    @Query("UPDATE Membership m SET m.moderator=:status " +
            "WHERE m.accountId=:accountId AND m.groupId=:groupId")
    int updateModeratorStatus(@Param("accountId") int accountId, @Param("groupId") int groupId,
                                  @Param("status") boolean moderatorStatus);

    @Modifying
    @Query("DELETE FROM Membership m " +
            "WHERE m.accountId=:accountId AND m.groupId=:groupId")
    int deleteMembership(@Param("accountId") int accountId, @Param("groupId") int groupId);

    @Query("SELECT a FROM Account a LEFT JOIN Membership m ON a.id = m.accountId " +
            "WHERE m.groupId = :groupId AND m.orderApproved = false")
    List<Account> getRequests(@Param("groupId") int groupId);

    @Query("SELECT a FROM Account a LEFT JOIN Membership m ON a.id = m.accountId " +
            "WHERE m.groupId = :groupId AND m.orderApproved = true AND m.moderator = false")
    List<Account> getUsers(@Param("groupId") int groupId);

    @Query("SELECT a FROM Account a LEFT JOIN Membership m ON a.id = m.accountId " +
            "WHERE m.groupId = :groupId AND m.moderator = true")
    List<Account> getModerators(@Param("groupId") int groupId);

    @Query("SELECT g FROM Group g LEFT JOIN Membership m ON g.id = m.groupId " +
            "WHERE m.accountId = :accountId AND m.orderApproved = false")
    List<Group> getRequestGroups(@Param("accountId") int accountId);

    @Query("SELECT g FROM Group g LEFT JOIN Membership m ON g.id = m.groupId " +
            "WHERE m.accountId = :accountId AND m.orderApproved = true AND m.moderator = false")
    List<Group> getUserGroups(@Param("accountId") int accountId);

    @Query("SELECT g FROM Group g LEFT JOIN Membership m ON g.id = m.groupId " +
            "WHERE m.accountId = :accountId AND m.moderator = true")
    List<Group> getModeratorGroups(@Param("accountId") int accountId);

}