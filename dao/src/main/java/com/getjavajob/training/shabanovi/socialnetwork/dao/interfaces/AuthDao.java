package com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;

public interface AuthDao extends GenericDao<Auth> {

    Auth getByEmail(String email);

    String getEmailByAccountId(int accountId);

}