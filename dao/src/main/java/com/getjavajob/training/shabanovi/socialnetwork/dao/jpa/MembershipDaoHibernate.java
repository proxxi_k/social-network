package com.getjavajob.training.shabanovi.socialnetwork.dao.jpa;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Membership;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MembershipDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class MembershipDaoHibernate extends AbstractDaoHibernate<Membership> implements MembershipDao {

    @Autowired
    public MembershipDaoHibernate() {
        setEntityClass(Membership.class);
    }

    @Override
    public Membership getMembership(int accountId, int groupId) {
        try {
            return entityManager.createQuery(
                    "SELECT m FROM Membership m " +
                            "WHERE m.accountId=:accountId AND m.groupId=:groupId", Membership.class)
                    .setParameter("accountId", accountId)
                    .setParameter("groupId", groupId)
                    .getSingleResult();
        } catch (
                NoResultException e) {
            return null;
        }
    }

    @Override
    public int updateModeratorStatus(int accountId, int groupId, MembershipStatus membershipStatus) {
        return entityManager.createQuery(
                "UPDATE Membership m SET m.moderator=:status " +
                        "WHERE m.accountId=:accountId AND m.groupId=:groupId")
                .setParameter("status", membershipStatus == MembershipStatus.MODERATOR)
                .setParameter("accountId", accountId)
                .setParameter("groupId", groupId)
                .executeUpdate();
    }

    @Override
    public int deleteMembership(int accountId, int groupId) {
        return entityManager.createQuery(
                "DELETE FROM Membership m  " +
                        "WHERE m.accountId=:accountId AND m.groupId=:groupId")
                .setParameter("accountId", accountId)
                .setParameter("groupId", groupId)
                .executeUpdate();
    }

    @Override
    public List<Account> getRequests(int groupId) {
        return null;
    }

    @Override
    public List<Account> getUsers(int groupId) {
        return null;
    }

    @Override
    public List<Account> getModerators(int groupId) {
        return null;
    }

    @Override
    public List<Group> getRequestGroups(int groupId) {
        return null;
    }

    @Override
    public List<Group> getUserGroups(int groupId) {
        return null;
    }

    @Override
    public List<Group> getModeratorGroups(int groupId) {
        return null;
    }

}