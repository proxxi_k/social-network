package com.getjavajob.training.shabanovi.socialnetwork.dao.jpa;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MessageDaoHibernate extends AbstractDaoHibernate<Message> implements MessageDao {

    @Autowired
    public MessageDaoHibernate() {
        setEntityClass(Message.class);
    }

    @Override
    public List<Account> getAccountsInterlocutors(int accountId) {
        return entityManager.createQuery(
                "SELECT a FROM Account a " +
                        "WHERE EXISTS " +
                        "(SELECT m FROM Message m WHERE m.type=0 AND ((m.sender.id=:accountId AND m.accountId=a.id) " +
                        "OR (m.accountId=:accountId AND m.sender.id=a.id)))",
                Account.class)
                .setParameter("accountId", accountId)
                .getResultList();
    }

    @Override
    public List<Message> getPersonalMessages(int accountId, int interlocutorId) {
        return entityManager.createQuery(
                "SELECT m FROM Message m " +
                        "WHERE ((m.sender.id=:accountId AND m.accountId=:interlocutorId) OR " +
                        "(m.sender.id=:interlocutorId AND m.accountId=:accountId)) AND m.type=0 " +
                        "ORDER BY m.date", Message.class)
                .setParameter("accountId", accountId)
                .setParameter("interlocutorId", interlocutorId)
                .getResultList();
    }

    @Override
    public List<Message> getAccountWallMessages(int accountId) {
        return null;
    }

    @Override
    public List<Message> getGroupWallMessages(int groupId) {
        return null;
    }

    @Override
    public int deletePersonalMessages(int senderId, int accountId) {
        return entityManager.createQuery(
                "DELETE FROM Message m " +
                        "WHERE m.type=0 AND " +
                        "(m.sender.id=:senderId AND m.accountId=:accountId OR " +
                        "m.sender.id=:accountId AND m.accountId=:senderId)")
                .setParameter("senderId", senderId)
                .setParameter("accountId", accountId)
                .executeUpdate();
    }

    @Override
    public int deleteAllMessagesByAccountId(int accountId) {
        return entityManager.createQuery(
                "DELETE FROM Message m " +
                        "WHERE m.sender.id=:accountId OR m.accountId=:accountId")
                .setParameter("accountId", accountId)
                .executeUpdate();
    }

}