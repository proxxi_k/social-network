package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, Integer> {

    @Query("SELECT COUNT(g) FROM Group g")
    int getAllGroupsQty();

    Group getByName(String name);

    @Query("SELECT g.image FROM Group g WHERE g.id=:id")
    byte[] getImageById(@Param("id") int id);

    @Query("SELECT COUNT(g) FROM Group g " +
            "WHERE g.creator.id = :accountId")
    int getCountCreatedGroups(@Param("accountId") int accountId);

    @EntityGraph(attributePaths = {"creator"})
    @Query("SELECT g FROM Group g WHERE g.id=:groupId")
    Group getWithCreatorById(@Param("groupId") int groupId);

    @Query("SELECT g FROM Group g " +
            "WHERE g.creator.id = :accountId")
    List<Group> getCreatedGroups(@Param("accountId") int accountId);

    @Query("SELECT g.id FROM Group g " +
            "WHERE g.creator.id=:accountId")
    List<Integer> getCreatedGroupsIdByAccountId(@Param("accountId") int accountId);

    @Modifying
    @Query("UPDATE Group g " +
            "SET g.creator.id=:newCreatorId " +
            "WHERE g.id=:groupId")
    int updateCreator(@Param("newCreatorId") int newCreatorId, @Param("groupId") int groupId);

    @Modifying
    @Query("UPDATE Group g " +
            "SET g.name=:#{#group.name}, g.description=:#{#group.description}, g.image=:#{#group.image} " +
            "WHERE g.id=:#{#group.id}")
    int update(@Param("group") Group group);

    @Query("SELECT new com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group (g.id, g.name) " +
            "FROM Group g " +
            "WHERE LOWER(g.name) LIKE CONCAT('%',LOWER(:likeValue) ,'%')")
    List<Group> searchByPartNameWithPagination(@Param("likeValue") String value, Pageable pageable);

    @Query("SELECT COUNT (g) FROM Group g " +
            "WHERE LOWER(g.name) LIKE CONCAT('%',LOWER(:likeValue) ,'%')")
    int getSearchGroupsQty(@Param("likeValue") String value);

}