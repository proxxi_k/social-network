package com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Common;

import java.util.List;

public interface GenericDao<T extends Common> {

    T create(T entity);

    List<T> getAll(int NumPage);

    T getById(int id);

    T update(T entity);

    void deleteById(int id);

}