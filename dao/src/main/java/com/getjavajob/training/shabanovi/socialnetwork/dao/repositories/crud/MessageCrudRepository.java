package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.crud;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageCrudRepository extends GenericCrudRepository<Message> {
}