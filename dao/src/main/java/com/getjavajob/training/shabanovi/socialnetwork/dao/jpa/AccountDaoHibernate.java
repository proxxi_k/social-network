package com.getjavajob.training.shabanovi.socialnetwork.dao.jpa;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class AccountDaoHibernate extends AbstractDaoHibernate<Account> implements AccountDao {

    @Autowired
    public AccountDaoHibernate() {
        setEntityClass(Account.class);
    }

    @Override
    public int getAllAccountsQty() {
        return 0;
    }

    @Override
    public Account getWithPhonesById(int id) {
        try {
            return entityManager.createQuery(
                    "SELECT a FROM Account a " +
                            "LEFT JOIN FETCH a.phones " +
                            "WHERE a.id=:id", Account.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Account getByEmail(String email) {
        try {
            return entityManager.createQuery(
                    "SELECT a FROM Account a " +
                            "WHERE a.auth.email=:email", Account.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public byte[] getAvatarById(int id) {
        return entityManager.createQuery(
                "SELECT a.avatar FROM Account a " +
                        "WHERE a.id=:id", byte[].class)
                .setParameter("id", id).getSingleResult();
    }

    @Override
    public int getSearchAccountsQty(String value) {
        return 0;
    }

    @Override
    public List<Account> searchBySurnameOrName(String value, int numPage) {
        return entityManager.createQuery(
                "SELECT new Account(a.id, a.name, a.surname) FROM Account a " +
                        "WHERE LOWER(a.surname) LIKE :likeValue OR " +
                        "LOWER(a.name) LIKE :likeValue OR " +
                        "CONCAT(LOWER(a.surname),' ', LOWER(a.name)) LIKE :likeValue OR " +
                        "CONCAT(LOWER(a.name),' ', LOWER(a.surname)) LIKE :likeValue " +
                        "ORDER BY a.surname NULLS LAST, a.name, a.id", Account.class)
                .setParameter("likeValue", "%" + value.toLowerCase() + "%")
                .setFirstResult(numPage * 5)
                .setMaxResults(6)
                .getResultList();
    }

    public List<Account> searchByPartSurnameOrName(String value) {
        return entityManager.createQuery(
                "SELECT new Account(a.id, a.name, a.surname) FROM Account a " +
                        "WHERE LOWER(a.surname) LIKE :likeValue OR " +
                        "LOWER(a.name) LIKE :likeValue OR " +
                        "CONCAT(LOWER(a.surname),' ', LOWER(a.name)) LIKE :likeValue OR " +
                        "CONCAT(LOWER(a.name),' ', LOWER(a.surname)) LIKE :likeValue " +
                        "ORDER BY a.surname NULLS LAST, a.name, a.id", Account.class)
                .setParameter("likeValue", "%" + value.toLowerCase() + "%")
                .setMaxResults(3)
                .getResultList();
    }


}