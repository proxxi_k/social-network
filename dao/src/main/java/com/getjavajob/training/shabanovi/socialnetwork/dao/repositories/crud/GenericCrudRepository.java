package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.crud;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Common;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenericCrudRepository<T extends Common> extends JpaRepository<T, Integer> {
}