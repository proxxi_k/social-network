package com.getjavajob.training.shabanovi.socialnetwork.dao.jpa;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Common;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.GenericDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public abstract class AbstractDaoHibernate<T extends Common> implements GenericDao<T> {

    @PersistenceContext
    protected EntityManager entityManager;
    private Class<T> entityClass;

    protected void setEntityClass(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public T create(T entity) {
        try {
            entityManager.persist(entity);
            return entity;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<T> getAll(int numPage) {
        return entityManager.createQuery("FROM " + entityClass.getName(), entityClass).getResultList();
    }

    @Override
    public T getById(int id) {
        return entityManager.find(entityClass, id);
    }

    @Override
    public T update(T entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void deleteById(int id) {
        entityManager.remove(getById(id));
    }

}