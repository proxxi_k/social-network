package com.getjavajob.training.shabanovi.socialnetwork.dao.repositories.crud;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Membership;
import org.springframework.stereotype.Repository;

@Repository
public interface MembershipCrudRepository extends GenericCrudRepository<Membership> {
}