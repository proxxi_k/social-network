package com.getjavajob.training.shabanovi.socialnetwork.dao.impl;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AuthDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AuthDaoTest extends DaoTest {

    @Autowired
    private AuthDao authDao;

    @Test
    public void testGetAll_returnListAllAuths() {
        List<Auth> actual = authDao.getAll(0);
        List<Auth> expected = new ArrayList<>();
        expected.add(new Auth(1, "ivanov@yandex.ru", "1"));
        expected.add(new Auth(2, "petrov@mail.ru", "2"));
        expected.add(new Auth(3, "serg@gmail.com", "3", true));
        expected.add(new Auth(4,  "banketnyj@mail.ru", "4"));
        expected.add(new Auth(5, "aleksandrov@yandex.ru", "5"));
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testGetById_existingId_returnAuth() {
        Auth actual = authDao.getById(1);
        Auth expected = new Auth(1, "ivanov@yandex.ru", "1");
        assertEquals(expected, actual);
    }

    @Test
    public void testGetById_missingId_returnNull() {
        Auth actual = authDao.getById(10);
        assertNull(actual);
    }

    @Test
    public void testUpdate_existingAuth_returnUpdateAuth() {
        Auth auth = new Auth(3, "serg30@mail.ru", "30", false);
        assertEquals(authDao.update(auth), auth);
    }

    @Test
    public void testUpdate_existingAuth_returnUpdateAuthAfterGetById() {
        Auth expected = new Auth(3, "serg30@mail.ru", "30", false);
        authDao.update(expected);
        Auth actual = authDao.getById(3);
        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteById_returnNullAfterGetById() {
        authDao.deleteById(1);
        Auth actual = authDao.getById(1);
        assertNull(actual);
    }

    @Test
    public void testGetByEmail_existingEmail_returnAuth() {
        Auth actual = authDao.getByEmail("serg@gmail.com");
        Auth expected = new Auth(3, "serg@gmail.com", "3", true);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetByEmail_missingEmail_returnNull() {
        Auth actual = authDao.getByEmail("test@mail.ru");
        assertNull(actual);
    }

    @Test
    public void testGetEmailByAccountId_existingAccountId_returnEmail() {
        assertEquals("ivanov@yandex.ru", authDao.getEmailByAccountId(1));
    }

    @Test
    public void testGetEmailByAccountId_missingAccountId_returnNull() {
        assertNull(authDao.getEmailByAccountId(10));
    }

}