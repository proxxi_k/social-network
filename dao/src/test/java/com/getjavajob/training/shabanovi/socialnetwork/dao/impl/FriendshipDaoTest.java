package com.getjavajob.training.shabanovi.socialnetwork.dao.impl;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Friendship;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.FriendshipDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FriendshipDaoTest extends DaoTest {

    @Autowired
    private FriendshipDao friendshipDao;

    @Test
    public void testCreate_createMissingFriendship_returnCreatedFriendship() {
        Friendship actual = friendshipDao.create(new Friendship(3, 9, 1));
        int id = actual.getId();
        Friendship expected = new Friendship(id, 3, 9, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testCreate_returnCreatedFriendshipAfterGetId() {
        int id = friendshipDao.create(new Friendship(3, 9, 1)).getId();
        Friendship actual = friendshipDao.getById(id);
        Friendship expected = new Friendship(id, 3, 9, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAll_returnListAllFriendships() {
        List<Friendship> actual = friendshipDao.getAll(0);
        List<Friendship> expected = new ArrayList<>();
        expected.add(new Friendship(1, 1, 3, 1));
        expected.add(new Friendship(2, 3, 1, -1));
        expected.add(new Friendship(3, 2, 3, -1));
        expected.add(new Friendship(4, 3, 2, 1));
        expected.add(new Friendship(5, 3, 4, 1));
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testGetById_existingId_returnFriendship() {
        Friendship actual = friendshipDao.getById(1);
        Friendship expected = new Friendship(1, 1, 3, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetById_missingId_returnNull() {
        Friendship actual = friendshipDao.getById(100);
        assertNull(actual);
    }

    @Test
    public void testUpdate_existingFriendship_returnUpdateFriendship() {
        Friendship friendship = new Friendship(1, 1, 3, 2);
        assertEquals(friendship, friendshipDao.update(friendship));
    }

    @Test
    public void testUpdate_missingFriendship_returnInsertFriendship() {
        Friendship friendship = new Friendship(12, 3, 9, 1);
        assertEquals(friendship, friendshipDao.update(friendship));
    }

    @Test
    public void testUpdate_existingFriendship_returnUpdateFriendshipAfterGetById() {
        Friendship expected = new Friendship(1, 1, 3, 2);
        friendshipDao.update(expected);
        Friendship actual = friendshipDao.getById(1);
        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteById_returnNullAfterGetById() {
        friendshipDao.deleteById(1);
        Friendship actual = friendshipDao.getById(1);
        assertNull(actual);
    }

    @Test
    public void testGetFriendship_existingFriendshipAndAccountIdLessThanFriendId_returnFriendship() {
        Friendship actual = friendshipDao.getFriendship(1, 3);
        Friendship expected = new Friendship(1, 1, 3, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetFriendship_missingFriendship_returnNull() {
        Friendship actual = friendshipDao.getFriendship(10, 30);
        assertNull(actual);
    }

    @Test
    public void testUpdateStatus_existingFriendship_returnTrue() {
        assertEquals(2, friendshipDao.updateStatus(1, 3, FriendStatus.FRIEND));
    }

    @Test
    public void testUpdateStatus_missingFriendship_returnFalse() {
        assertEquals(0, friendshipDao.updateStatus(10, 30, FriendStatus.FRIEND));
    }

    @Test
    public void testUpdateStatus_existingFriendship_returnUpdateFriendshipsAfterGetById() {
        friendshipDao.updateStatus(1, 3, FriendStatus.FRIEND);
        Friendship actual1 = friendshipDao.getById(1);
        Friendship actual2 = friendshipDao.getById(2);
        Friendship expected1 = new Friendship(1, 1, 3, 2);
        Friendship expected2 = new Friendship(2, 3, 1, 2);
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
    }

    @Test
    public void testDeleteFriendshipsByAccountId_existingFriendships_returnNumberRemoteFriendships() {
        assertEquals(friendshipDao.deleteFriendshipsByAccountId(3), 11);
    }

    @Test
    public void testDeleteFriendshipsByAccountId_missingFriendshipsForAccount_returnZero() {
        assertEquals(friendshipDao.deleteFriendshipsByAccountId(9), 0);
    }

    @Test
    public void testDeleteFriendshipsByAccountId_missingAccount_returnZero() {
        assertEquals(friendshipDao.deleteFriendshipsByAccountId(10), 0);
    }

    @Test
    public void testDeleteFriendship_existingFriendship_returnTrue() {
//        assertTrue(friendshipDao.deleteFriendship(1, 3));
    }

    @Test
    public void testDeleteFriendship_missingFriendship_returnFalse() {
//        assertFalse(friendshipDao.deleteFriendship(10, 30));
    }

    @Test
    public void testDeleteFriendship_returnNullAfterGetById() {
        friendshipDao.deleteFriendship(1, 3);
        Friendship actual = friendshipDao.getById(1);
        assertNull(actual);
    }

}