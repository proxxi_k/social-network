package com.getjavajob.training.shabanovi.socialnetwork.dao.impl;

import com.getjavajob.training.shabanovi.socialnetwork.dao.impl.config.JpaTestConfig;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

@Sql(value = "classpath:create_db.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:drop_db.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ContextConfiguration(classes = JpaTestConfig.class)
@ExtendWith(SpringExtension.class)
@Transactional
@SpringBootTest
public class DaoTest {
}