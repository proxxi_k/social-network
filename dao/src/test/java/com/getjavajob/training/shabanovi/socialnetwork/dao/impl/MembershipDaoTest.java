package com.getjavajob.training.shabanovi.socialnetwork.dao.impl;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Membership;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MembershipDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class MembershipDaoTest extends DaoTest {

    @Autowired
    private MembershipDao membershipDao;

    @Test
    public void testCreate_returnCreatedMembership() {
        Membership actual = membershipDao.create(new Membership(9, 1, false, false));
        int id = actual.getId();
        Membership expected = new Membership(id, 9, 1, false, false);
        assertEquals(expected, actual);
    }

    @Test
    public void testCreate_returnCreatedMembershipAfterGetId() {
        int id = membershipDao.create(new Membership(9, 1, false, false)).getId();
        Membership actual = membershipDao.getById(id);
        Membership expected = new Membership(id, 9, 1, false, false);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAll_returnListAllMemberships() {
        List<Membership> actual = membershipDao.getAll(0);
        List<Membership> expected = new ArrayList<>();
        expected.add(new Membership(1, 3, 2, true, true));
        expected.add(new Membership(2, 3, 3, false, false));
        expected.add(new Membership(3, 3, 4, true, false));
        expected.add(new Membership(4, 3, 6, true, true));
        expected.add(new Membership(5, 1, 5, true, true));
        assertEquals(expected, actual);
    }

    @Test
    public void testGetById_existingId_returnMembership() {
        Membership actual = membershipDao.getById(1);
        Membership expected = new Membership(1, 3, 2, true, true);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetById_missingId_returnNull() {
        Membership actual = membershipDao.getById(100);
        assertNull(actual);
    }

    @Test
    public void testUpdate_existingMembership_returnUpdateMembership() {
        Membership membership = new Membership(1, 50, 10, false, false);
        assertEquals(membershipDao.update(membership), membership);
    }

    @Test
    public void testUpdate_existingMembership_returnUpdateMembershipAfterGetById() {
        Membership expected = new Membership(1, 50, 10, false, false);
        membershipDao.update(expected);
        Membership actual = membershipDao.getById(1);
        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteById_returnNullAfterGetById() {
        membershipDao.deleteById(1);
        Membership actual = membershipDao.getById(1);
        assertNull(actual);
    }

    @Test
    public void testGetMembership_existingMembership_returnMembership() {
        Membership actual = membershipDao.getMembership(3, 2);
        Membership expected = new Membership(1, 3, 2, true, true);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetMembership_missingMembership_returnNull() {
        Membership actual = membershipDao.getMembership(10, 1);
        assertNull(actual);
    }

    @Test
    public void testUpdateModeratorStatus_existingMembership_returnTrue() {
        assertEquals(1, membershipDao.updateModeratorStatus(3, 3, MembershipStatus.MODERATOR));
    }

    @Test
    public void testUpdateModeratorStatus_missingMembership_returnFalse() {
        assertEquals(0, membershipDao.updateModeratorStatus(30, 30, MembershipStatus.MODERATOR));
    }

    @Test
    public void testUpdateModeratorStatus_existingMembership_returnUpdateMembershipAfterGetById() {
        membershipDao.updateModeratorStatus(3, 4, MembershipStatus.MODERATOR);
        Membership expected = new Membership(3, 3, 4, true, true);
        Membership actual = membershipDao.getById(3);
        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteMembership_existingMembership_returnTrue() {
        assertEquals(1, membershipDao.deleteMembership(3, 3));
    }

    @Test
    public void testDeleteMembership_missingMembership_returnFalse() {
        assertEquals(0, membershipDao.deleteMembership(30, 30));
    }

    @Test
    public void testDeleteMembership_returnNullAfterGetById() {
        membershipDao.deleteMembership(3, 4);
        Membership actual = membershipDao.getById(3);
        assertNull(actual);
    }

}