package com.getjavajob.training.shabanovi.socialnetwork.dao.impl;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Phone;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AccountDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AccountDaoTest extends DaoTest {

    @Autowired
    private AccountDao accountDao;

    @Test
    public void testCreate_createMissingAccountWithAuthAndPhones_returnCreatedAccountAndAuthAndPhones() {
        Account actual = new Account("name", "surname", Date.valueOf("2021-10-01"));
        Auth auth = new Auth("email@mail.ru", "0");
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone("личный", "number1"));
        phones.add(new Phone("домашний", "number2"));
        actual.setAuth(auth);
        actual.setPhones(phones);
        auth.setAccount(actual);
        for (Phone phone : phones) {
            phone.setAccount(actual);
        }
        actual = accountDao.create(actual);
        int id = actual.getId();
        Account expected = new Account(id, "name", "surname", Date.valueOf("2021-10-01"));
        assertEquals(expected, actual);
        assertEquals(auth, actual.getAuth());
        assertEquals(phones.size(), actual.getPhones().size());
        assertTrue(phones.containsAll(actual.getPhones()));
    }

    @Test
    public void testCreate_returnCreatedAccountAfterGetById() {
        int id = accountDao.create(new Account("name", "surname", Date.valueOf("2021-10-01"))).getId();
        Account actual = accountDao.getById(id);
        Account expected = new Account(id, "name", "surname", Date.valueOf("2021-10-01"));
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAll_returnListAllAccounts() {
        List<Account> actual = accountDao.getAll(0);
        List<Account> expected = new ArrayList<>();
        expected.add(new Account(1, "Иван", "Иванов", Date.valueOf("2021-01-01")));
        expected.add(new Account(4, "Владимир", "Банкетный", "Анатольевич",
                Date.valueOf("1999-01-01"), "home address", "home address", "banketnyj", "additional info",
                Date.valueOf("2021-04-01")));
        expected.add(new Account(5, "Александр", "Александров", Date.valueOf("2021-05-01")));
        expected.add(new Account(7, "Валера", "Валеров", Date.valueOf("2021-07-01")));
        expected.add(new Account(9, "Неизвестный", Date.valueOf("2021-09-01")));
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testGetById_existingId_returnAccountAndAuth() {
        Account actual = accountDao.getById(1);
        Account expectedAccount = new Account(1, "Иван", "Иванов", Date.valueOf("2021-01-01"));
        Auth expectedAuth = new Auth(1, "ivanov@yandex.ru", "1");
        assertEquals(expectedAccount, actual);
        assertEquals(expectedAuth, actual.getAuth());
    }

    @Test
    public void testGetById_missingId_returnNull() {
        Account actual = accountDao.getById(10);
        assertNull(actual);
    }

    @Test
    public void testUpdate_existingAccount_returnUpdateAccount() {
        Account account = new Account(1, "Иван", "Сидоров", Date.valueOf("2021-01-01"));
        assertEquals(accountDao.update(account), account);
    }

    @Test
    public void testUpdate_missingAccount_returnInsertAccount() {
        Account account = new Account(10, "Иван", "Сидоров", Date.valueOf("2021-01-01"));
        assertEquals(accountDao.update(account), account);
    }

    @Test
    public void testUpdate_existingAccount_returnUpdateAccountAfterGetById() {
        Account expected = new Account(1, "Иван", "Сидоров", Date.valueOf("2021-01-01"));
        accountDao.update(expected);
        Account actual = accountDao.getById(1);
        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteById_returnNullAfterGetById() {
        accountDao.deleteById(1);
        Account actual = accountDao.getById(1);
        assertNull(actual);
    }

    @Test
    public void testGetWithPhonesById_existingPhones_returnListPhones() {
        Account account = accountDao.getWithPhonesById(3);
        List<Phone> expected = new ArrayList<>(Arrays.asList(
                new Phone(3, "личный", "+79993333333"),
                new Phone(4, "рабочий", "+79994444444"),
                new Phone(5, "домашний", "+79995555555")
        ));
        assertEquals(expected.size(), account.getPhones().size());
        assertTrue(expected.containsAll(account.getPhones()));
    }

    @Test
    public void testGetWithPhonesById_missingPhones_returnEmptyList() {
        Account account = accountDao.getWithPhonesById(9);
        assertTrue(account.getPhones().isEmpty());
    }

    @Test
    public void testGetWithPhonesById_missingAccountId_returnNull() {
        assertNull(accountDao.getWithPhonesById(10));
    }

    @Test
    public void testGetByEmail_existingId_returnAccountAndAuth() {
        Account actual = accountDao.getByEmail("ivanov@yandex.ru");
        List<Account> accountList = accountDao.getAll(0);
        System.out.println(accountList);
        accountList.forEach(a -> System.out.println(a.getAuth()));
        Account expectedAccount = new Account(1, "Иван", "Иванов", Date.valueOf("2021-01-01"));
        Auth expectedAuth = new Auth(1, "ivanov@yandex.ru", "1");
        assertEquals(expectedAccount, actual);
        assertEquals(expectedAuth, actual.getAuth());
    }

    @Test
    public void testGetByEmail_missingEmail_returnNull() {
        assertNull(accountDao.getByEmail("missing@mail.ru"));
    }

    @Test
    public void testSearchByPartSurnameOrNameWithPagination_availabilityOfSearchResults_returnFirstSearchPage() {
        List<Account> actual = accountDao.searchBySurnameOrName("а", 0);
        List<Account> expected = new ArrayList<>();
        expected.add(new Account(5, "Александр", "Александров"));
        expected.add(new Account(4, "Владимир", "Банкетный"));
        expected.add(new Account(7, "Валера", "Валеров"));
        expected.add(new Account(1, "Иван", "Иванов"));
        expected.add(new Account(8, "Максим", "Максимов"));
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testSearchByPartSurnameOrNameWithPagination_availabilityOfSearchResults_returnSecondSearchPage() {
        List<Account> actual = accountDao.searchBySurnameOrName("а", 1);
        List<Account> expected = new ArrayList<>();
        expected.add(new Account(6, "Мария", "Мариева"));
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testSearchByPartSurnameOrNameWithPagination_checkingSameSearchResultInUppercaseAndLowercase() {
        List<Account> expected = accountDao.searchBySurnameOrName("a", 1);
        List<Account> actual = accountDao.searchBySurnameOrName("A", 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testSearchByPartSurnameOrNameWithPagination_noSearchResults_returnEmptyList() {
        List<Account> actual = accountDao.searchBySurnameOrName("ъ", 0);
        assertTrue(actual.isEmpty());
    }

}