package com.getjavajob.training.shabanovi.socialnetwork.dao.impl;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MessageDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MessageDaoTest extends DaoTest {

    @Autowired
    private MessageDao messageDao;

    @Test
    public void testCreate_returnCreatedMessage() {
        Message actual = messageDao.create(
                new Message(1, 2, null, "message", Timestamp.valueOf("2021-01-01 00:00:00"), 0));
        int id = actual.getId();
        Message expected = new Message(id, 2, null, "message", Timestamp.valueOf("2021-01-01 00:00:00"), 0);
        assertEquals(expected, actual);
    }

    @Test
    public void testCreate_returnCreatedMessageAfterGetId() {
        int id = messageDao.create(
                new Message(1, 2, null, "message", Timestamp.valueOf("2021-01-01 00:00:00"), 0)).getId();
        Message actual = messageDao.getById(id);
        Message expected = new Message(id, 2, null, "message", Timestamp.valueOf("2021-01-01 00:00:00"), 0);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAll_returnListAllMessages() {
        List<Message> actual = messageDao.getAll(0);
        List<Message> expected = new ArrayList<>();
        expected.add(new Message(1, 2, null, "Привет!", Timestamp.valueOf("2021-03-02 13:13:13"), 0));
        expected.add(new Message(2, 1, null, "Ну привет!", Timestamp.valueOf("2021-03-02 14:14:14"), 0));
        expected.add(new Message(3, 2, null, "Как дела?", Timestamp.valueOf("2021-03-02 15:14:14"), 0));
        expected.add(new Message(4, 1, null, "Привет! Добавь в друзья!", Timestamp.valueOf("2021-03-02 15:15:15"),
                0));
        expected.add(new Message(5, 4, null, "Оооо, а я тебя знаю)!", Timestamp.valueOf("2021-03-02 16:16:16"),
                0));
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testGetById_existingId_returnMessage() {
        Message actual = messageDao.getById(1);
        Message expected = new Message(1, 2, null, "Привет!", Timestamp.valueOf("2021-03-02 13:13:13"), 0);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetById_missingId_returnNull() {
        Message actual = messageDao.getById(100);
        assertNull(actual);
    }

    @Test
    public void testUpdate_existingMessage_returnUpdateMessage() {
        Message message = new Message(1, 1, null, "new message", Timestamp.valueOf("2021-01-01 00:00:00"), 2);
        assertEquals(messageDao.update(message), message);
    }

    @Test
    public void testUpdate_existingMessage_returnUpdateMessageAfterGetById() {
        Message expected = new Message(1, 2, null, "new message", Timestamp.valueOf("2021-01-01 00:00:00"), 2);
        messageDao.update(expected);
        Message actual = messageDao.getById(1);
        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteById_returnNullAfterGetById() {
        messageDao.deleteById(1);
        Message actual = messageDao.getById(1);
        assertNull(actual);
    }

    @Test
    public void testGetPersonalMessage_existingPersonalMessage_returnListMessages() {
        List<Message> actual = messageDao.getPersonalMessages(1, 2);
        List<Message> expected = new ArrayList<>();
        expected.add(new Message(1, 2, null, "Привет!", Timestamp.valueOf("2021-03-02 13:13:13"), 0));
        expected.add(new Message(2, 1, null, "Ну привет!", Timestamp.valueOf("2021-03-02 14:14:14"), 0));
        expected.add(new Message(3, 2, null, "Как дела?", Timestamp.valueOf("2021-03-02 15:14:14"), 0));
        assertEquals(expected, actual);
    }

    @Test
    public void testGetPersonalMessage_checkingSameMessagesFromRecipientAndSender_returnTrue() {
        List<Message> actual = messageDao.getPersonalMessages(1, 2);
        List<Message> actual2 = messageDao.getPersonalMessages(2, 1);
        assertEquals(actual, actual2);
    }

    @Test
    public void testGetPersonalMessage_missingPersonalMessage_returnEmptyList() {
        List<Message> actual = messageDao.getPersonalMessages(1, 9);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetAccountsInterlocutors_existingInterlocutors_returnListAccountInterlocutors() {
        List<Account> actual = messageDao.getAccountsInterlocutors(1);
        List<Account> expected = new ArrayList<>();
        expected.add(new Account(4, "Владимир", "Банкетный", "Анатольевич",
                Date.valueOf("1999-01-01"), "home address", "home address", "banketnyj", "additional info",
                Date.valueOf("2021-04-01")));
        expected.add(new Account(2, "Петр", "Петров", Date.valueOf("2021-02-01")));
        expected.add(new Account(3, "Сергей", "Сергеев", Date.valueOf("2021-03-01")));
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAccountsInterlocutors_missingInterlocutors_returnEmptyList() {
        List<Account> actual = messageDao.getAccountsInterlocutors(9);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testDeletePersonalMessages_existingMessage_returnNumberRemoteMessages() {
        assertEquals(messageDao.deletePersonalMessages(1, 2), 3);
    }

    @Test
    public void testDeletePersonalMessages_missingMessage_returnZero() {
        assertEquals(messageDao.deletePersonalMessages(1, 9), 0);
    }

    @Test
    public void testDeleteById_returnEmptyListMessagesAfterGetById() {
        messageDao.deletePersonalMessages(1, 2);
        List<Message> actual = messageDao.getPersonalMessages(1, 2);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testDeleteAllMessagesByAccountId_existingMessage_returnNumberRemoteMessages() {
        assertEquals(messageDao.deleteAllMessagesByAccountId(1), 10);
    }

    @Test
    public void testDeleteAllMessagesByAccountId_missingMessage_returnZero() {
        assertEquals(messageDao.deleteAllMessagesByAccountId(9), 0);
    }

}