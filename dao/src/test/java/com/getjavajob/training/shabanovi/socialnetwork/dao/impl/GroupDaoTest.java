package com.getjavajob.training.shabanovi.socialnetwork.dao.impl;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AccountDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.GroupDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GroupDaoTest extends DaoTest {

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private GroupDao groupDao;

    @Test
    public void testCreate_returnCreatedGroup() {
        Account creator = accountDao.getById(3);
        Group group = new Group("name", "description", Date.valueOf("2021-10-01"));
        group.setCreator(creator);
        Group actual = groupDao.create(group);
        Group expected = new Group(actual.getId(), "name", "description", Date.valueOf("2021-10-01"));
        assertEquals(expected, actual);
    }

    @Test
    public void testCreate_returnCreatedGroupAfterGetId() {
        Account creator = accountDao.getById(3);
        Group group = new Group("name", "description", Date.valueOf("2021-10-01"));
        group.setCreator(creator);
        int id = groupDao.create(group).getId();
        Group actual = groupDao.getById(id);
        Group expected = new Group(id, "name", "description", Date.valueOf("2021-10-01"));
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAll_returnListAllGroups() {
        List<Group> actual = groupDao.getAll(0);
        List<Group> expected = new ArrayList<>();
        expected.add(new Group(1, "Группа №1", "лучшая группа", Date.valueOf("2021-01-02")));
        expected.add(new Group(2, "Группа №2", "классная группа", Date.valueOf("2021-02-02")));
        expected.add(new Group(3, "Группа №3", Date.valueOf("2021-03-02")));
        expected.add(new Group(4, "Группа №4", Date.valueOf("2021-04-02")));
        expected.add(new Group(5, "Группа №5", Date.valueOf("2021-05-02")));
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testGetById_existingId_returnGroup() {
        Group actual = groupDao.getById(1);
        Group expected = new Group(1, "Группа №1", "лучшая группа", Date.valueOf("2021-01-02"));
        assertEquals(expected, actual);
    }

    @Test
    public void testGetById_missingId_returnNull() {
        Group actual = groupDao.getById(10);
        assertNull(actual);
    }

    @Test
    public void testUpdate_existingGroup_returnUpdateGroup() {
        Group group = new Group(1, "new name", Date.valueOf("2021-01-01"));
        assertEquals(groupDao.update(group), group);
    }

    @Test
    public void testUpdate_existingGroup_returnUpdateAccountAfterGetById() {
        Group expected = new Group(1, "new name", Date.valueOf("2021-06-02"));
        groupDao.update(expected);
        Group actual = groupDao.getById(1);
        assertEquals(expected, actual);
    }


    @Test
    public void testDeleteById_returnNullAfterGetById() {
        groupDao.deleteById(1);
        Group actual = groupDao.getById(1);
        assertNull(actual);
    }

    @Test
    public void testGetByName_existingName_returnGroup() {
        Group actual = groupDao.getByName("Группа №1");
        Group expected = new Group(1, "Группа №1", "лучшая группа", Date.valueOf("2021-01-02"));
        assertEquals(expected, actual);
    }

    @Test
    public void testGetByName_missingName_returnNull() {
        Group actual = groupDao.getByName("Группа №10");
        assertNull(actual);
    }

    @Test
    public void testGetCreatedGroupsIdByAccountId_existingCreatedGroups_returnListCreatedGroupsId() {
        List<Integer> actual = groupDao.getCreatedGroupsIdByAccountId(3);
        List<Integer> expected = Arrays.asList(1, 5);
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testGetCreatedGroupsIdByAccountId_missingCreatedGroups_returnEmptyListCreatedGroupsId() {
        List<Integer> actual = groupDao.getCreatedGroupsIdByAccountId(9);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetCreatedGroupsIdByAccountId_missingAccountId_returnEmptyListCreatedGroupsId() {
        List<Integer> actual = groupDao.getCreatedGroupsIdByAccountId(10);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testUpdateCreator_existingGroup_returnTrue() {
        assertEquals(1, groupDao.updateCreator(9, 1));
    }

    @Test
    public void testUpdateCreator_missingGroup_returnFalse() {
        assertEquals(0, groupDao.updateCreator(9, 10));
    }

    @Test
    public void testUpdateCreator_existingGroup_returnUpdateGroupAfterGetById() {
        assertEquals(1, groupDao.updateCreator(9, 1));
        Account expected = accountDao.getById(9);
        Account actual = groupDao.getById(1).getCreator();
        assertEquals(expected, actual);
    }

    @Test
    public void testSearchByPartNameWithPagination_availabilityOfSearchResultsIsNext_returnFirstSearchPage() {
        List<Group> actual = groupDao.searchByName("а", 0);
        List<Group> expected = new ArrayList<>();
        expected.add(new Group(1, "Группа №1"));
        expected.add(new Group(2, "Группа №2"));
        expected.add(new Group(3, "Группа №3"));
        expected.add(new Group(4, "Группа №4"));
        expected.add(new Group(5, "Группа №5"));
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testSearchByPartNameWithPagination_availabilityOfSearchResultsIsNext_returnSecondSearchPage() {
        List<Group> actual = groupDao.searchByName("а", 1);
        List<Group> expected = new ArrayList<>();
        expected.add(new Group(6, "Группа №6"));
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testSearchByPartNameWithPagination_checkingSameSearchResultInUppercaseAndLowercase() {
        List<Group> expected = groupDao.searchByName("a", 1);
        List<Group> actual = groupDao.searchByName("A", 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testSearchByPartNameWithPagination_noSearchResults_returnEmptyList() {
        List<Group> actual = groupDao.searchByName("ъ", 0);
        assertTrue(actual.isEmpty());
    }

}