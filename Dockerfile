#FROM maven:3.8.4-openjdk-8 as builder
#WORKDIR /app
#COPY . /app
#RUN mvn package
#FROM openjdk:8
#COPY --from=builder /app/webapp/target/*.war social-network-1.0.war
#ENTRYPOINT ["java", "-jar", "social-network-1.0.war"]

FROM openjdk:8
ADD webapp/target/webapp-1.0-SNAPSHOT.war social-network-1.0.war
ENTRYPOINT ["java", "-jar", "social-network-1.0.war"]