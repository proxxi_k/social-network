package com.getjavajob.training.shabanovi.socialnetwork.webapp.config;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.service.AccountDetailService;
import com.getjavajob.training.shabanovi.socialnetwork.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final Integer MAX_AGE_REMEMBER_ME_TOKEN = 60 * 60 * 24 * 30;
    private static final Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);
    private final AccountDetailService accountDetailService;
    private final AccountService accountService;

    @Autowired
    public WebSecurityConfig(AccountDetailService accountDetailService, AccountService accountService) {
        this.accountDetailService = accountDetailService;
        this.accountService = accountService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/login", "/accounts/registration", "/resources/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").usernameParameter("email")
                .successHandler(loginSuccessHandler()).failureHandler(loginFailureHandler()).permitAll()
                .and()
                .rememberMe().key("secretKey").tokenValiditySeconds(MAX_AGE_REMEMBER_ME_TOKEN)
                .userDetailsService(accountDetailService).authenticationSuccessHandler(rememberMeSuccessHandler())
                .and()
                .logout().logoutUrl("/logout").logoutSuccessUrl("/login?logout=true")
                .invalidateHttpSession(true).deleteCookies("JSESSIONID", "remember-me")
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(accountDetailService);
        return daoAuthenticationProvider;
    }

    @Bean
    public SavedRequestAwareAuthenticationSuccessHandler getSavedRequestAwareAuthenticationSuccessHandler(){
        return new SavedRequestAwareAuthenticationSuccessHandler();
    }

    @Bean
    public AuthenticationSuccessHandler loginSuccessHandler() {
        return (request, response, authentication) -> {
            logger.warn("Account with email: '{}' successful login", authentication.getName());
            Account account = accountService.getAccountByEmail(authentication.getName());
            request.getSession().setAttribute("loginAccount", account);
            getSavedRequestAwareAuthenticationSuccessHandler().onAuthenticationSuccess(request, response, authentication);
        };
    }

    @Bean
    public AuthenticationSuccessHandler rememberMeSuccessHandler() {
        return (request, response, authentication) -> {
            logger.warn("Account with email: '{}' successful login with remember me token", authentication.getName());
            request.getSession().setAttribute("loginAccount",
                    accountService.getAccountByEmail(authentication.getName()));
            response.sendRedirect(request.getRequestURI());
        };
    }

    @Bean
    public AuthenticationFailureHandler loginFailureHandler() {
        return (request, response, authentication) -> response.sendRedirect("/login?error=true&email=" + request.getParameter("email"));
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return (request, response, accessDeniedException) -> {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null) {
                logger.warn("Account '{}' attempted to access the protected URL: '{}'",
                        auth.getName(), request.getRequestURI());
            }
            request.setAttribute("error", "У Вас нет прав доступа к данной странице/ресурсу");
            request.getRequestDispatcher("/WEB-INF/views//errorPage.jsp").forward(request, response);
        };
    }

}