package com.getjavajob.training.shabanovi.socialnetwork.webapp.controllers;

import com.getjavajob.training.shabanovi.socialnetwork.common.Event;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.EventType;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.InternetPage;
import com.getjavajob.training.shabanovi.socialnetwork.service.AccountService;
import com.getjavajob.training.shabanovi.socialnetwork.service.GroupService;
import com.getjavajob.training.shabanovi.socialnetwork.webapp.config.RedisConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.Date;
import java.util.Objects;
import java.util.Random;

@Controller
@RequestMapping("messages")
public class MessageController {

    public static final int PROBABILITY_CHECK_SIZE_ZSET = 10;
    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
    private final AccountService accountService;
    private final GroupService groupService;
    private final RabbitTemplate rabbitTemplate;
    private final RedisTemplate<String, Integer> redisTemplate;
    private final Random random;

    @Autowired
    public MessageController(AccountService accountService, GroupService groupService, RabbitTemplate rabbitTemplate,
                             RedisTemplate<String, Integer> redisTemplate) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.rabbitTemplate = rabbitTemplate;
        this.redisTemplate = redisTemplate;
        random = new Random();
    }

    @RequestMapping(value = "{recipientId}/send", method = RequestMethod.POST)
    public String sendWallMessage(@PathVariable Integer recipientId,
                                  @RequestParam("messageBody") String messageBody,
                                  @RequestParam("internetPage") String internetPage,
                                  @SessionAttribute("loginAccount") Account loginAccount,
                                  Model model) {
        Message message;
        switch (InternetPage.valueOf(internetPage)) {
            case ACCOUNT_PAGE:
                logger.info("Sending a message from account with id='{}' to wall account with id='{}'",
                        loginAccount.getId(), recipientId);
                message = new Message(recipientId, null, messageBody);
                message.setSender(loginAccount);
                accountService.sendAccountPost(message);
                if (loginAccount.getId() == recipientId) {
                    for (int friendId : accountService.getFriendsId(loginAccount.getId())) {
                        rabbitTemplate.convertAndSend(new Event(loginAccount, accountService.getAccountById(friendId),
                                EventType.ACCOUNT_POST));
                        if (Objects.equals(redisTemplate.hasKey("feed:" + friendId), true)) {
                            redisTemplate.opsForZSet().add("feed:" + friendId, message.getId(), new Date().getTime());
                            if (random.nextInt(PROBABILITY_CHECK_SIZE_ZSET + 1) == PROBABILITY_CHECK_SIZE_ZSET) {
                                redisTemplate.opsForZSet().removeRange("feed:" + friendId, 0,
                                        -(RedisConfig.SIZE_CACHE + 1));
                            }
                        }
                    }
                }
                return "redirect:/accounts/" + recipientId;
            case GROUP_PAGE:
                logger.info("Sending a message from account with id='{}' to wall group with id='{}'",
                        loginAccount.getId(), recipientId);
                message = new Message(null, recipientId, messageBody);
                message.setSender(loginAccount);
                groupService.createGroupPost(message);
                return "redirect:/groups/" + recipientId;
            default:
                model.addAttribute("error", "Сообщение не может быть отправлено");
                return "errorPage";
        }
    }

    @RequestMapping(value = "{messageId}/delete", method = RequestMethod.POST)
    public String deleteMessage(@PathVariable int messageId,
                                @RequestParam(value = "accountId", required = false) Integer accountId,
                                @RequestParam(value = "groupId", required = false) Integer groupId,
                                @RequestParam("internetPage") String internetPage,
                                Model model) {
        logger.info("Deleting a message with id='{}'", messageId);
        switch (InternetPage.valueOf(internetPage)) {
            case ACCOUNT_PAGE:
                accountService.deleteMessage(messageId);
                return "redirect:/accounts/" + accountId;
            case GROUP_PAGE:
                groupService.deleteGroupPost(messageId);
                return "redirect:/groups/" + groupId;
            default:
                model.addAttribute("error", "Сообщение не может быть удалено");
                return "errorPage";
        }
    }

    @MessageMapping("/chat/{sender}/{recipient}")
    @SendTo("/topic/{sender}/{recipient}")
    public Message getPersonalMessage(Message message) {
        logger.info("Account with id='{}' send personal message to account with id='{}'", message.getSender().getId(),
                message.getAccountId());
        rabbitTemplate.convertAndSend(new Event(accountService.getAccountById(message.getSender().getId()),
                accountService.getAccountById(message.getAccountId()), EventType.PERSONAL_MESSAGE));
        return accountService.sendPersonalMessage(message);
    }

}