package com.getjavajob.training.shabanovi.socialnetwork.webapp.controllers;

import com.getjavajob.training.shabanovi.socialnetwork.service.AccountService;
import com.getjavajob.training.shabanovi.socialnetwork.service.GroupService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping("images")
public class ImageController {

    private final AccountService accountService;
    private final GroupService groupService;
    ServletContext servletContext;

    @Autowired
    public ImageController(ServletContext servletContext, AccountService accountService, GroupService groupService) {
        this.servletContext = servletContext;
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @ResponseBody
    @RequestMapping(value = "/avatar/{accountId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] printAccountAvatar(@PathVariable int accountId) throws IOException {
        byte[] avatar = accountService.getAvatarById(accountId);
        if (avatar != null) {
            return avatar;
        } else {
            InputStream in = servletContext.getResourceAsStream("/resources/img/default-avatar.jpg");
            return IOUtils.toByteArray(in);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/group-image/{groupId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] printGroupImage(@PathVariable int groupId) throws IOException {
        byte[] image = groupService.getImageById(groupId);
        if (image != null) {
            return image;
        } else {
            InputStream in = servletContext.getResourceAsStream("/resources/img/default-group.jpg");
            return IOUtils.toByteArray(in);
        }
    }

}