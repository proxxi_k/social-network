package com.getjavajob.training.shabanovi.socialnetwork.webapp.utils;

import javax.servlet.http.Cookie;

public class CookieUtils {

    public static void deleteCookie(Cookie cookie) {
        if (cookie!=null){
            cookie.setMaxAge(0);
            cookie.setValue("");
        }
    }

}