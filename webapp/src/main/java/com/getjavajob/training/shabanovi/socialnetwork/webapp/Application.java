package com.getjavajob.training.shabanovi.socialnetwork.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@SpringBootApplication(scanBasePackages = {"com.getjavajob.training.shabanovi.socialnetwork.dao",
        "com.getjavajob.training.shabanovi.socialnetwork.service",
        "com.getjavajob.training.shabanovi.socialnetwork.webapp"})
@EnableJpaRepositories(basePackages = {"com.getjavajob.training.shabanovi.socialnetwork.dao.repositories"})
@EntityScan(basePackages = "com.getjavajob.training.shabanovi.socialnetwork.common")
@EnableRedisHttpSession
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}