package com.getjavajob.training.shabanovi.socialnetwork.webapp.utils;

import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;

public class CustomStringEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.hasLength(text)){
            setValue(text);
        } else {
            setValue(null);
        }
    }
}