package com.getjavajob.training.shabanovi.socialnetwork.webapp.controllers;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.service.AccountService;
import com.getjavajob.training.shabanovi.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class SearchController {

    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
    private final AccountService accountService;
    private final GroupService groupService;

    @Autowired
    public SearchController(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    protected String search(@RequestParam("search") String searchValue,
                            Model model) {
        logger.info("Pagination search for accounts and groups containing the value='{}'", searchValue);
        model.addAttribute("search", searchValue);
        int qtyAccounts = accountService.getSearchAccountsQty(searchValue);
        int qtyGroups = groupService.getSearchGroupsQty(searchValue);
        model.addAttribute("maxNumPageAccount",
                qtyAccounts % 5 == 0 && qtyAccounts != 0 ? (qtyAccounts / 5) - 1 : (qtyAccounts / 5));
        model.addAttribute("maxNumPageGroup",
                qtyGroups % 5 == 0 && qtyGroups != 0 ? (qtyGroups / 5) - 1 : (qtyGroups / 5));
        return "search";
    }

    @RequestMapping(value = "api/search/accounts", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> searchPaginationAccountAjax(@RequestParam("filter") String filter,
                                                     @RequestParam("numPage") int numPage) {
        logger.info("Pagination search for accounts containing the value='{}', page number='{}'", filter, numPage);
        return accountService.searchAccountsWithPagination(filter, numPage);
    }

    @RequestMapping(value = "api/search/groups", method = RequestMethod.GET)
    @ResponseBody
    public List<Group> searchPaginationGroupAjax(@RequestParam("filter") String filter,
                                                 @RequestParam("numPage") int numPage) {
        logger.info("Pagination search for groups containing the value='{}', page number='{}'", filter, numPage);
        return groupService.searchGroupsWithPagination(filter, numPage);
    }

    @RequestMapping(value = "api/search/autocomplete", method = RequestMethod.GET)
    @ResponseBody
    public List<Object> searchAutocompleteAjax(@RequestParam("filter") String filter) {
        logger.info("Ajax search for accounts and groups containing the value='{}'", filter);
        List<Object> listSearchResults = new ArrayList<>();
        listSearchResults.addAll(accountService.searchAccounts(filter));
        listSearchResults.addAll(groupService.searchGroups(filter));
        return listSearchResults;
    }

}