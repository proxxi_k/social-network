package com.getjavajob.training.shabanovi.socialnetwork.webapp.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    public static final String EVENT_QUEUE = "event-queue";
    public static final String FIRST_DELAY_EVENT_QUEUE = "first-delay-event-queue";
    public static final String SECOND_DELAY_EVENT_QUEUE = "second-delay-event-queue";
    public static final String DEAD_EVENT_QUEUE = "dead-event-queue";
    public static final String EVENT_EXCHANGE = "event-exchange";
    public static final String DELAY_EVENT_EXCHANGE = "delay-event-exchange";
    private static final int FIRST_DELAY = 5 * 60 * 1000;
    private static final int SECOND_DELAY = 15 * 60 * 1000;

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setRoutingKey(EVENT_QUEUE);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Queue eventQueue() {
        return QueueBuilder.durable(EVENT_QUEUE).deadLetterExchange(DELAY_EVENT_EXCHANGE).build();
    }

    @Bean
    public Queue firstDelayEventQueue() {
        return QueueBuilder.durable(FIRST_DELAY_EVENT_QUEUE).ttl(FIRST_DELAY).deadLetterExchange(EVENT_EXCHANGE)
                .deadLetterRoutingKey(FIRST_DELAY_EVENT_QUEUE).build();
    }

    @Bean
    public Queue secondDelayEventQueue() {
        return QueueBuilder.durable(SECOND_DELAY_EVENT_QUEUE).ttl(SECOND_DELAY).deadLetterExchange(EVENT_EXCHANGE)
                .deadLetterRoutingKey(SECOND_DELAY_EVENT_QUEUE).build();
    }

    @Bean
    public Queue deadEventQueue() {
        return QueueBuilder.durable(DEAD_EVENT_QUEUE).build();
    }

    @Bean
    public FanoutExchange eventExchange() {
        return ExchangeBuilder.fanoutExchange(EVENT_EXCHANGE).durable(true).build();
    }

    @Bean
    public TopicExchange delayEventExchange() {
        return ExchangeBuilder.topicExchange(DELAY_EVENT_EXCHANGE).durable(true).build();
    }

    @Bean
    public Binding bindingEvent() {
        return BindingBuilder.bind(eventQueue()).to(eventExchange());
    }

    @Bean
    public Binding bindingFirstDelayEvent() {
        return BindingBuilder.bind(firstDelayEventQueue()).to(delayEventExchange()).with(EVENT_QUEUE);
    }

    @Bean
    public Binding bindingSecondDelayEvent() {
        return BindingBuilder.bind(secondDelayEventQueue()).to(delayEventExchange()).with(FIRST_DELAY_EVENT_QUEUE);
    }

    @Bean
    public Binding bindingDeadEvent() {
        return BindingBuilder.bind(deadEventQueue()).to(delayEventExchange()).with(SECOND_DELAY_EVENT_QUEUE);
    }

}