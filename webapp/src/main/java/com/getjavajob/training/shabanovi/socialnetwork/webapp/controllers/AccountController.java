package com.getjavajob.training.shabanovi.socialnetwork.webapp.controllers;

import com.getjavajob.training.shabanovi.socialnetwork.common.Event;
import com.getjavajob.training.shabanovi.socialnetwork.common.News;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Phone;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.EventType;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendshipTab;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.InternetPage;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipAccountTab;
import com.getjavajob.training.shabanovi.socialnetwork.service.AccountService;
import com.getjavajob.training.shabanovi.socialnetwork.webapp.config.RedisConfig;
import com.getjavajob.training.shabanovi.socialnetwork.webapp.utils.CustomStringEditor;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.getjavajob.training.shabanovi.socialnetwork.webapp.utils.CookieUtils.deleteCookie;
import static com.thoughtworks.xstream.security.AnyTypePermission.ANY;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;

@Controller
@RequestMapping("accounts")
public class AccountController {

    private static final int IMG_MAX_SIZE = 65000;
    private static final int IMG_MIN_SIZE = 0;
    private static final String REQUEST_FRIENDSHIP_URI = "/request";
    private static final String ADD_FRIEND_URI = "/add";
    private static final String ADD_BLACKLIST_URI = "/block";
    private static final String DELETE_FRIENDSHIP_URI = "/delete";
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private final RedisTemplate<String, Integer> redisTemplate;
    private final RabbitTemplate template;
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;
    private final XStream xStream;

    @Autowired
    public AccountController(RedisTemplate<String, Integer> redisTemplate, RabbitTemplate template,
                             AccountService accountService, PasswordEncoder passwordEncoder) {
        this.redisTemplate = redisTemplate;
        this.template = template;
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
        this.xStream = new XStream();
        xStream.autodetectAnnotations(true);
        xStream.addPermission(ANY);
    }

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        binder.registerCustomEditor(String.class, new CustomStringEditor());
        CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true);
        binder.registerCustomEditor(Date.class, editor);
    }

    /* Accounts */

    @RequestMapping(value = "registration", method = RequestMethod.GET)
    public String showRegistration(@SessionAttribute(value = "loginAccount", required = false) Account loginAccount,
                                   Model model) {
        logger.info("Going to the new account registration page");
        if (loginAccount != null) {
            return "redirect:/accounts/" + loginAccount.getId();
        }
        model.addAttribute("account", new Account());
        return "registrationAccount";
    }

    @RequestMapping(value = "registration", method = RequestMethod.POST)
    public String doRegistration(@ModelAttribute Account account,
                                 @RequestParam("email") String email,
                                 @RequestParam("password") String password,
                                 Model model, HttpSession session) {
        logger.info("Registering a new account with email='{}'", email);
        boolean isIdentified = accountService.getAuthByEmail(email) != null;
        boolean isExceedingImageSize = account.getAvatar().length > IMG_MAX_SIZE;
        if (!isIdentified && !isExceedingImageSize) {
            if (account.getAvatar().length == IMG_MIN_SIZE) {
                account.setAvatar(null);
            }
            if (account.getPhones() != null) {
                for (Phone phone : account.getPhones()) {
                    phone.setAccount(account);
                }
            }
            session.setAttribute("loginAccount", accountService.createAccount(email,
                    passwordEncoder.encode(password), account));
            return "redirect:/accounts/" + account.getId();
        } else {
            model.addAttribute("account", account);
            model.addAttribute("email", email);
            if (isIdentified) {
                model.addAttribute("errorEmail", "Пользователь с указанным почтовым адресом уже существует");
                logger.warn("Error when registering a new account with mail='{}': the account with the specified " +
                        "email address already exists", email);
            }
            if (isExceedingImageSize) {
                model.addAttribute("errorSizeAvatar", "Размер выбранного файла превышает 65КБ");
                logger.warn("Error when registering a new account with mail='{}': the size of the selected avatar " +
                        "file exceeds 65KB", email);
            }
            return "registrationAccount";
        }
    }

    @RequestMapping(value = "{accountId}", method = RequestMethod.GET)
    public String showAccount(@PathVariable int accountId,
                              @SessionAttribute("loginAccount") Account loginAccount,
                              Model model, HttpSession session) {
        logger.info("Going to the home page of the account with the id='{}'", accountId);
        Account account = accountService.getAccountWithPhonesById(accountId);
        if (account != null) {
            model.addAttribute("account", account);
            model.addAttribute("isCheckCreatedGroups", accountService.checkedCreatedGroups(accountId));
            model.addAttribute("friendStatus", accountService.getFriendshipStatus(loginAccount.getId(), accountId));
            model.addAttribute("wallMessages", accountService.getWallMessages(accountId));
            return "account";
        } else {
            model.addAttribute("error", "Аккаунт не существует");
            logger.warn("Error when going to the home page of an account with the id='{}': the account does not exist",
                    accountId);
            return "errorPage";
        }
    }

    @PreAuthorize("#loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/save", method = RequestMethod.GET)
    public void saveAccount(@PathVariable(name = "accountId") int accountId,
                            @SessionAttribute("loginAccount") Account loginAccount,
                            HttpServletResponse response) {
        logger.info("Saving an account with id='{}' to an xml file", accountId);
        Account account = accountService.getAccountWithPhonesById(accountId);
        if (account != null) {
            response.setHeader(CONTENT_DISPOSITION, "attachment; filename=account.xml");
            try {
                response.getOutputStream().write(xStream.toXML(account).getBytes());
            } catch (IOException e) {
                response.setStatus(SC_BAD_REQUEST);
                logger.error("Error when saving an account with id='{}' to an xml file: error getting the output " +
                        "stream response", accountId, e);
            }
        } else {
            logger.warn("Error when saving an account with id='{}' to an xml file: the account does not exist",
                    accountId);
            response.setStatus(SC_BAD_REQUEST);
        }
    }

    @RequestMapping(value = "{accountId}/read", method = RequestMethod.POST)
    public String readAccount(@PathVariable int accountId,
                              @RequestParam("file") MultipartFile file,
                              Model model) {
        logger.info("Reading an account with id='{}' from an xml file", accountId);
        Account account = accountService.getAccountWithPhonesById(accountId);
        if (Objects.isNull(file) || file.isEmpty()) {
            model.addAttribute("errorXmlFile", "Файл не выбран или пустой");
            model.addAttribute("account", account);
            logger.warn("Error with reading an account with id='{}' from an xml file: file not selected or empty",
                    accountId);
            return "updateAccount";
        } else {
            xStream.alias("account", Account.class);
            try {
                Account xmlAccount = (Account) xStream.fromXML(new String(file.getBytes()));
                xmlAccount.setRegistrationDate(account.getRegistrationDate());
                xmlAccount.setAvatar(account.getAvatar());
                model.addAttribute("account", xmlAccount);
                return "updateAccount";
            } catch (IOException | XStreamException e) {
                model.addAttribute("errorXmlFile", "Файл поврежден");
                model.addAttribute("account", account);
                logger.error("Error when reading an account with id='{}' from an xml file: error reading from file",
                        account.getId(), e);
                return "updateAccount";
            }
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showAllAccounts(Model model) {
        logger.info("Going to the page with all accounts");
        int qty = accountService.getAllAccountsQty();
        model.addAttribute("maxNumPage", qty % 5 == 0 ? (qty / 5) - 1 : (qty / 5));
        return "allAccounts";
    }

    @RequestMapping(value = "api", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> getPaginationAllAccounts(@RequestParam("numPage") int numPage) {
        logger.info("Pagination all accounts, page number='{}'", numPage);
        return accountService.getAllAccounts(numPage);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/update", method = RequestMethod.GET)
    public String showUpdateAccount(@PathVariable int accountId,
                                    @SessionAttribute("loginAccount") Account loginAccount,
                                    Model model) {
        logger.info("Going to the account with id='{}' update page", accountId);
        Account account = accountService.getAccountWithPhonesById(accountId);
        if (account != null) {
            model.addAttribute("account", account);
            return "updateAccount";
        } else {
            logger.warn("Error when going to the account with id='{}' update page: the account does not exist",
                    accountId);
            model.addAttribute("error", "Аккаунт не существует");
            return "errorPage";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/update", method = RequestMethod.POST)
    public String doUpdateAccount(@PathVariable int accountId,
                                  @RequestParam(value = "deleteAvatar", required = false) String deleteAvatar,
                                  @ModelAttribute Account account,
                                  @SessionAttribute("loginAccount") Account loginAccount,
                                  Model model) {
        logger.info("Updating an account with an id='{}'", accountId);
        boolean isExceedingAvatarSize = account.getAvatar().length > IMG_MAX_SIZE;
        if (!isExceedingAvatarSize) {
            Account updatedAccount = accountService.getAccountWithPhonesById(accountId);
            if (deleteAvatar != null && deleteAvatar.equals("on")) {
                updatedAccount.setAvatar(null);
            }
            accountService.updateAccount(account, updatedAccount);
            return "redirect:/accounts/" + accountId;
        } else {
            model.addAttribute("account", account);
            model.addAttribute("errorSizeAvatar", "Размер выбранного файла превышает 65КБ");
            logger.warn("Error when updating an account with an id='{}': the size of the selected avatar file exceeds" +
                    " 65KB", accountId);
            return "updateAccount";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/auth/update", method = RequestMethod.GET)
    public String showUpdateAuthInfo(@PathVariable("accountId") int accountId,
                                     @SessionAttribute("loginAccount") Account loginAccount,
                                     Model model) {
        logger.info("Going to the account with id='{}' auth info update page", accountId);
        String email = accountService.getEmailByAccountId(accountId);
        if (email != null) {
            model.addAttribute("accountId", accountId);
            model.addAttribute("currentEmail", email);
            return "updateAuthInfo";
        } else {
            model.addAttribute("error", "Аккаунт не существует");
            logger.warn("Error when going to the account with id='{}' auth info update page: the account does not " +
                    "exist", accountId);
            return "errorPage";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/auth/update", method = RequestMethod.POST)
    public String doUpdateAuthInfo(@PathVariable int accountId,
                                   @RequestParam("currentEmail") String currentEmail,
                                   @RequestParam("newEmail") String newEmail,
                                   @RequestParam(value = "currentPassword", required = false) String currentPassword,
                                   @RequestParam("newPassword") String newPassword,
                                   @SessionAttribute("loginAccount") Account loginAccount,
                                   Model model) {
        if (accountId == loginAccount.getId()) {
            logger.info("Updating auth info an account with an id='{}'", accountId);
        } else {
            logger.info("Updating auth info an account with an id='{}' by an administrator with an id='{}'",
                    accountId, loginAccount.getId());
        }
        if (passwordEncoder.matches(currentPassword, loginAccount.getAuth().getPassword())) {
            Auth foundAuth = accountService.getAuthByEmail(newEmail);
            if (foundAuth == null || foundAuth.getAccount().getId() == accountId) {
                // my account
                if (accountId == loginAccount.getId()) {
                    accountService.updateAuthEmailAndPassword(loginAccount.getAuth(), newEmail,
                            passwordEncoder.encode(newPassword));
                    // someone else's account
                } else {
                    Auth auth = accountService.getAuthByEmail(currentEmail);
                    accountService.updateAuthEmailAndPassword(auth, newEmail, passwordEncoder.encode(newPassword));
                }
                return "redirect:/accounts/" + accountId;
            } else {
                logger.warn("Error when updating auth info an account with an id='{}': the entered email is already " +
                        "in use", accountId);
                model.addAttribute("errorEmail", "Введенный почтовый адрес уже используется");
            }
        } else {
            if (accountId == loginAccount.getId()) {
                logger.warn("Error when updating auth info an account with an id='{}': the current password is " +
                        "incorrect", accountId);
                model.addAttribute("errorPassword", "Неверно указан текущий пароль");
            } else {
                logger.warn("Error when updating auth info an account with an id='{}': the admin password is " +
                        "incorrect", accountId);
                model.addAttribute("errorPassword", "Неверно указан пароль администратора");
            }
        }
        model.addAttribute("accountId", accountId);
        model.addAttribute("currentEmail", currentEmail);
        model.addAttribute("newEmail", newEmail);
        return "updateAuthInfo";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/delete", method = RequestMethod.POST)
    public String doDeleteAccount(@PathVariable int accountId,
                                  @SessionAttribute("loginAccount") Account loginAccount,
                                  @CookieValue(value = "remember-me", required = false) Cookie rememberMe,
                                  HttpSession session) {
        logger.info("Deleting an account with an id='{}'", accountId);
        accountService.deleteAccountById(accountId);
        if (accountId == loginAccount.getId()) {
            session.invalidate();
            deleteCookie(rememberMe);
            return "redirect:/login";
        } else {
            return "redirect:/accounts";
        }
    }

    /* Friendships */

    @RequestMapping(value = "{accountId}/friendships", method = RequestMethod.GET)
    public String showFriendshipsAccount(@PathVariable int accountId,
                                         @RequestParam(value = "friendshipTab", required = false) String tab,
                                         @SessionAttribute("loginAccount") Account loginAccount,
                                         Model model) {
        logger.info("Going to the friendships page of the account with the id='{}'", accountId);
        if (loginAccount.getId() == accountId) {
            model.addAttribute("account", loginAccount);
            int qtyFriends = accountService.getFriendsQty(accountId);
            int qtyRequestsFrom = accountService.getRequestsFromQty(accountId);
            int qtyRequestsTo = accountService.getRequestsToQty(accountId);
            int qtyBlacklists = accountService.getBlacklistsQty(accountId);
            model.addAttribute("maxNumPageFriend",
                    qtyFriends % 5 == 0 && qtyFriends != 0 ? (qtyFriends / 5) - 1 : (qtyFriends / 5));
            model.addAttribute("maxNumPageRequestFrom",
                    qtyRequestsFrom % 5 == 0 && qtyRequestsFrom != 0 ? (qtyRequestsFrom / 5) - 1 :
                            (qtyRequestsFrom / 5));
            model.addAttribute("maxNumPageRequestTo",
                    qtyRequestsTo % 5 == 0 && qtyRequestsTo != 0 ? (qtyRequestsTo / 5) - 1 : (qtyRequestsTo / 5));
            model.addAttribute("maxNumPageBlacklist",
                    qtyBlacklists % 5 == 0 && qtyBlacklists != 0 ? (qtyBlacklists / 5) - 1 : (qtyBlacklists / 5));
        } else {
            Account account = accountService.getAccountById(accountId);
            if (account != null) {
                model.addAttribute("account", account);
                int qtyFriends = accountService.getFriendsQty(accountId);
                model.addAttribute("maxNumPageFriend",
                        qtyFriends % 5 == 0 && qtyFriends != 0 ? (qtyFriends / 5) - 1 : (qtyFriends / 5));
                model.addAttribute("maxNumPageRequestFrom", 0);
                model.addAttribute("maxNumPageRequestTo", 0);
                model.addAttribute("maxNumPageBlacklist", 0);
            } else {
                model.addAttribute("error", "Аккаунт не существует");
                logger.warn("Error when going to the friendships page of the account with the id='{}': the account " +
                        "does not exist", accountId);
                return "errorPage";
            }
        }
        if (tab != null) {
            if (FriendshipTab.valueOf(tab) == FriendshipTab.FRIENDS) {
                model.addAttribute("friendshipTab", "friends");
            } else if (FriendshipTab.valueOf(tab) == FriendshipTab.REQUESTS_FROM) {
                model.addAttribute("friendshipTab", "requestsFrom");
            } else if (FriendshipTab.valueOf(tab) == FriendshipTab.REQUESTS_TO) {
                model.addAttribute("friendshipTab", "requestsTo");
            } else {
                model.addAttribute("friendshipTab", "blacklists");
            }
        } else {
            model.addAttribute("friendshipTab", "friends");
        }
        return "friendshipsAccount";
    }

    @RequestMapping(value = "{accountId}/api/friendships/friends", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> getPaginationFriendsAjax(@PathVariable int accountId,
                                                  @RequestParam("numPage") int numPage) {
        logger.info("Pagination friends for accounts with an id='{}', page number='{}'", accountId, numPage);
        return accountService.getFriendsWithPagination(accountId, numPage);
    }

    @RequestMapping(value = "{accountId}/api/friendships/requests-from", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> getPaginationRequestsFromAjax(@PathVariable int accountId,
                                                       @RequestParam("numPage") int numPage) {
        logger.info("Pagination requests from for accounts with an id='{}', page number='{}'", accountId, numPage);
        return accountService.getRequestsFromWithPagination(accountId, numPage);
    }

    @RequestMapping(value = "{accountId}/api/friendships/requests-to", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> getPaginationRequestsToAjax(@PathVariable int accountId,
                                                     @RequestParam("numPage") int numPage) {
        logger.info("Pagination requests to for accounts with an id='{}', page number='{}'", accountId, numPage);
        return accountService.getRequestsToWithPagination(accountId, numPage);
    }

    @RequestMapping(value = "{accountId}/api/friendships/blacklists", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> getPaginationBlacklistsAjax(@PathVariable int accountId,
                                                     @RequestParam("numPage") int numPage) {
        logger.info("Pagination blacklists for accounts with an id='{}', page number='{}'", accountId, numPage);
        return accountService.getBlacklistsWithPagination(accountId, numPage);
    }

    @RequestMapping(value = "{accountId}/friendships/{friendId}/request", method = RequestMethod.POST)
    public String requestFriendship(@PathVariable int accountId,
                                    @PathVariable int friendId,
                                    @RequestParam("internetPage") String internetPage,
                                    @RequestParam(value = "friendshipTab", required = false) String friendshipTab,
                                    @SessionAttribute("loginAccount") Account loginAccount,
                                    HttpServletRequest request, Model model) {
        logger.info("Sending friend request by accounts from id='{}' to id='{}'", accountId, friendId);
        template.convertAndSend(new Event(loginAccount, accountService.getAccountById(friendId),
                EventType.FRIENDS_REQUEST));
        return actionOnFriendship(accountId, friendId, internetPage, friendshipTab, request, model);
    }

    @PreAuthorize("#loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/friendships/{friendId}/add", method = RequestMethod.POST)
    public String addFriend(@PathVariable int accountId,
                            @PathVariable int friendId,
                            @RequestParam("internetPage") String internetPage,
                            @RequestParam(value = "friendshipTab", required = false) String friendshipTab,
                            @SessionAttribute("loginAccount") Account loginAccount,
                            HttpServletRequest request, Model model) {
        logger.info("Accepting friend request by accounts from id='{}' to id='{}'", accountId, friendId);
        return actionOnFriendship(accountId, friendId, internetPage, friendshipTab, request, model);
    }

    @PreAuthorize("#loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/friendships/{friendId}/block", method = RequestMethod.POST)
    public String addToBlacklist(@PathVariable int accountId,
                                 @PathVariable int friendId,
                                 @RequestParam("internetPage") String internetPage,
                                 @RequestParam(value = "friendshipTab", required = false) String friendshipTab,
                                 @SessionAttribute("loginAccount") Account loginAccount,
                                 HttpServletRequest request, Model model) {
        logger.info("Adding to the blacklist by accounts from id='{}' to id='{}'", accountId, friendId);
        System.out.println(friendshipTab);
        return actionOnFriendship(accountId, friendId, internetPage, friendshipTab, request, model);
    }

    @PreAuthorize("#loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/friendships/{friendId}/delete", method = RequestMethod.POST)
    public String deleteFriendship(@PathVariable int accountId,
                                   @PathVariable int friendId,
                                   @RequestParam("internetPage") String internetPage,
                                   @RequestParam(value = "friendshipTab", required = false) String friendshipTab,
                                   @SessionAttribute("loginAccount") Account loginAccount,
                                   HttpServletRequest request, Model model) {
        logger.info("Deleting from friends/blacklists/orders by accounts from id='{}' to id='{}'", accountId, friendId);
        return actionOnFriendship(accountId, friendId, internetPage, friendshipTab, request, model);
    }

    /* Memberships */

    @RequestMapping(value = "{accountId}/memberships", method = RequestMethod.GET)
    public String showMembershipsAccount(@PathVariable int accountId,
                                         @RequestParam(value = "membershipAccountTab", required = false) String tab,
                                         Model model) {
        logger.info("Going to the memberships page of the account with the id='{}'", accountId);
        Account account = accountService.getAccountById(accountId);
        if (account != null) {
            model.addAttribute("account", account);
            model.addAttribute("orderGroups", sortGroupListByName(accountService.getRequestGroups(accountId)));
            model.addAttribute("userGroups", sortGroupListByName(accountService.getUserGroups(accountId)));
            model.addAttribute("moderatorGroups", sortGroupListByName(accountService.getModeratorGroups(accountId)));
            model.addAttribute("createdGroups", sortGroupListByName(accountService.getCreatedGroups(accountId)));
            if (tab != null) {
                model.addAttribute("membershipAccountTab", MembershipAccountTab.valueOf(tab));
            } else {
                model.addAttribute("membershipAccountTab", MembershipAccountTab.ALL_GROUPS);
            }
            return "membershipsAccount";
        } else {
            model.addAttribute("error", "Аккаунт не существует");
            logger.warn("Error when going to the memberships page of the account with the id='{}': the account does " +
                    "not exist", accountId);
            return "errorPage";
        }
    }

    /* Admin */

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "{accountId}/admin/assign", method = RequestMethod.POST)
    public String doAssignAdmin(@PathVariable int accountId,
                                Model model) {
        logger.info("Assign an account with an id='{}' as an administrator", accountId);
        Account account = accountService.getAccountById(accountId);
        if (account != null) {
            account.getAuth().setAdmin(true);
            accountService.updateAuth(account.getAuth());
            return "redirect:/accounts/" + accountId;
        } else {
            model.addAttribute("error", "Невозможно назначить администратором несуществующего пользователя");
            logger.warn("Error when assign an account with an id='{}' as an administrator: the account does not " +
                    "exist", accountId);
            return "errorPage";
        }
    }

    /* Interlocutor */

    @PreAuthorize("#loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/interlocutors", method = RequestMethod.GET)
    public String showInterlocutors(@PathVariable int accountId,
                                    @SessionAttribute("loginAccount") Account loginAccount,
                                    Model model) {
        logger.info("Going to the interlocutors page of the account with the id='{}'", accountId);
        List<Account> interlocutors = accountService.getInterlocutors(accountId);
        model.addAttribute("interlocutors", interlocutors);
        model.addAttribute("account", loginAccount);
        return "interlocutors";
    }

    @PreAuthorize("#loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/interlocutors/{interlocutorId}", method = RequestMethod.GET)
    public String showChat(@PathVariable int accountId,
                           @PathVariable int interlocutorId,
                           @SessionAttribute("loginAccount") Account loginAccount,
                           Model model) {
        logger.info("Going to the chat page between of the account with id='{}' and the interlocutor with id='{}'",
                accountId, interlocutorId);
        List<Message> chat = accountService.getChat(accountId, interlocutorId);
        model.addAttribute("interlocutor", accountService.getAccountById(interlocutorId));
        model.addAttribute("chat", chat);
        return "chat";
    }

    @PreAuthorize("#loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/interlocutors/{interlocutorId}/delete", method = RequestMethod.POST)
    public String deleteChat(@PathVariable int accountId,
                             @PathVariable int interlocutorId,
                             @SessionAttribute("loginAccount") Account loginAccount,
                             Model model) {
        logger.info("Delete chat between of the account with id='{}' and the interlocutor with id='{}'",
                accountId, interlocutorId);
        if (accountService.deleteChat(accountId, interlocutorId)) {
            return "redirect:/accounts/" + accountId + "/interlocutors";
        } else {
            model.addAttribute("error", "Между аккаунтами не существует чата");
            logger.warn("Error when going to the chat page between of the account with id='{}' and the " +
                    "interlocutor with id='{}': there is no chat between accounts", accountId, interlocutorId);
            return "errorPage";
        }
    }

    /* News */
    @PreAuthorize("#loginAccount.id == #accountId")
    @RequestMapping(value = "{accountId}/news", method = RequestMethod.GET)
    public String showNews(@PathVariable int accountId,
                           @SessionAttribute("loginAccount") Account loginAccount,
                           Model model) {
        logger.info("Going to the news of the account with id='{}'", accountId);
        Account account = accountService.getAccountById(accountId);
        long qty = accountService.getNewsQty(accountId);
        model.addAttribute("maxNumPage", qty % 5 == 0 && qty != 0 ? (qty / 5) - 1 : (qty / 5));
        model.addAttribute("account", account);
        return "news";
    }

    @RequestMapping(value = "{accountId}/news/api", method = RequestMethod.GET)
    @ResponseBody
    public List<News> getPaginationAllNews(@PathVariable int accountId,
                                               @RequestParam("numPage") int numPage) {
        logger.info("Pagination all news of the account with id='{}', page number='{}'", accountId, numPage);
        if (numPage < RedisConfig.SIZE_CACHE / 5) {
            if (Objects.equals(redisTemplate.hasKey("feed:" + accountId), false)) {
                List<News> cacheNews = accountService.getNewsCache(accountId, RedisConfig.SIZE_CACHE);
                for (News news : cacheNews) {
                    redisTemplate.opsForZSet().add("feed:" + accountId, news.getNewsId(), news.getNewsDate().getTime());
                }
            }
            Set<Integer> newsId = redisTemplate.opsForZSet().reverseRange("feed:" + accountId, numPage * 5,
                    numPage * 5 + 4);
            return accountService.getNews(newsId);
        } else {
            return accountService.getNews(accountId, numPage);
        }
    }


    private String actionOnFriendship(int loginId, int friendId, String internetPage,
                                      String friendshipTab, HttpServletRequest request, Model model) {
        String uri = request.getRequestURI();
        switch (uri.substring(uri.lastIndexOf('/'))) {
            case REQUEST_FRIENDSHIP_URI:
                accountService.createFriendshipRequest(loginId, friendId);
                break;
            case ADD_FRIEND_URI:
                accountService.acceptFriendship(loginId, friendId);
                break;
            case ADD_BLACKLIST_URI:
                accountService.addToBlacklist(loginId, friendId);
                break;
            case DELETE_FRIENDSHIP_URI:
                accountService.deleteFriendship(loginId, friendId);
                break;
        }
        switch (InternetPage.valueOf(internetPage)) {
            case ACCOUNT_PAGE:
                return "redirect:/accounts/" + friendId;
            case FRIENDSHIP_ACCOUNT_PAGE:
                return "redirect:/accounts/" + loginId + "/friendships?friendshipTab=" + friendshipTab;
            default:
                model.addAttribute("error", "Страница не найдена");
                logger.warn("Error when going to the target page of the account with the id='{}' after the change" +
                        " friendship", loginId);
                return "errorPage";
        }
    }

    private List<Group> sortGroupListByName(List<Group> groups) {
        groups.sort(Comparator.comparing(Group::getName, String::compareToIgnoreCase));
        return groups;
    }

}