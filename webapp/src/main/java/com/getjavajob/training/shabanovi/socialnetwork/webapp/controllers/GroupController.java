package com.getjavajob.training.shabanovi.socialnetwork.webapp.controllers;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.InternetPage;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MemberGroupTab;
import com.getjavajob.training.shabanovi.socialnetwork.service.AccountService;
import com.getjavajob.training.shabanovi.socialnetwork.service.GroupService;
import com.getjavajob.training.shabanovi.socialnetwork.webapp.utils.CustomStringEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("groups")
public class GroupController {

    private static final int IMG_MAX_SIZE = 65000;
    private static final int IMG_MIN_SIZE = 0;
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
    private final AccountService accountService;
    private final GroupService groupService;

    @Autowired
    public GroupController(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        binder.registerCustomEditor(String.class, new CustomStringEditor());
        CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true);
        binder.registerCustomEditor(Date.class, editor);
    }

    /* Groups */

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String showCreateGroup(Model model) {
        logger.info("Going to the new group create page");
        model.addAttribute("group", new Group());
        return "createGroup";
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public String doCreateGroup(@ModelAttribute Group group,
                                @SessionAttribute("loginAccount") Account loginAccount,
                                Model model) {
        logger.info("Creating new group with name='{}'", group.getName());
        boolean isUniqueGroupName = groupService.getGroupByName(group.getName()) == null;
        boolean isExceedingImageSize = group.getImage().length > IMG_MAX_SIZE;
        if (isUniqueGroupName && !isExceedingImageSize) {
            if (group.getImage().length == IMG_MIN_SIZE) {
                group.setImage(null);
            }
            group.setCreator(loginAccount);
            group = groupService.createGroup(group);
            return "redirect:/groups/" + group.getId();
        } else {
            model.addAttribute("group", group);
            if (!isUniqueGroupName) {
                model.addAttribute("errorName", "Название группы уже используется");
                logger.warn("Error when creating a new group with name='{}': the group name already exists",
                        group.getName());
            }
            if (isExceedingImageSize) {
                model.addAttribute("errorSizeImage", "Размер выбранного файла превышает 65КБ");
                logger.warn("Error when creating a new group with name='{}': the size of the selected group image " +
                        "file exceeds 65KB", group.getName());
            }
            return "createGroup";
        }
    }

    @RequestMapping(value = "{groupId}", method = RequestMethod.GET)
    public String showGroup(@PathVariable int groupId,
                            @SessionAttribute("loginAccount") Account loginAccount,
                            Model model) {
        logger.info("Going to the home page of the group with the id='{}'", groupId);
        Group group = groupService.getGroupWithCreator(groupId);
        if (group != null) {
            model.addAttribute("group", group);
            model.addAttribute("membershipStatus", accountService.getMembershipStatus(loginAccount.getId(), groupId));
            model.addAttribute("wallMessages", groupService.getWallMessages(groupId));
            return "group";
        } else {
            model.addAttribute("error", "Группа не существует");
            logger.warn("Error when going to the home page of an group with the id='{}': the group does not exist",
                    groupId);
            return "errorPage";
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showAllGroups(Model model) {
        logger.info("Going to the page with all groups");
        int qty = groupService.getAllGroupsQty();
        model.addAttribute("maxNumPage", qty % 5 == 0 ? (qty / 5) - 1 : (qty / 5));
        return "allGroups";
    }

    @RequestMapping(value = "api", method = RequestMethod.GET)
    @ResponseBody
    public List<Group> getPaginationAllGroups(@RequestParam("numPage") int numPage) {
        logger.info("Pagination all groups, page number='{}'", numPage);
        return groupService.getAllGroups(numPage);
    }

    @PreAuthorize("@accountService.getMembershipStatus(#loginAccount.getId(), #groupId) ==" +
            " T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus).CREATOR")
    @RequestMapping(value = "{groupId}/update", method = RequestMethod.GET)
    public String showUpdateGroup(@PathVariable int groupId,
                                  @SessionAttribute("loginAccount") Account loginAccount,
                                  Model model) {
        logger.info("Going to the group with id='{}' update page", groupId);
        Group group = groupService.getGroupById(groupId);
        if (group != null) {
            model.addAttribute("group", group);
            return "updateGroup";
        } else {
            model.addAttribute("error", "Группа не существует");
            logger.warn("Error when going to the group with id='{}' update page: the group does not exist",
                    groupId);
            return "errorPage";
        }
    }

    @PreAuthorize("@accountService.getMembershipStatus(#loginAccount.getId(), #groupId) ==" +
            " T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus).CREATOR")
    @RequestMapping(value = "{groupId}/update", method = RequestMethod.POST)
    public String doUpdateGroup(@PathVariable int groupId,
                                @RequestParam(value = "deleteImageGroup", required = false) String deleteImageGroup,
                                @ModelAttribute Group group,
                                @SessionAttribute("loginAccount") Account loginAccount,
                                Model model) {
        logger.info("Updating an group with an id='{}'", groupId);
        Group foundGroup = groupService.getGroupByName(group.getName());
        boolean isUniqueOrSameGroup = foundGroup == null || group.getId() == foundGroup.getId();
        boolean isExceedingImageSize = group.getImage().length > IMG_MAX_SIZE;
        if (isUniqueOrSameGroup && !isExceedingImageSize) {
            if (deleteImageGroup != null && deleteImageGroup.equals("on")) {
                group.setImage(null);
            } else if (group.getImage().length == IMG_MIN_SIZE) {
                group.setImage(groupService.getGroupById(group.getId()).getImage());
            }
            groupService.updateGroup(group);
            return "redirect:/groups/" + groupId;
        } else {
            model.addAttribute("group", group);
            if (!isUniqueOrSameGroup) {
                model.addAttribute("errorName", "Группа с указанным именем уже существует");
                logger.warn("Error when updating an group with an id='{}': the group with the specified name already " +
                        "exists", groupId);
            }
            if (isExceedingImageSize) {
                model.addAttribute("errorSizeImage", "Размер выбранного файла превышает 65КБ");
                logger.warn("Error when updating an group with an id='{}': the size of the selected image file " +
                        "exceeds 65KB", groupId);
            }
            return "updateGroup";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or @accountService.getMembershipStatus(#loginAccount.getId(), #groupId) ==" +
            " T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus).CREATOR")
    @RequestMapping(value = "{groupId}/delete", method = RequestMethod.POST)
    public String doDeleteGroup(@PathVariable int groupId,
                                @RequestParam(value = "accountId", required = false) Integer accountId,
                                @RequestParam("internetPage") String internetPage,
                                @RequestParam(value = "membershipAccountTab", required = false) String membershipAccountTab,
                                @SessionAttribute("loginAccount") Account loginAccount,
                                Model model) {
        logger.info("Deleting an group with an id='{}'", groupId);
        groupService.deleteGroupById(groupId);
        switch (InternetPage.valueOf(internetPage)) {
            case MEMBERSHIP_ACCOUNT_PAGE:
                return "redirect:/accounts/" + accountId + "/memberships?membershipAccountTab=" + membershipAccountTab;
            case GROUP_PAGE:
                return "redirect:/groups";
            default:
                model.addAttribute("error", "Страница не найдена");
                return "errorPage";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #loginAccount.id == #accountId")
    @RequestMapping(value = "created/delete", method = RequestMethod.POST)
    public String doDeleteCreatedGroup(@RequestParam("accountId") int accountId,
                                       @SessionAttribute("loginAccount") Account loginAccount) {
        logger.info("Deleting all created groups for account with id='{}'", accountId);
        groupService.deleteCreatedGroupByAccountId(accountId);
        return "redirect:/accounts/" + accountId + "/memberships";
    }

    /* Members */

    @RequestMapping(value = "{groupId}/members", method = RequestMethod.GET)
    public String showMembersGroup(@PathVariable int groupId,
                                   @RequestParam(value = "memberGroupTab", required = false) String tab,
                                   @SessionAttribute("loginAccount") Account loginAccount,
                                   Model model) {
        logger.info("Going to the members page of the group with id='{}'", groupId);
        model.addAttribute("group", groupService.getGroupById(groupId));
        model.addAttribute("orders", sortAccountListBySurnameAndName(groupService.getOrders(groupId)));
        model.addAttribute("users", sortAccountListBySurnameAndName(groupService.getUsers(groupId)));
        model.addAttribute("moderators", sortAccountListBySurnameAndName(groupService.getModerators(groupId)));
        model.addAttribute("membershipStatus", accountService.getMembershipStatus(loginAccount.getId(), groupId));
        if (tab != null) {
            model.addAttribute("memberGroupTab", MemberGroupTab.valueOf(tab));
        } else {
            model.addAttribute("memberGroupTab", MemberGroupTab.ALL_MEMBERS);
        }
        return "membersGroup";
    }

    @RequestMapping(value = "{groupId}/members/request", method = RequestMethod.POST)
    public String doRequestMembership(@PathVariable int groupId,
                                      @SessionAttribute("loginAccount") Account loginAccount) {
        logger.info("Sending member request from account with id='{}' to group with id='{}'",
                loginAccount.getId(), groupId);
        groupService.createMembershipRequest(groupId, loginAccount.getId());
        return "redirect:/groups/" + groupId;
    }

    @PreAuthorize("@accountService.getMembershipStatus(#loginAccount.getId(), #groupId) ==" +
            " T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus).CREATOR or " +
            "@accountService.getMembershipStatus(#loginAccount.getId(), #groupId) == T(com.getjavajob.training" +
            ".shabanovi.socialnetwork.common.enums.MembershipStatus).MODERATOR")
    @RequestMapping(value = "{groupId}/members/{accountId}/add", method = RequestMethod.POST)
    public String doAddMember(@PathVariable int groupId,
                              @PathVariable int accountId,
                              @RequestParam("memberGroupTab") String memberGroupTab,
                              @SessionAttribute("loginAccount") Account loginAccount,
                              Model model) {
        logger.info("Adding member from account with id='{}' to group with id='{}'", accountId, groupId);
        groupService.acceptMembership(groupId, accountId);
        model.addAttribute("memberGroupTab", MemberGroupTab.valueOf(memberGroupTab));
        return "redirect:/groups/" + groupId + "/members?memberGroupTab=" + memberGroupTab;
    }

    @PreAuthorize("#loginAccount.id == #accountId or " +
            "@accountService.getMembershipStatus(#loginAccount.getId(), #groupId) == " +
            "T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus).CREATOR or " +
            "@accountService.getMembershipStatus(#loginAccount.getId(), #groupId) == " +
            "T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus).MODERATOR")
    @RequestMapping(value = "{groupId}/members/{accountId}/delete", method = RequestMethod.POST)
    public String doDeleteMembership(@PathVariable int groupId,
                                     @PathVariable Integer accountId,
                                     @RequestParam("internetPage") String internetPage,
                                     @RequestParam(value = "membershipAccountTab", required = false) String membershipAccountTab,
                                     @RequestParam(value = "memberGroupTab", required = false) String memberGroupTab,
                                     @SessionAttribute("loginAccount") Account loginAccount,
                                     Model model, HttpSession session) {
        logger.info("Deleting member an account with id='{}' from group with='{}'", accountId, groupId);
        groupService.deleteMembership(groupId, accountId);
        switch (InternetPage.valueOf(internetPage)) {
            case MEMBERSHIP_ACCOUNT_PAGE:
                return "redirect:/accounts/" + accountId + "/memberships?membershipAccountTab=" + membershipAccountTab;
            case MEMBER_GROUP_PAGE:
                return "redirect:/groups/" + groupId + "/members?memberGroupTab=" + memberGroupTab;
            case GROUP_PAGE:
                return "redirect:/groups/" + groupId;
            default:
                model.addAttribute("error", "Страница не найдена");
                return "errorPage";
        }
    }

    /* Creator */

    @PreAuthorize("hasRole('ROLE_ADMIN') or @accountService.getMembershipStatus(#loginAccount.getId(), #groupId) ==" +
            " T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus).CREATOR")
    @RequestMapping(value = "{groupId}/creator/{accountId}/assign", method = RequestMethod.POST)
    public String doAssignCreator(@PathVariable int groupId,
                                  @PathVariable int accountId,
                                  @RequestParam("creatorId") int creatorId,
                                  @RequestParam("memberGroupTab") String memberGroupTab,
                                  @SessionAttribute("loginAccount") Account loginAccount) {
        logger.info("Assign an account with id='{}' creator of a group with='{}'", accountId, groupId);
        groupService.assignCreator(accountId, creatorId, groupId);
        return "redirect:/groups/" + groupId + "/members?memberGroupTab=" + memberGroupTab;
    }

    /* Moderator */

    @PreAuthorize("@accountService.getMembershipStatus(#loginAccount.getId(), #groupId) ==" +
            " T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus).CREATOR")
    @RequestMapping(value = "{groupId}/moderator/{accountId}/assign", method = RequestMethod.POST)
    public String doAssignModerator(@PathVariable int groupId,
                                    @PathVariable int accountId,
                                    @RequestParam(value = "memberGroupTab") String memberGroupTab,
                                    @SessionAttribute("loginAccount") Account loginAccount) {
        logger.info("Assign an account with id='{}' moderator of a group with='{}'", accountId, groupId);
        groupService.assignModerator(groupId, accountId);
        return "redirect:/groups/" + groupId + "/members?memberGroupTab=" + memberGroupTab;
    }

    @PreAuthorize("@accountService.getMembershipStatus(#loginAccount.getId(), #groupId) ==" +
            " T(com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus).CREATOR")
    @RequestMapping(value = "{groupId}/moderator/{accountId}/downgrade", method = RequestMethod.POST)
    public String doDowngradeModerator(@PathVariable int groupId,
                                       @PathVariable int accountId,
                                       @RequestParam(value = "memberGroupTab") String memberGroupTab,
                                       @SessionAttribute("loginAccount") Account loginAccount) {
        logger.info("Downgrade an account with id='{}' to user of a group with='{}'", accountId, groupId);
        groupService.downgradeToUser(groupId, accountId);
        return "redirect:/groups/" + groupId + "/members?memberGroupTab=" + memberGroupTab;
    }

    private List<Account> sortAccountListBySurnameAndName(List<Account> accounts) {
        accounts.sort(Comparator.comparing(Account::getSurname,
                Comparator.nullsLast(String::compareToIgnoreCase)).thenComparing(Account::getName,
                String::compareToIgnoreCase));
        return accounts;
    }

}