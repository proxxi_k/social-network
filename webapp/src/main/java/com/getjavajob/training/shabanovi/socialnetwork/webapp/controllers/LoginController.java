package com.getjavajob.training.shabanovi.socialnetwork.webapp.controllers;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showLogin(@SessionAttribute(value = "loginAccount", required = false) Account loginAccount) {
        logger.info("Going to the root application");
        if (loginAccount != null) {
            return "redirect:accounts/" + loginAccount.getId();
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin(@RequestParam(value = "error", required = false) String error,
                            @RequestParam(value = "email", required = false) String email,
                            @RequestParam(value = "logout", required = false) String logout,
                            Model model) {
        logger.info("Going to the login page");
        if (error != null) {
            logger.info("The username or password for the account with email: '{}' is incorrectly specified", email);
            model.addAttribute("errorLogin", "Неправильный email или пароль");
            model.addAttribute("email", email);
        } else if (logout != null) {
            logger.info("Successful logout");
            model.addAttribute("logout", "Вы успешно вышли из системы");
        }
        return "login";
    }

}