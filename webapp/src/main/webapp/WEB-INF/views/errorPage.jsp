<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <title>Error Page</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">

        <script src="http://code.jquery.com/jquery-1.11.0.js"></script>
        <style>

            .input-group {
                display: flex;
                width: 90%;
                margin: 0 5% 15px 5%;
            }

            h4 {
                text-align: center;
                margin: 10px 0 15px 0;
            }

            h8 {
                width: 90%;
                margin: 0 5% 15px 5%;
            }

            .border-container {
                border: 3px solid #f1f1f1;
                width: 34%;
                margin: 60px 33% 0 33%;
                padding-top: 10px;
            }

            .alert-danger {
                width: 100%;
                height: 50px;
            }

            .alert {
                margin-bottom: 0;
            }

        </style>

    </head>
    <body>
        <div class="border-container">
            <h4>Сообщение об ошибке</h4>
            <div class="form-group input-group">
                <div class="alert alert-danger">
                    <p>${error}</p>
                </div>
            </div>
            <h8>* обратитесь к администратору для решения проблемы</h8>
        </div>
    </body>
</html>