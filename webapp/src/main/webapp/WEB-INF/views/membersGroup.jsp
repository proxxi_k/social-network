<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.InternetPage" %>
<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.MemberGroupTab" %>
<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <title>Members Group</title>
        <script src="${pageContext.request.contextPath}/resources/js/script.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">
        <style>
            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .delButton {
                background: none !important;
                border: none;
                padding: 0 !important;
                color: blue;
                cursor: pointer;
                font-size: 14px;
            }

            * {
                text-decoration: none;
            }

            /* Стиль вкладок */
            .tab {
                overflow: hidden;
                border: 1px solid #ccc;
                background-color: #f1f1f1;
                width: 60%;
                margin: 0 auto;
            }

            /* Стиль кнопок внутри вкладок */
            .tab button {
                background-color: inherit;
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 10px 10px;
                transition: 0.3s;
            }

            /* Изменение цвета фона кнопок при наведении курсора мыши */
            .tab button:hover {
                background-color: #ddd;
            }


            /* Стиль содержимого вкладки */
            .tabcontent {
                display: none;
                border: 1px solid #ccc;
                width: 60%;
                margin: 0 auto;
            }

            .controls {
                float: right;
                align-self: center;

            }

            .login-container {
                border: 1px solid #ccc;
                background-color: #eee;
                border-radius: 5px;
                margin: 10px 10px 10px 10px;
            }

            .login-container::after {
                content: "";
                clear: both;
                display: table;
            }

            .login-container img {
                float: left;
                margin-left: 20px;
                border-radius: 50%;
                padding: 10px 0 10px 0;
            }

            .login-container span {
                font-size: 20px;
                margin-right: 15px;
            }

            .tabcontent a:active, /* активная/посещенная ссылка */
            .tabcontent a:hover, /* при наведении */
            .tabcontent a {
                text-decoration: none;
                color: #0000FF;
            }

            .friendName {
                display: flex;
                align-self: center;
            }

            .form-group {
                margin: 10px 10px 10px 10px;
            }

            .info {
                display: table;
                margin: 0 auto;
                font-size: 20px;
                padding: 10px 0 10px 0;
            }

        </style>
    </head>
    <body>
        <%-- Page scope --%>
        <c:set var="internetPage" value="<%=InternetPage.MEMBER_GROUP_PAGE%>" scope="page"/>

        <c:set var="all_members_tab" value="<%=MemberGroupTab.ALL_MEMBERS%>" scope="page"/>
        <c:set var="users_tab" value="<%=MemberGroupTab.USERS%>" scope="page"/>
        <c:set var="moderators_tab" value="<%=MemberGroupTab.MODERATORS%>" scope="page"/>
        <c:set var="requests_tab" value="<%=MemberGroupTab.REQUESTS%>" scope="page"/>
        <c:set var="creator_tab" value="<%=MemberGroupTab.CREATOR%>" scope="page"/>

        <c:set var="role_creator" value="<%=MembershipStatus.CREATOR%>" scope="page"/>
        <c:set var="role_moderator" value="<%=MembershipStatus.MODERATOR%>" scope="page"/>
        <c:set var="role_user" value="<%=MembershipStatus.USER%>" scope="page"/>
        <c:set var="role_request" value="<%=MembershipStatus.REQUEST%>" scope="page"/>

        <%-- Heading --%>
        <div class="info">
            <b>Список участников группы:
                <a href=" <c:url value="/groups/${group.id}"/>">${group.name}</a></b>
        </div>

        <%-- Memberships tab bar --%>
        <c:choose>
            <%-- Creator/Moderator --%>
            <c:when test="${membershipStatus == role_creator || membershipStatus == role_moderator ||
            loginAccount.auth.admin}">
                <div class="tab">
                    <button class="tablinks" onclick="openTab(event, '${all_members_tab}')">Все участники</button>
                    <button class="tablinks" onclick="openTab(event, '${users_tab}')">Участники</button>
                    <button class="tablinks" onclick="openTab(event, '${moderators_tab}')">Модераторы</button>
                    <button class="tablinks" onclick="openTab(event, '${creator_tab}')">Создатель группы</button>
                    <button class="tablinks" onclick="openTab(event, '${requests_tab}')">Заявки</button>
                </div>
            </c:when>
            <%-- User/Guest --%>
            <c:otherwise>
                <div class="tab">
                    <button class="tablinks" onclick="openTab(event, '${all_members_tab}')">Участники</button>
                </div>
            </c:otherwise>
        </c:choose>

        <%-- All members group tab --%>
        <div id="${all_members_tab}" class="tabcontent">
            <%-- User --%>
            <c:forEach var="user" items="${users}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/avatar/${user.id}"/>" alt="Аватар" style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/accounts/${user.id}"/>">${user.name} ${user.surname} (участник)</a>
                    </div>
                    <div class="col-md-3 controls">
                            <%-- Creator/Admin controls --%>
                        <c:if test="${membershipStatus == role_creator || loginAccount.auth.admin}">
                            <form action="/groups/${group.id}/creator/${user.id}/assign" method="post">
                                <input type="hidden" name="creatorId" value="${group.creator.id}">
                                <input type="hidden" name="memberGroupTab" value="${all_members_tab}">
                                <button type="submit" class="delButton">Назначить владельцем</button>
                            </form>
                        </c:if>
                            <%-- Creator controls --%>
                        <c:if test="${membershipStatus == role_creator}">
                            <form action="/groups/${group.id}/moderator/${user.id}/assign" method="post">
                                <input type="hidden" name="memberGroupTab" value="ALL_MEMBERS">
                                <button type="submit" class="delButton">Назначить модератором</button>
                            </form>
                        </c:if>
                            <%-- Moderator/Creator controls --%>
                        <c:if test="${membershipStatus == role_creator || membershipStatus == role_moderator}">
                            <form action="/groups/${group.id}/members/${user.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="memberGroupTab" value="${all_members_tab}">
                                <button type="submit" class="delButton">Удалить из группы</button>
                            </form>
                        </c:if>
                            <%-- My user controls --%>
                        <c:if test="${user.id == loginAccount.id}">
                            <form action="/groups/${group.id}/members/${user.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="memberGroupTab" value="${all_members_tab}">
                                <button type="submit" class="delButton">Выйти из группы</button>
                            </form>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
            <%-- Moderator --%>
            <c:forEach var="moderator" items="${moderators}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/avatar/${moderator.id}"/>" alt="Аватар"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/accounts/${moderator.id}"/>">${moderator.name} ${moderator.surname}
                            (модератор)</a>
                    </div>
                    <div class="col-md-3 controls">
                            <%-- Creator/Admin controls --%>
                        <c:if test="${membershipStatus == role_creator || loginAccount.auth.admin}">
                            <form action="/groups/${group.id}/creator/${moderator.id}/assign" method="post">
                                <input type="hidden" name="creatorId" value="${group.creator.id}">
                                <input type="hidden" name="memberGroupTab" value="${all_members_tab}">
                                <button type="submit" class="delButton">Назначить владельцем</button>
                            </form>
                        </c:if>
                            <%-- Creator controls --%>
                        <c:if test="${membershipStatus == role_creator}">
                            <form action="/groups/${group.id}/moderator/${moderator.id}/downgrade" method="post">
                                <input type="hidden" name="memberGroupTab" value="${all_members_tab}">
                                <button type="submit" class="delButton">Понизить до участника</button>
                            </form>
                            <form action="/groups/${group.id}/members/${moderator.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="memberGroupTab" value="${all_members_tab}">
                                <button type="submit" class="delButton">Удалить из группы</button>
                            </form>
                        </c:if>
                            <%-- My moderator controls --%>
                        <c:if test="${moderator.id == loginAccount.id}">
                            <form action="/groups/${group.id}/members/${moderator.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="memberGroupTab" value="${all_members_tab}">
                                <button type="submit" class="delButton">Выйти из группы</button>
                            </form>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
            <%-- Creator --%>
            <div class="row login-container">
                <div class="col-md-2">
                    <img src="<c:url value="/images/avatar/${group.creator.id}"/>" alt="Аватар"
                         style="width:90px">
                </div>
                <div class="col-md-7 friendName">
                    <a href="<c:url value="/accounts/${group.creator.id}"/>">${group.creator.name}
                        ${group.creator.surname} (создатель)</a>
                </div>
            </div>
        </div>
        <%-- Users tab --%>
        <div id="${users_tab}" class="tabcontent">
            <c:if test="${empty users}">
                <div class="form-group">
                    <div class="alert alert-danger">
                        <p>В группе пока нет ни одного участника</p>
                    </div>
                </div>
            </c:if>
            <%-- User --%>
            <c:forEach var="user" items="${users}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/avatar/${user.id}"/>" alt="Аватар" style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/accounts/${user.id}"/>">${user.name} ${user.surname} (участник)</a>
                    </div>
                    <div class="col-md-3 controls">
                            <%-- Creator/Admin controls --%>
                        <c:if test="${membershipStatus == role_creator || loginAccount.auth.admin}">
                            <form action="/groups/${group.id}/creator/${user.id}/assign" method="post">
                                <input type="hidden" name="creatorId" value="${group.creator.id}">
                                <input type="hidden" name="memberGroupTab" value="${users_tab}">
                                <button type="submit" class="delButton">Назначить владельцем</button>
                            </form>
                        </c:if>
                            <%-- Creator controls --%>
                        <c:if test="${membershipStatus == role_creator}">
                            <form action="/groups/${group.id}/moderator/${user.id}/assign" method="post">
                                <input type="hidden" name="memberGroupTab" value="${users_tab}">
                                <button type="submit" class="delButton">Назначить модератором</button>
                            </form>
                        </c:if>
                            <%-- Moderator/Creator controls --%>
                        <c:if test="${membershipStatus == role_creator || membershipStatus == role_moderator}">
                            <form action="/groups/${group.id}/members/${user.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="memberGroupTab" value="${users_tab}">
                                <button type="submit" class="delButton">Удалить из группы</button>
                            </form>
                        </c:if>
                            <%-- My user controls --%>
                        <c:if test="${user.id == loginAccount.id}">
                            <form action="/groups/${group.id}/members/${user.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="memberGroupTab" value="${users_tab}">
                                <button type="submit" class="delButton">Выйти из группы</button>
                            </form>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
        </div>
        <%-- Moderators tab --%>
        <div id="${moderators_tab}" class="tabcontent">
            <c:if test="${empty moderators}">
                <div class="form-group">
                    <div class="alert alert-danger">
                        <p>В группе пока нет ни одного модератора</p>
                    </div>
                </div>
            </c:if>
            <c:forEach var="moderator" items="${moderators}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/avatar/${moderator.id}"/>" alt="Аватар"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/accounts/${moderator.id}"/>">${moderator.name} ${moderator.surname}
                            (модератор)</a>
                    </div>
                    <div class="col-md-3 controls">
                            <%-- Creator/Admin controls --%>
                        <c:if test="${membershipStatus == role_creator || loginAccount.auth.admin}">
                            <form action="/groups/${group.id}/creator/${moderator.id}/assign" method="post">
                                <input type="hidden" name="creatorId" value="${group.creator.id}">
                                <input type="hidden" name="memberGroupTab" value="${moderators_tab}">
                                <button type="submit" class="delButton">Назначить владельцем</button>
                            </form>
                        </c:if>
                            <%-- Creator controls --%>
                        <c:if test="${membershipStatus == role_creator}">
                            <form action="/groups/${group.id}/moderator/${moderator.id}/downgrade" method="post">
                                <input type="hidden" name="memberGroupTab" value="MODERATORS">
                                <button type="submit" class="delButton">Понизить до участника</button>
                            </form>
                            <form action="/groups/${group.id}/members/${moderator.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="memberGroupTab" value="${moderators_tab}">
                                <button type="submit" class="delButton">Удалить из группы</button>
                            </form>
                        </c:if>
                            <%-- My moderator controls --%>
                        <c:if test="${moderator.id == loginAccount.id}">
                            <form action="/groups/${group.id}/members/${moderator.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="memberGroupTab" value="${moderators_tab}">
                                <button type="submit" class="delButton">Выйти из группы</button>
                            </form>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
        </div>
        <%-- Creator tab --%>
        <div id="${creator_tab}" class="tabcontent">
            <div class="row login-container">
                <div class="col-md-2">
                    <img src="<c:url value="/images/avatar/${group.creator.id}"/>" alt="Аватар"
                         style="width:90px">
                </div>
                <div class="col-md-7 friendName">
                    <a href="<c:url value="/accounts/${group.creator.id}"/>">${group.creator.name}
                        ${group.creator.surname} (создатель)</a>
                </div>
            </div>
        </div>
        <%-- Orders tab --%>
        <div id="${requests_tab}" class="tabcontent">
            <c:if test="${empty orders}">
                <div class="form-group">
                    <div class="alert alert-danger">
                        <p>В группе пока не поступило ни одной заявки</p>
                    </div>
                </div>
            </c:if>
            <c:forEach var="order" items="${orders}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/avatar/${order.id}"/>" alt="Аватар"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/accounts/${order.id}"/>">${order.name} ${order.surname} (участник)</a>
                    </div>
                        <%-- Creator/moderator controls --%>
                    <div class="col-md-3 controls">
                        <c:if test="${membershipStatus == role_creator || membershipStatus == role_moderator}">
                            <form action="/groups/${group.id}/members/${order.id}/add" method="post">
                                <input type="hidden" name="memberGroupTab" value="${requests_tab}">
                                <button type="submit" class="delButton">Принять в группу</button>
                            </form>
                            <form action="/groups/${group.id}/members/${order.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="memberGroupTab" value="${requests_tab}">
                                <button type="submit" class="delButton">Отклонить заявку</button>
                            </form>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
        </div>

        <script>
            window.onload(openTab(event, '${memberGroupTab}'));
        </script>

    </body>
</html>