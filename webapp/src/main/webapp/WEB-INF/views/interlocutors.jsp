<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <title>Interlocutors</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">

        <style>

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .delButton {
                background: none !important;
                border: none;
                padding: 0 !important;
                color: blue;
                cursor: pointer;
                font-size: 14px;
            }

            * {
                text-decoration: none;
            }

            .controls {
                float: right;
                align-self: center;

            }

            .login-container {
                border: 1px solid #ccc;
                background-color: #eee;
                border-radius: 5px;
                width: 60%;
                margin: 10px auto;
            }

            .login-container::after {
                content: "";
                clear: both;
                display: table;
            }

            .login-container img {
                float: left;
                margin-left: 20px;
                border-radius: 50%;
                padding: 10px 0 10px 0;
            }

            .login-container span {
                font-size: 20px;
                margin-right: 15px;
            }

            .tabcontent a:active, /* активная/посещенная ссылка */
            .tabcontent a:hover, /* при наведении */
            .tabcontent a {
                text-decoration: none;
                color: #0000FF;
            }

            .friendName {
                display: flex;
                align-self: center;
            }

            .info {
                display: table;
                margin: 0 auto;
                font-size: 20px;
                padding: 10px 0 10px 0;
            }

            .controls a {
                font-size: 14px;
            }

            .form-group {
                width: 60%;
                margin: 0 auto;
            }

        </style>

    </head>
    <body>
        <%-- Heading --%>
        <div class="info">
            <b>Сообщения пользователя:
                <a href=" <c:url value="/accounts/${account.id}"/>">${account.surname} ${account.name}</a></b>
        </div>

        <c:if test="${empty interlocutors}">
            <div class="form-group">
                <div class="alert alert-danger">
                    <p>У вас пока нет ни одного сообщения</p>
                </div>
            </div>
        </c:if>

        <c:forEach var="interlocutor" items="${interlocutors}">
            <div class="row login-container">
                <div class="col-md-2">
                    <img src="<c:url value="/images/avatar/${interlocutor.id}"/>" alt="Аватар"
                         style="width:90px">
                </div>
                <div class="col-md-8 friendName">
                    <a href="<c:url value="/accounts/${interlocutor.id}"/>">${interlocutor.name}
                            ${interlocutor.surname}</a>
                </div>
                <div class="col-md-2 controls">
                    <p><a
                            href="<c:url value="/accounts/${loginAccount.id}/interlocutors/${interlocutor.id}"/>">Зайти
                        в чат</a></p>
                    <form action="/accounts/${loginAccount.id}/interlocutors/${interlocutor.id}/delete"
                          method="post">
                        <button type="submit" class="delButton">Удалить чат</button>
                    </form>
                </div>
            </div>
        </c:forEach>
    </body>
</html>