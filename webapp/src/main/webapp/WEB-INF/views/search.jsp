<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <title>Search</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="${pageContext.request.contextPath}/resources/js/script.js"></script>
        <style>

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .containerAccount {
                border: 1px solid #ccc;
                background-color: #eee;
                border-radius: 5px;
                margin: 10px 10px 10px 10px;
            }

            .containerAccount::after {
                content: "";
                clear: both;
                display: table;
            }

            .friendName {
                display: flex;
                align-self: center;
            }

            .containerAccount img {
                float: left;
                margin-left: 20px;
                border-radius: 50%;
                padding: 10px 0 10px 0;
            }

            .containerAccount span {
                font-size: 20px;
                margin-right: 15px;
            }

            .containerGroup {
                border: 1px solid #ccc;
                background-color: #eee;
                border-radius: 5px;
                margin: 10px 10px 10px 10px;
            }

            .containerGroup::after {
                content: "";
                clear: both;
                display: table;
            }

            .friendName {
                display: flex;
                align-self: center;
            }

            .containerGroup img {
                float: left;
                margin-left: 20px;
                border-radius: 50%;
                padding: 10px 0 10px 0;
            }

            .containerGroup span {
                font-size: 20px;
                margin-right: 15px;
            }

            .next, .previous {
                position: absolute;
                bottom: 10px;
            }

            .previous {
                left: 35%;
            }

            .next {
                right: 35%;
            }

            /* Кнопки стиля */
            .btn {
                background-color: DodgerBlue; /* Синий фон */
                border: none; /* Удалить границы */
                color: white; /* Белый текст */
                padding: 12px 16px; /* Немного отступов */
                font-size: 16px; /* Установите размер шрифта */
                cursor: pointer; /* Указатель мыши при наведении */
            }

            /* Более темный фон при наведении курсора мыши */
            .btn:hover {
                background-color: #56abfd;
                color: white;
            }

            .border-container {
                width: 50%;
                margin: 0 25% 0 25%;
                height: 680px;
                position: relative;
                padding-bottom: 50px;
            }

            .info {
                display: table;
                margin: 0 auto;
                font-size: 20px;
                padding: 10px 0 10px 0;
            }

            /* Стиль вкладок */
            .tab {
                overflow: hidden;
                border: 1px solid #ccc;
                background-color: #f1f1f1;
                width: 50%;
                margin: 0 auto;
            }

            /* Стиль кнопок внутри вкладок */
            .tab button {
                background-color: inherit;
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 10px 10px;
                transition: 0.3s;
            }

            /* Изменение цвета фона кнопок при наведении курсора мыши */
            .tab button:hover {
                background-color: #ddd;
            }


            /* Стиль содержимого вкладки */
            .tabcontent {
                display: none;
                border: 1px solid #ccc;
                width: 50%;
                margin: 0 auto;
                height: 650px;
                position: relative;
                padding-bottom: 50px;
            }

            .tabcontent a:active, /* активная/посещенная ссылка */
            .tabcontent a:hover, /* при наведении */
            .tabcontent a {
                text-decoration: none;
                color: #0000FF;
            }

            .form-group {
                margin: 10px 10px 10px 10px;
            }

        </style>

    </head>
    <body>
        <%-- Heading --%>
        <div class="info">
            <b>Результаты поиска : "${search}"</b>
        </div>

        <%--   Search tab bar--%>
        <div class="tab">
            <button class="tablinks" onclick="openTab(event, 'accounts')">Люди</button>
            <button class="tablinks" onclick="openTab(event, 'groups')">Группы</button>
        </div>
        <%-- Accounts tab --%>
        <div id="accounts" class="tabcontent">
        </div>
        <%-- Groups tab --%>
        <div id="groups" class="tabcontent">
        </div>

        <script>
            window.onload = openTab(event, 'accounts');
            let numPageAccount = 0;
            let maxNumPageAccount = ${maxNumPageAccount};
            let numPageGroup = 0;
            let maxNumPageGroup = ${maxNumPageGroup};

            function printAccounts(data) {
                if (data.length === 0) {
                    $("#accounts").append(
                        '<div class="form-group">' +
                        '<div class="alert alert-danger">' +
                        '<p>Резальтатов не найдено</p>' +
                        '</div>' +
                        '</div>'
                    )
                } else {
                    for (let i = 0; i < data.length; i++) {
                        $("#accounts").append(
                            '<div class="row containerAccount">' +
                            '<div class="col-md-2">' +
                            '<img alt="Аватар" style="width:70px" src="<c:url value="/images/avatar/' + data[i].id + '"/>">' +
                            '</div>' +
                            '<div class="col-md-7 friendName">' +
                            '<a href="<c:url value="/accounts/' + data[i].id + '"/>">' + data[i].name + ' ' +
                            (data[i].surname != null ? data[i].surname : '') + '</a>' +
                            '</div>' +
                            '</div>'
                        )
                    }
                }
                if (numPageAccount !== 0) {
                    $("#accounts").append(
                        '<button type="submit" class="btn previous" id="prevAccountsButton">Назад</button>'
                    )
                }
                if (numPageAccount !== maxNumPageAccount) {
                    $("#accounts").append(
                        '<button type="submit" class="btn next" id="nextAccountsButton">Вперед</button>'
                    )
                }
            }

            function printGroups(data) {
                if (data.length === 0) {
                    $("#groups").append(
                        '<div class="form-group">' +
                        '<div class="alert alert-danger">' +
                        '<p>Резальтатов не найдено</p>' +
                        '</div>' +
                        '</div>'
                    )
                } else {
                    for (let i = 0; i < data.length; i++) {
                        $("#groups").append(
                            '<div class="row containerGroup">' +
                            '<div class="col-md-2">' +
                            '<img alt="Изображение группы" style="width:80px" src="<c:url
                        value="/images/group-image/' + data[i].id + '"/>">' +
                            '</div>' +
                            '<div class="col-md-7 friendName">' +
                            '<a href="<c:url value="/groups/' + data[i].id + '"/>">' + data[i].name + '</a>' +
                            '</div>' +
                            '</div>'
                        )
                    }
                }
                if (numPageGroup !== 0) {
                    $("#groups").append(
                        '<button type="submit" class="btn previous" id="prevGroupsButton">Назад</button>'
                    )
                }
                if (numPageGroup !== maxNumPageGroup) {
                    $("#groups").append(
                        '<button type="submit" class="btn next" id="nextGroupsButton">Вперед</button>'
                    )
                }
            }

            function removeAccountsAndButtons() {
                let groups = document.querySelectorAll("div.containerAccount");
                for (let i = 0; i < groups.length; i++) {
                    groups[i].remove();
                }
                if (document.getElementById('nextAccountsButton') != null) {
                    document.getElementById('nextAccountsButton').remove();
                }
                if (document.getElementById('prevAccountsButton') != null) {
                    document.getElementById('prevAccountsButton').remove();
                }
            }

            function removeGroupsAndButtons() {
                let accounts = document.querySelectorAll("div.containerGroup");
                for (let i = 0; i < accounts.length; i++) {
                    accounts[i].remove();
                }
                if (document.getElementById('nextGroupsButton') != null) {
                    document.getElementById('nextGroupsButton').remove();
                }
                if (document.getElementById('prevGroupsButton') != null) {
                    document.getElementById('prevGroupsButton').remove();
                }
            }

            function ajaxSearchForAccounts(search, numPage) {
                $.ajax({
                    url: "<c:url value='api/search/accounts'/>",     // URL - сервлета
                    data: {                                         // передаваемые сервлету данные
                        filter: search,
                        numPage: numPage
                    },
                    success: function (data) {
                        // обработка ответа от сервера
                        printAccounts(data);
                    }
                });
            }

            function ajaxSearchForGroups(search, numPage) {
                $.ajax({
                    url: "<c:url value='/api/search/groups'/>",     // URL - сервлета
                    data: {                                         // передаваемые сервлету данные
                        filter: search,
                        numPage: numPage
                    },
                    success: function (data) {
                        // обработка ответа от сервера
                        printGroups(data);
                    }
                });
            }
        </script>

        <script>
            // вызов функции по завершению загрузки страницы
            $(document).ready(function () {
                ajaxSearchForAccounts("${search}", numPageAccount);
                ajaxSearchForGroups("${search}", numPageGroup);
            });
        </script>

        <script>
            $(document).on('click', "#nextAccountsButton", function () {
                removeAccountsAndButtons();
                ajaxSearchForAccounts("${search}", ++numPageAccount);
            })
        </script>

        <script>
            $(document).on('click', "#prevAccountsButton", function () {
                removeAccountsAndButtons();
                ajaxSearchForAccounts("${search}", --numPageAccount);
            })
        </script>

        <script>
            $(document).on('click', "#nextGroupsButton", function () {
                removeGroupsAndButtons();
                ajaxSearchForGroups("${search}", ++numPageGroup);
            })
        </script>

        <script>
            $(document).on('click', "#prevGroupsButton", function () {
                removeGroupsAndButtons();
                ajaxSearchForGroups("${search}", --numPageGroup);
            })
        </script>

    </body>
</html>