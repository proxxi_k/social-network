<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <title>Update Authentication Information</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">

        <script src="http://code.jquery.com/jquery-1.11.0.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/script.js"></script>

        <style>

            input:-webkit-autofill,
            input:-webkit-autofill:hover,
            input:-webkit-autofill:focus,
            input:-webkit-autofill:active {
                box-shadow: 0 0 0 30px #ffffff inset !important;
            }

            .form-control, .btn-save, .btn-cancel {
                box-shadow: none !important;
                outline: none !important;
            }

            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            * {
                box-sizing: border-box;
            }

            .border-container {
                border: 3px solid #f1f1f1;
                width: 34%;
                margin: 60px 33% 0 33%;
                padding-top: 10px;
            }

            h4 {
                text-align: center;
                margin: 10px 0 15px 0;
            }

            .input-group {
                display: flex;
                width: 90%;
                margin: 0 5% 15px 5%;
            }

            .form-control:focus {
                border: 2px solid dodgerblue;
            }

            #showPassword {
                color: blue;
                cursor: pointer;
            }

            .btn-save, .btn-cancel {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                border: none;
                cursor: pointer;
                width: 90%;
                margin: 0 5% 0 5%;
                border-radius: 5px;
            }

            .btn-cancel {
                background-color: #ea8080;
            }

            .btn-save:hover, .btn-cancel:hover {
                opacity: 0.8;
                color: white;
            }

            .incorrectNum p {
                width: 100%;
            }

            .alert-danger {
                width: 100%;
                height: 50px;
            }

            .alert {
                margin-bottom: 0;
            }

        </style>

    </head>
    <body>
        <%-- Form updating authentification info --%>
        <div class="border-container">
            <form action="/accounts/${accountId}/auth/update" method="post"
                  onsubmit="return confirm('Сохранить изменения?')">
                <h4>Изменение логина и пароля</h4>
                <input type="hidden" name="currentEmail" value="${currentEmail}">
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                    </div>
                    <input name="newEmail" class="form-control" placeholder="Почта" type="email"
                           value="${newEmail == null ? currentEmail : newEmail}" required/>
                </div>
                <c:if test="${errorEmail!=null}">
                    <div class="form-group input-group">
                        <div class="alert alert-danger">
                            <p>${errorEmail}</p>
                        </div>
                    </div>
                </c:if>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <c:choose>
                        <c:when test="${loginAccount.id == accountId}">
                            <input name="currentPassword" class="form-control" id="currentPassword" placeholder="Текущий пароль"
                                   type="password" required/>
                        </c:when>
                        <c:otherwise>
                            <input name="currentPassword" class="form-control" id="currentPassword"
                                   placeholder="Пароль администратора"
                                   type="password" required/>
                        </c:otherwise>
                    </c:choose>
                </div>
                <c:if test="${errorPassword!=null}">
                    <div class="form-group input-group">
                        <div class="alert alert-danger">
                            <p>${errorPassword}</p>
                        </div>
                    </div>
                </c:if>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input name="newPassword" class="form-control" id="newPassword" placeholder="Новый пароль"
                           type="password" required/>
                </div>
                <div class="form-group input-group">
                    <a id="showPassword" onclick="show_hide_passwords(this)">Показать пароли</a>
                </div>
                <button type="submit" class="btn-save">Сохранить</button>
            </form>

            <%-- Cancel button --%>
            <form action="/accounts/${accountId}" method="get">
                <button type="submit" class="btn-cancel">Отмена</button>
            </form>
        </div>
    </body>
</html>
