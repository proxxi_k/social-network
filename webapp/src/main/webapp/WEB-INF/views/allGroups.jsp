<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">

        <title>All Groups</title>

        <style>
            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .containerGroup {
                border: 1px solid #ccc;
                background-color: #eee;
                border-radius: 5px;
                margin: 10px 10px 10px 10px;
            }

            .containerGroup::after {
                content: "";
                clear: both;
                display: table;
            }

            .friendName {
                display: flex;
                align-self: center;
            }

            .containerGroup img {
                float: left;
                margin-left: 20px;
                border-radius: 50%;
                padding: 10px 0 10px 0;
            }

            .containerGroup span {
                font-size: 20px;
                margin-right: 15px;
            }

            .next, .previous {
                position: absolute;
                bottom: 0;
            }

            .previous{
                left: 35%;
            }

            .next{
                right: 35%;
            }

            /* Кнопки стиля */
            .btn {
                background-color: DodgerBlue; /* Синий фон */
                border: none; /* Удалить границы */
                color: white; /* Белый текст */
                padding: 12px 16px; /* Немного отступов */
                font-size: 16px; /* Установите размер шрифта */
                cursor: pointer; /* Указатель мыши при наведении */
            }

            /* Более темный фон при наведении курсора мыши */
            .btn:hover {
                background-color: #56abfd;
                color: white;
            }

            .border-container {
                width: 50%;
                margin: 0 25% 0 25%;
                height: 680px;
                position:relative;
                padding-bottom:50px;
            }

            .info {
                display: table;
                margin: 0 auto;
                font-size: 20px;
                padding: 10px 0 10px 0;
            }

        </style>

    </head>
    <body>
        <div class="info">
            <b>Список всех групп:</b>
        </div>
        <div class="border-container">
            <div id="groups">
            </div>
        </div>

        <script>
            let numPage = 0;
            let maxNumPage = ${maxNumPage};

            function printGroups(data) {
                for (let i = 0; i < data.length; i++) {
                    $("#groups").append(
                        '<div class="row containerGroup">' +
                        '<div class="col-md-2">' +
                        '<img alt="Изображение группы" style="width:80px" src="<c:url
                        value="/images/group-image/' + data[i].id + '"/>">' +
                        '</div>' +
                        '<div class="col-md-7 friendName">' +
                        '<a href="<c:url value="/groups/' + data[i].id + '"/>">' + data[i].name + '</a>' +
                        '</div>' +
                        '</div>'
                    )
                }
                if (numPage !== 0) {
                    $("#groups").append(
                        '<button type="submit" class="btn previous" id="prevGroupsButton">Назад</button>'
                    )
                }
                if (numPage !== maxNumPage) {
                    $("#groups").append(
                        '<button type="submit" class="btn next" id="nextGroupsButton">Вперед</button>'
                    )
                }
            }

            function removeGroupsAndButtons() {
                let accounts = document.querySelectorAll("div.containerGroup");
                for (let i = 0; i < accounts.length; i++) {
                    accounts[i].remove();
                }
                if (document.getElementById('nextGroupsButton') != null) {
                    document.getElementById('nextGroupsButton').remove();
                }
                if (document.getElementById('prevGroupsButton') != null) {
                    document.getElementById('prevGroupsButton').remove();
                }
            }

            function ajaxGetAllGroups(numPage) {
                $.ajax({
                    url: "<c:url value='groups/api'/>",     // URL - сервлета
                    data: {                                         // передаваемые сервлету данные
                        numPage: numPage
                    },
                    success: function (data) {
                        // обработка ответа от сервера
                        printGroups(data);
                    }
                });
            }
        </script>

        <script>
            // вызов функции по завершению загрузки страницы
            $(document).ready(function () {
                ajaxGetAllGroups(numPage);
            });
        </script>

        <script>
            $(document).on('click', "#nextGroupsButton", function () {
                removeGroupsAndButtons();
                ajaxGetAllGroups(++numPage);
            })
        </script>

        <script>
            $(document).on('click', "#prevGroupsButton", function () {
                removeGroupsAndButtons();
                ajaxGetAllGroups(--numPage);
            })
        </script>

    </body>
</html>