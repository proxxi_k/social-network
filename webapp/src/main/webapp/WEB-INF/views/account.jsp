<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus" %>
<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.InternetPage" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <title>Account</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossorigin="anonymous"></script>

        <style>

            .form-control, .btn-save {
                box-shadow: none !important;
                outline: none !important;
            }

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .form-control {
                margin-top: 10px;
            }

            .form-control:focus {
                border: 2px solid dodgerblue;
            }

            .side {
                margin-left: 2%;
            }

            .btn-secondary, .btn-secondary:focus, .btn-secondary:hover, .btn-secondary:active {
                color: blue;
                background-color: white;
                border: none;
                background: none !important;
                padding: 0 !important;
                cursor: pointer;
                font-size: 16px;
            }

            .dropdown {
                margin-top: 1em;
                position: relative;
                display: inline-block;
                margin-bottom: 1em;
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown a:hover {
                background-color: #ddd;
            }

            .delButton, .delMessage {
                background: none !important;
                border: none;
                padding: 0 !important;
                color: blue;
                cursor: pointer;
                font-size: 1rem;
            }

            .delMessage {
                font-size: 12px;
            }


            /* Создайте три неравных столбца, которые плавают рядом друг с другом */
            .column {
                float: left;
                padding: 10px;
            }

            /* Левый и правый столбец */
            .column.side {
                width: 20%;
            }

            /* Средний столбец */
            .column.middle {
                width: 40%;
            }

            .btn-save {
                background-color: #4CAF50;
                color: white;
                padding: 8px;
                border: none;
                cursor: pointer;
                width: 30%;
                margin-top: 10px;
                border-radius: 5px;
            }

            /* Очистить float после столбцов */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            .login-container img {
                margin-right: 20px;
                margin-bottom: 20px;
            }

            .delButton {
                text-align: left;
            }

            body {
                font-family: Arial, Helvetica, sans-serif;
                line-height: 1.5rem;
            }

            .row.about {
                margin-top: 0;
                padding-top: 10px;
            }

            .aboutInfo {
                min-height: 371px;
            }

            b {
                font-size: 25px;
                color: rgb(66, 66, 66);
            }

            b.description {
                font-size: small;
            }

            .imgMessage {
                border-radius: 50px;
                display: table;
                margin: 0 auto;
            }

            .col-md-2 {
                font-size: 12px;
            }

            hr {
                margin: 0 0 5px 0;
            }

            .sender {
                line-height: 1em;
                display: table;
                margin: 0.5em auto;
                text-align: center;
            }

            .textMessage {
                display: flex;
                align-self: center;
            }

            .controlMessage {
                align-self: center;
            }

        </style>

    </head>
    <body>
        <%-- Page scope --%>
        <c:set var="today" value="<%=new java.util.Date()%>"/>

        <%-- Page scope --%>
        <c:set var="internetPage" value="<%=InternetPage.ACCOUNT_PAGE%>" scope="page"/>
        <c:set var="requestTo" value="<%=FriendStatus.REQUEST_TO%>" scope="page"/>
        <c:set var="requestFrom" value="<%=FriendStatus.REQUEST_FROM%>" scope="page"/>
        <c:set var="friend" value="<%=FriendStatus.FRIEND%>" scope="page"/>
        <c:set var="blacklist" value="<%=FriendStatus.BLACKLIST%>" scope="page"/>
        <c:set var="someoneBlacklist" value="<%=FriendStatus.SOMEONE_BLACKLIST%>" scope="page"/>

        <div class="row justify-content-center">
            <div class="column side">
                <div class="login-container">
                    <img src="<c:url value="/images/avatar/${account.id}"/>" alt="Аватар"
                         style="width:150px">
                </div>
                <%-- Friendships menu --%>
                <c:if test="${account.id != loginAccount.id}">
                    <div class="dropdown">
                        <c:choose>
                            <%-- Friends --%>
                            <c:when test="${friendStatus == friend}">
                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">У вас в друзьях
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/delete"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Удалить из друзей</button>
                                        </form>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/block"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Добавить в черный список</button>
                                        </form>
                                    </li>
                                </ul>
                            </c:when>
                            <%-- Required from --%>
                            <c:when test="${friendStatus == requestFrom}">
                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">Заявка отправлена
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/delete"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Отозвать заявку</button>
                                        </form>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/block"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Добавить в черный список</button>
                                        </form>
                                    </li>
                                </ul>
                            </c:when>
                            <%-- Required to --%>
                            <c:when test="${friendStatus == requestTo}">
                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">Заявка в друзья
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/add"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Добавить в друзья</button>
                                        </form>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/delete"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Отклонить заявку</button>
                                        </form>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/block"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Добавить в черный список</button>
                                        </form>
                                    </li>
                                </ul>
                            </c:when>
                            <%-- My blaclist --%>
                            <c:when test="${friendStatus == blacklist}">
                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">В черном списке
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/delete"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Удалить из черного списка</button>
                                        </form>
                                    </li>
                                </ul>
                            </c:when>
                            <%-- Someone blacklist --%>
                            <c:when test="${friendStatus == someoneBlacklist}">
                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">Вы в черном списке
                                </button>
                            </c:when>
                            <%-- No friendships --%>
                            <c:otherwise>
                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">Добавить в друзья
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/request"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Отправить заявку</button>
                                        </form>
                                        <form action="/accounts/${loginAccount.id}/friendships/${account.id}/block"
                                              method="post">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delButton">Добавить в черный список</button>
                                        </form>
                                    </li>
                                </ul>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </c:if>

                <%--Common panel--%>
                <p><a href="<c:url value="/accounts/${account.id}/friendships"/>">Друзья</a></p>
                <p><a href="<c:url value="/accounts/${account.id}/memberships"/>">Группы</a></p>

                <%--Account panel--%>
                <c:choose>
                    <c:when test="${account.id == loginAccount.id}">
                        <p><a href="<c:url value="/accounts/${account.id}/interlocutors"/>">Сообщения</a></p>
                        <p><a href="<c:url value="/accounts/${account.id}/news"/>">Новости</a></p>
                    </c:when>
                    <c:otherwise>
                        <p><a href="<c:url value="/accounts/${loginAccount.id}/interlocutors/${account.id}"/>">Написать
                            сообщение</a></p>
                    </c:otherwise>
                </c:choose>
                <c:if test="${account.id == loginAccount.id || loginAccount.auth.admin}">
                    <hr>
                </c:if>
                <c:if test="${account.id == loginAccount.id}">
                    <p><a href="<c:url value="/accounts/${account.id}/save"/>" download>Сохранить профиль</a></p>
                </c:if>

                <%--Admin panel--%>
                <c:if test="${account.id != loginAccount.id && loginAccount.auth.admin && !account.auth.admin}">
                    <form action="/accounts/${account.id}/admin/assign" method="post">
                        <button type="submit" class="delButton">Назначить администратором</button>
                    </form>
                </c:if>

                <%--Admin/account panel--%>
                <c:if test="${account.id == loginAccount.id || loginAccount.auth.admin}">
                    <p><a href="<c:url value="/accounts/${account.id}/update"/>">Редактировать профиль</a></p>
                    <p><a href="<c:url value="/accounts/${account.id}/auth/update"/>">Изменить логин/пароль</a></p>
                    <form action="/accounts/${account.id}/delete" method="post"
                          onsubmit="checkDeleteAccount(this);return false;">
                        <button type="submit" class="delButton">Удалить аккаунт</button>
                    </form>
                </c:if>
            </div>

            <div class="column middle">
                <%-- Account Info --%>
                <div class="aboutInfo">
                    <c:choose>
                        <c:when test="${account.auth.admin}">
                            <b>${account.surname} ${account.name} ${account.patronymic} (Администратор)</b>
                        </c:when>
                        <c:otherwise>
                            <b><p>${account.surname} ${account.name} ${account.patronymic}</p></b>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${account.birthday!=null}">
                        <div class="row about">
                            <div class="col-md-3 about">
                                <b class="description"><i><label>День рождения:</label></i></b>
                            </div>
                            <div class="col-md-9">
                                    ${account.birthday}
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${account.homeAddress!=null}">
                        <div class="row about">
                            <div class="col-md-3 about">
                                <b class="description"><i><label>Домашний адрес:</label></i></b>
                            </div>
                            <div class="col-md-9">
                                    ${account.homeAddress}
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${account.workAddress!=null}">
                        <div class="row about">
                            <div class="col-md-3 about">
                                <b class="description"><i><label>Рабочий адрес:</label></i></b>
                            </div>
                            <div class="col-md-9">
                                    ${account.workAddress}
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${account.skype!=null}">
                        <div class="row about">
                            <div class="col-md-3 about">
                                <b class="description"><i><label>Скайп:</label></i></b>
                            </div>
                            <div class="col-md-9">
                                    ${account.skype}
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${account.additionalInfo!=null}">
                        <div class="row about">
                            <div class="col-md-3 about">
                                <b class="description"><i><label>Дополнительная информация:</label></i></b>
                            </div>
                            <div class="col-md-9">
                                    ${account.additionalInfo}
                            </div>
                        </div>
                    </c:if>
                    <div class="row about">
                        <div class="col-md-3 about">
                            <b class="description"><i><label>Дата регистрации:</label></i></b>
                        </div>
                        <div class="col-md-9">
                            ${account.registrationDate}
                        </div>
                    </div>
                    <c:if test="${not empty account.phones}">
                        <c:forEach var="phones" items="${account.phones}">
                            <div class="row about">
                                <div class="col-md-3 about">
                                    <c:if test="${phones.equals(account.phones.get(0))}">
                                        <b class="description"><i><label>Телефоны:</label></i></b>
                                    </c:if>
                                </div>
                                <div class="col-md-9">
                                        ${phones.number} (${phones.type})
                                </div>

                            </div>
                        </c:forEach>
                    </c:if>
                </div>
                <hr>
                <%-- Send Post --%>
                <%-- Not on the blacklist --%>
                <div>
                    <c:if test="${friendStatus != blacklist}">
                        <form action="/messages/${account.id}/send" method="post">
                            <input type="hidden" name="internetPage" value="${internetPage}">
                            <div class="input-container">
                                <textarea class="form-control" rows="3" placeholder="Что у вас нового?"
                                          name="messageBody"></textarea>
                                <button type="submit" class="btn-save">Опубликовать</button>
                            </div>
                        </form>
                        <c:if test="${not empty wallMessages}">
                            <hr>
                        </c:if>
                        <%-- Account Wall --%>
                        <c:forEach var="wallMessage" items="${wallMessages}">
                            <div class="row sendPost">
                                <div class="col-md-2">
                                    <img class="imgMessage"
                                         src="<c:url value="/images/avatar/${wallMessage.sender.id}"/>"
                                         alt="Аватар" style="width:50px">
                                    <a class="sender" href="<c:url value="/accounts/${wallMessage.sender.id}"/>">
                                            ${wallMessage.sender.surname} ${wallMessage.sender.name}
                                    </a>
                                </div>
                                <div class="col-md-8 textMessage">
                                        ${wallMessage.message}
                                </div>
                                <div class="col-md-2 controlMessage">
                                    <c:choose>
                                        <c:when test="${wallMessage.date.date  == today.date}">
                                            <fmt:formatDate type="time" value="${wallMessage.date}"
                                                            pattern="HH:mm:ss"/>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:formatDate type="time" value="${wallMessage.date}"
                                                            pattern="dd.MM.yyyy"/>
                                        </c:otherwise>
                                    </c:choose>
                                        <%-- Account Control--%>
                                    <c:if test="${account.id==loginAccount.id}">
                                        <form action="/messages/${wallMessage.id}/delete" method="post">
                                            <input type="hidden" name="accountId" value="${account.id}">
                                            <input type="hidden" name="internetPage" value="${internetPage}">
                                            <button type="submit" class="delMessage">Удалить
                                            </button>
                                        </form>
                                    </c:if>
                                </div>
                            </div>
                            <hr>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
        </div>

        <script>
            function checkDeleteAccount(f) {
                if (${isCheckCreatedGroups}) {
                    alert("Невозможно удалить аккаунт. Перед удалением аккаунта необходимо удалить созданные группы или назначить в них новых владельцев.");
                } else {
                    if (confirm("Вы действительно хотитие удалить аккаунт?")) {
                        f.submit();
                    }
                }
            }
        </script>

    </body>
</html>