<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <style>

        body {
            line-height: 1.5rem;
        }

        .d1 input {
            margin-top: 8px;
            padding-right: 20px;
            width: 90%;
            height: 30px;
            border: 2px solid #ccc;
            border-radius: 5px;
            outline: none;
            font-size: 14px;
        }

        .d1 button {
            margin-top: 8px;
            margin-right: 20px;
            position: absolute;
            top: 0;
            right: 0;
            width: 50px;
            height: 30px;
            border: none;
            background: #9c9c9c;
            border-radius: 0 5px 5px 0;
            cursor: pointer;
        }

        .d1 button:before {
            content: "\f002";
            font-family: FontAwesome;
            font-size: 16px;
            color: #f9f0da;
        }

        * a:active, * a:hover, * a {
            text-decoration: none;
            color: blue;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        header {
            margin-bottom: 50px;
            position: relative;
            z-index: 2;
        }

        .my-navbar {
            margin-bottom: 20px;
            position: fixed;
            left: 0;
            top: 0;;
            width: 100%;
            background-color: #555555;
            overflow: auto;
        }

        .my-navbar a {
            float: left;
            padding: 12px;
            color: white;
            text-decoration: none;
            font-size: 17px;
        }

        .my-navbar a:hover {
            background-color: #363535;
            color: white;
            text-decoration: none;
        }

        .home {
            background-color: #4CAF50;
        }

        a.home:hover {
            background-color: #5dbd62;

        }

        .search-container {
            float: right;
            width: 20%;
        }

    </style>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    </head>
    <body>
        <header>
            <%-- Session scope --%>
            <c:set var="loginAccount" value="${sessionScope.loginAccount}"/>

            <%-- Navigation bar --%>
            <div class="my-navbar">
                <a class="home" href="<c:url value="/accounts/${loginAccount.id}"/>"><i
                        class="fa fa-home"></i>
                    ${loginAccount.surname} ${loginAccount.name}</a>
                <a href="<c:url value="/accounts"/>"> Люди </a>
                <a href="<c:url value="/groups"/>"> Группы </a>
                <c:choose>
                    <c:when test="${loginAccount==null}">
                        <a href="${pageContext.request.contextPath}/login"> Войти </a>
                    </c:when>
                    <c:otherwise>
                        <a href="${pageContext.request.contextPath}/logout"> Выйти </a>
                    </c:otherwise>
                </c:choose>
                <form action="${pageContext.request.contextPath}/search" method="get">
                    <div class="search-container">
                        <div class="d1">
                            <form>
                                <input type="text" placeholder="Поиск..." name="search" id="search">
                                <button type="submit"></button>
                            </form>
                        </div>
                    </div>
                </form>
            </div>
        </header>

        <script>
            $("#search").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "<c:url value='/api/search/autocomplete'/>",
                        data: {
                            filter: request.term
                        },
                        success: function (data) {
                            response($.map(data, function (object) {
                                var link;
                                object.hasOwnProperty("surname") ? link = '/accounts/' + object.id : link =
                                    '/groups/' + object.id;
                                return object.hasOwnProperty("surname") ?
                                    (object.surname === null) ?
                                        {
                                            plink: link,
                                            value: object.name,
                                            label: 'Пользователь: ' + object.name
                                        } :
                                        {
                                            plink: link,
                                            value: object.surname + ' ' + object.name,
                                            label: 'Пользователь: ' + object.surname + ' ' + object.name
                                        } : {
                                        plink: link,
                                        value: object.name,
                                        label: 'Группа: ' + object.name
                                    }
                            }))
                        }
                    });
                },
                minLength: 8,
                select: function (event, ui) {
                    location.href = ui.item.plink;
                    return false;
                }
            });
        </script>

    </body>
</html>