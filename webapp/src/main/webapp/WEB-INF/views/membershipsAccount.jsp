<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.InternetPage" %>
<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipAccountTab" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <title>Memberships Account</title>
        <script src="${pageContext.request.contextPath}/resources/js/script.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">
        <style>

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .delButton {
                background: none !important;
                border: none;
                padding: 0 !important;
                color: blue;
                cursor: pointer;
                font-size: 14px;
            }

            * {
                text-decoration: none;
            }

            /* Стиль вкладок */
            .tab {
                overflow: hidden;
                border: 1px solid #ccc;
                background-color: #f1f1f1;
                width: 60%;
                margin: 0 auto;
            }

            /* Стиль кнопок внутри вкладок */
            .tab button {
                background-color: inherit;
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 10px 10px;
                transition: 0.3s;
            }

            /* Изменение цвета фона кнопок при наведении курсора мыши */
            .tab button:hover {
                background-color: #ddd;
            }


            /* Стиль содержимого вкладки */
            .tabcontent {
                display: none;
                border: 1px solid #ccc;
                width: 60%;
                margin: 0 auto;
            }

            .controls {
                float: right;
                align-self: center;

            }

            .login-container {
                border: 1px solid #ccc;
                background-color: #eee;
                border-radius: 5px;
                margin: 10px 10px 10px 10px;
            }

            .login-container::after {
                content: "";
                clear: both;
                display: table;
            }

            .login-container img {
                float: left;
                margin-left: 20px;
                border-radius: 50%;
                padding: 10px 0 10px 0;
            }

            .login-container span {
                font-size: 20px;
                margin-right: 15px;
            }

            .tabcontent a:active, /* активная/посещенная ссылка */
            .tabcontent a:hover, /* при наведении */
            .tabcontent a {
                text-decoration: none;
                color: #0000FF;
            }

            .friendName {
                display: flex;
                align-self: center;
            }

            .form-group {
                margin: 10px 10px 10px 10px;
            }

            .info {
                display: table;
                margin: 0 auto;
                font-size: 20px;
                padding: 10px 0 10px 0;
            }

            .del {
                font-size: 14px;
                float: right;
                padding-right: 25px;
            }

        </style>
    </head>
    <body>
        <%-- Page scope --%>
        <c:set var="internetPage" value="<%=InternetPage.MEMBERSHIP_ACCOUNT_PAGE%>" scope="page"/>
        <c:set var="all_tab" value="<%=MembershipAccountTab.ALL_GROUPS%>" scope="page"/>
        <c:set var="order_tab" value="<%=MembershipAccountTab.ORDER_GROUPS%>" scope="page"/>
        <c:set var="user_tab" value="<%=MembershipAccountTab.USER_GROUPS%>" scope="page"/>
        <c:set var="moderator_tab" value="<%=MembershipAccountTab.MODERATOR_GROUPS%>" scope="page"/>
        <c:set var="creator_tab" value="<%=MembershipAccountTab.CREATED_GROUPS%>" scope="page"/>

        <%-- Heading --%>
        <div class="info">
            <b>Список групп пользователя:
                <a href=" <c:url value="/accounts/${account.id}"/>">${account.surname} ${account.name}</a></b>
        </div>

        <%-- Memberships tab bar --%>
        <c:choose>
            <c:when test="${account.id == loginAccount.id}">
                <div class="tab">
                    <button class="tablinks" onclick="openTab(event, '${all_tab}')">Все группы</button>
                    <button class="tablinks" onclick="openTab(event, '${user_tab}')">Участник</button>
                    <button class="tablinks" onclick="openTab(event, '${moderator_tab}')">Модератор</button>
                    <button class="tablinks" onclick="openTab(event, '${creator_tab}')">Созданные группы</button>
                    <button class="tablinks" onclick="openTab(event, '${order_tab}')">Заявки</button>
                    <div class="del">
                        <a href="<c:url value="/groups/create"/> ">Создать группу</a>
                        <form action="/groups/created/delete" method="post"
                              onsubmit="return confirm('Вы действительно хотите удалить группу?')">
                            <input type="hidden" name="accountId" value="${account.id}">
                            <button type="submit" class="delButton">Удалить созданные группы</button>
                        </form>
                    </div>
                </div>
            </c:when>
            <c:when test="${loginAccount.auth.admin}">
                <div class="tab">
                    <button class="tablinks" onclick="openTab(event, '${all_tab}')">Мои группы</button>
                    <div class="del">
                        <form action="/groups/created/delete" method="post"
                              onsubmit="return confirm('Вы действительно хотите удалить все созданные группы?')">
                            <input type="hidden" name="accountId" value="${account.id}">
                            <button type="submit" class="delButton">Удалить созданные группы</button>
                        </form>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="tab">
                    <button class="tablinks" onclick="openTab(event, '${all_tab}')">Мои группы</button>
                </div>
            </c:otherwise>
        </c:choose>

        <%-- All my groups tab --%>
        <div id="${all_tab}" class="tabcontent">
            <c:if test="${empty userGroups && empty moderatorGroups && empty createdGroups}">
                <div class="form-group">
                    <div class="alert alert-danger">
                        <p>Группы отсутствуют</p>
                    </div>
                </div>
            </c:if>
            <%-- User --%>
            <c:forEach var="userGroup" items="${userGroups}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/group-image/${userGroup.id}"/>" alt="Изображение группы"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/groups/${userGroup.id}"/>">${userGroup.name} (участник)</a>
                    </div>
                    <div class="col-md-3 controls">
                        <c:if test="${account.id == loginAccount.id}">
                            <form action="/groups/${userGroup.id}/members/${loginAccount.id}/delete" method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="membershipAccountTab" value="${all_tab}">
                                <button type="submit" class="delButton">Выйти из группы</button>
                            </form>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
            <%-- Moderator --%>
            <c:forEach var="moderatorGroup" items="${moderatorGroups}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/group-image/${moderatorGroup.id}"/>" alt="Изображение группы"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/groups/${moderatorGroup.id}"/>">${moderatorGroup.name}
                            (модератор)</a>
                    </div>
                    <div class="col-md-3 controls">
                        <c:if test="${account.id == loginAccount.id}">
                            <form action="/groups/${moderatorGroup.id}/members/${loginAccount.id}/delete"
                                  method="post">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="membershipAccountTab" value="${all_tab}">
                                <button type="submit" class="delButton">Выйти из группы</button>
                            </form>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
            <%-- Creator --%>
            <c:forEach var="createdGroup" items="${createdGroups}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/group-image/${createdGroup.id}"/>" alt="Изображение группы"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/groups/${createdGroup.id}"/>">${createdGroup.name}
                            (владелец)</a>
                    </div>
                    <div class="col-md-3 controls">
                        <c:if test="${account.id == loginAccount.id || loginAccount.auth.admin}">
                            <form action="/groups/${createdGroup.id}/delete" method="post"
                                  onsubmit="return confirm('Вы действительно хотите удалить группу?')">
                                <input type="hidden" name="accountId" value="${account.id}">
                                <input type="hidden" name="groupId" value="${createdGroup.id}">
                                <input type="hidden" name="internetPage" value="${internetPage}">
                                <input type="hidden" name="membershipAccountTab" value="${all_tab}">
                                <button type="submit" class="delButton">Удалить группу</button>
                            </form>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
        </div>

        <%-- User groups tab --%>
        <div id="${user_tab}" class="tabcontent">
            <c:if test="${empty userGroups}">
                <div class="form-group">
                    <div class="alert alert-danger">
                        <p>Вы пока не состоите ни в одной группе</p>
                    </div>
                </div>
            </c:if>
            <c:forEach var="userGroup" items="${userGroups}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/group-image/${userGroup.id}"/>" alt="Изображение группы"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/groups/${userGroup.id}"/>">${userGroup.name}</a>
                    </div>
                    <div class="col-md-3 controls">
                        <form action="/groups/${userGroup.id}/members/${loginAccount.id}/delete" method="post">
                            <input type="hidden" name="internetPage" value="${internetPage}">
                            <input type="hidden" name="membershipAccountTab" value="${user_tab}">
                            <button type="submit" class="delButton">Выйти из группы</button>
                        </form>
                    </div>
                </div>
            </c:forEach>
        </div>
        <%-- Moderator groups tab --%>
        <div id="${moderator_tab}" class="tabcontent">
            <c:if test="${empty moderatorGroups}">
                <div class="form-group">
                    <div class="alert alert-danger">
                        <p>Вы пока не являетесь модератором ни в одной группе</p>
                    </div>
                </div>
            </c:if>
            <c:forEach var="moderatorGroup" items="${moderatorGroups}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/group-image/${moderatorGroup.id}"/>" alt="Изображение группы"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/groups/${moderatorGroup.id}"/>">${moderatorGroup.name}</a>
                    </div>
                    <div class="col-md-3 controls">
                        <form action="/groups/${moderatorGroup.id}/members/${loginAccount.id}/delete" method="post">
                            <input type="hidden" name="internetPage" value="${internetPage}">
                            <input type="hidden" name="membershipAccountTab" value="${moderator_tab}">
                            <button type="submit" class="delButton">Выйти из группы</button>
                        </form>
                    </div>
                </div>
            </c:forEach>
        </div>

        <%-- Created groups tab --%>
        <div id="${creator_tab}" class="tabcontent">
            <c:if test="${empty createdGroups}">
                <div class="form-group">
                    <div class="alert alert-danger">
                        <p>Вы пока не создали ни одну группу</p>
                    </div>
                </div>
            </c:if>
            <c:forEach var="createdGroup" items="${createdGroups}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/group-image/${createdGroup.id}"/>" alt="Изображение группы"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/groups/${createdGroup.id}"/>">${createdGroup.name}</a>
                    </div>
                    <div class="col-md-3 controls">
                        <form action="/groups/${createdGroup.id}/delete" method="post"
                              onsubmit="return confirm('Вы действительно хотите удалить группу?')">
                            <input type="hidden" name="accountId" value="${loginAccount.id}">
                            <input type="hidden" name="internetPage" value="${internetPage}">
                            <input type="hidden" name="membershipAccountTab" value="${creator_tab}">
                            <button type="submit" class="delButton">Удалить группу</button>
                        </form>
                    </div>
                </div>
            </c:forEach>
        </div>

        <%-- Order to groups tab --%>
        <div id="${order_tab}" class="tabcontent">
            <c:if test="${empty orderGroups}">
                <div class="form-group">
                    <div class="alert alert-danger">
                        <p>Вы пока не подали ни одну заявку на вступление в группу</p>
                    </div>
                </div>
            </c:if>
            <c:forEach var="orderGroup" items="${orderGroups}">
                <div class="row login-container">
                    <div class="col-md-2">
                        <img src="<c:url value="/images/group-image/${orderGroup.id}"/>" alt="Изображение группы"
                             style="width:90px">
                    </div>
                    <div class="col-md-7 friendName">
                        <a href="<c:url value="/groups/${orderGroup.id}"/>">${orderGroup.name}</a>
                    </div>
                    <div class="col-md-3 controls">
                        <form action="/groups/${orderGroup.id}/members/${loginAccount.id}/delete" method="post">
                            <input type="hidden" name="internetPage" value="${internetPage}">
                            <input type="hidden" name="membershipAccountTab" value="${order_tab}">
                            <button type="submit" class="delButton">Отозвать заявку</button>
                        </form>
                    </div>
                </div>
            </c:forEach>
        </div>

        <script>
            window.onload = openTab(event, '${membershipAccountTab}');
        </script>

    </body>
</html>
