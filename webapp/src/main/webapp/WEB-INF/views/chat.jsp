<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="common/header.jsp" %>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.2.0/sockjs.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.js"></script>
<html>
    <head>
        <title>Chat</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">

        <style>

            :root {
                --send-bg: #5ac13a;
                --send-color: white;
                --receive-bg: #E5E5EA;
                --receive-text: black;
                --page-background: white;
            }

            .form-control, .btn-save, .btn-cancel {
                box-shadow: none !important;
                outline: none !important;
            }

            .btn-save, .btn-cancel {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                border: none;
                cursor: pointer;
                width: 96%;
                margin: 2% 2% 0 2%;
                border-radius: 5px;
            }

            .btn-cancel {
                background-color: #ea8080;
            }

            .form-control:focus {
                border: 2px solid dodgerblue;
            }

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            body {
                font-family: "Helvetica Neue", Helvetica, sans-serif;
                font-size: 15px;
                font-weight: normal;
                margin: 50px auto;
                display: flex;
                flex-direction: column;
                background-color: var(--page-background);
            }

            .border-container {
                border: 3px solid #f1f1f1;
                width: 34%;
                margin: -30px 33% 0 33%;
            }

            .accountMessage {
                color: var(--send-color);
                background: var(--send-bg);
                align-self: flex-end;
                max-width: 255px;
                min-width: 100px;
                word-wrap: break-word;
                margin-bottom: 5px;
                margin-right: 20px;
                line-height: 15px;
                padding: 5px 15px;
                border-radius: 20px;
                display: inline-block;
                height: content-box;
            }

            .dateMessage {
                font-size: 10px;
                float: right;
            }

            .interlocutorMessage {
                background: var(--receive-bg);
                color: var(--receive-text);
                align-self: flex-start;
                max-width: 255px;
                min-width: 100px;
                word-wrap: break-word;
                margin-bottom: 5px;
                margin-left: 20px;
                line-height: 15px;
                padding: 5px 15px;
                border-radius: 25px;
                display: inline-block;
            }

            .message {
                display: flex;
                flex-direction: column;
            }

            .messages {
                overflow-x: scroll;
                height: 390px;
                margin-bottom: 10px;
            }

            .form-control {
                width: 96%;
                margin: 0 2% 0 2%;
            }

            hr {
                width: 96%;
                margin: 0 2% 0.5em 2%;
            }

            .interlocutorInfo {
                display: table;
                margin: 0 auto;
            }

            label {
                font-size: 19px;
                vertical-align: center;
            }

            img {
                border-radius: 50%;
                vertical-align: center;
            }

        </style>

    </head>
    <body>
        <%-- Page scope --%>
        <c:set var="internetPage" value="CHAT_PAGE" scope="page"/>
        <c:set var="today" value="<%=new java.util.Date()%>"/>

        <div class="border-container">
            <%-- Heading --%>
            <div class="interlocutorInfo">
                <label>Чат с
                    <img src="<c:url value="/images/avatar/${interlocutor.id}"/>" alt="Аватар"
                         style="width:40px">
                    <a href=" <c:url value="/accounts/${interlocutor.id}"/>">${interlocutor.name} ${interlocutor.surname}</a>
                </label>
            </div>
            <hr>

            <%-- Chat Window --%>
            <div class="messages" id="messages">
                <c:forEach var="personalMessage" items="${chat}">
                    <div class="message">
                        <c:choose>
                            <c:when test="${personalMessage.sender.id==loginAccount.id}">
                                <div class="accountMessage">
                                    <div class="textMessage">${personalMessage.message}</div>
                                    <div class="dateMessage">
                                        <c:choose>
                                            <c:when test="${personalMessage.date.date  == today.date}">
                                                <fmt:formatDate type="time" value="${personalMessage.date}"
                                                                pattern="HH:mm:ss"/>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:formatDate type="time" value="${personalMessage.date}"
                                                                pattern="dd.MM.yyyy"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="interlocutorMessage">
                                    <div class="textMessage">${personalMessage.message}</div>
                                    <div class="dateMessage">
                                        <c:choose>
                                            <c:when test="${personalMessage.date.date  == today.date}">
                                                <fmt:formatDate type="time" value="${personalMessage.date}"
                                                                pattern="HH:mm"/>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:formatDate type="time" value="${personalMessage.date}"
                                                                pattern="dd.MM.yyyy"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </c:forEach>
            </div>

            <%-- Send Message --%>
            <hr>
            <div class="input-container">
            <textarea class="form-control" id="textMessage" rows="3" placeholder="Текст сообщения"
                      name="messageBody"></textarea>
                <button type="submit" class="btn-save" id="btn">Отправить</button>
            </div>
            <%-- Cancel button --%>
            <form action="/accounts/${loginAccount.id}/interlocutors" method="get">
                <button type="submit" class="btn-cancel">Вернуться ко всем чатам</button>
            </form>
        </div>

        <script>
            $(function () {
                let client = null;
                const sender = ${loginAccount.id};
                const recipient = ${interlocutor.id};
                let login = {
                    id: ${loginAccount.id}
                };

                    function getChat() {
                        return sender < recipient ? '/' + sender + '/' + recipient : '/' + recipient + '/' + sender;
                    }

                function connect() {
                    if (client != null) {
                        client.disconnect();
                    }
                    let socket = new SockJS('/chat');
                    client = Stomp.over(socket);
                    client.connect({}, function () {
                        client.subscribe('/topic' + getChat(), function (message) {
                            showMessage(JSON.parse(message.body));
                        });
                    });
                }

                function showMessage(msg) {
                    let date = new Date(msg.date);
                    if (msg.sender.id === sender) {
                        $('#messages').append(
                            '<div class="message">' +
                            '<div class="accountMessage">' +
                            '<div class="textMessage">' + msg.message + '</div>' +
                            '<div class="dateMessage">' + date.toLocaleTimeString() + '</div>' +
                            '</div>');
                    } else {
                        $('#messages').append(
                            '<div class="message">' +
                            '<div class="interlocutorMessage">' +
                            '<div class="textMessage">' + msg.message + '</div>' +
                            '<div class="dateMessage">' + date.toLocaleTimeString() + '</div>' +
                            '</div>');
                    }
                    scrollMessageToBottom();
                }

                connect();

                $("#btn").click(function () {
                    let text = $("#textMessage");
                    let msg = {
                        accountId: recipient,
                        sender: login,
                        message: text.val()
                    };
                    text.val('');
                    client.send("/app/chat" + getChat(), {}, JSON.stringify(msg))
                });

            });
        </script>

        <script>
            function scrollMessageToBottom() {
                let blockMessages = document.getElementById("messages");
                blockMessages.scrollTop = blockMessages.scrollHeight;
            }
        </script>

        <script>
            window.onload = scrollMessageToBottom();

        </script>

    </body>
</html>
