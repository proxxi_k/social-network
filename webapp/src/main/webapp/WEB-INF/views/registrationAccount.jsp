<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <title>Registration</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">

        <script src="http://code.jquery.com/jquery-1.11.0.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/script.js"></script>

        <style>

            input:-webkit-autofill,
            input:-webkit-autofill:hover,
            input:-webkit-autofill:focus,
            input:-webkit-autofill:active {
                box-shadow: 0 0 0 30px #ffffff inset !important;
            }

            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            h4 {
                text-align: center;
                margin: 10px 0 15px 0;
            }

            .phone {
                width: 100%;
            }

            .border-container {
                border: 3px solid #f1f1f1;
                width: 40%;
                margin: 10px 30% 0 30%;
            }

            .input-group {
                display: flex;
                width: 90%;
                margin: 0 5% 15px 5%;
            }

            input[type=button] {
                width: 7%;
            }

            .form-control, .btn {
                box-shadow: none !important;
                outline: none !important;
            }

            .btn {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                border: none;
                cursor: pointer;
                width: 90%;
                margin: 0 5% 0 5%;
                border-radius: 5px;
            }

            .btn:hover {
                opacity: 0.8;
                color: white;
            }

            .form-control:focus {
                border: 2px solid dodgerblue;
            }

            .input-select:focus {
                border: 2px solid dodgerblue;
            }

            .input-select {
                border: 1px solid #c2c2c2;
            }

            .custom-file-label::after {
                content: "Обзор...";
            }

            #showPassword {
                color: blue;
                cursor: pointer;
            }

            #login {
                margin: 25px 0 0 57%;
            }

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .incorrectNum p {
                width: 100%;
            }

            .alert-danger {
                width: 100%;
                height: 50px;
            }

            .alert {
                margin-bottom: 0;
            }

        </style>

    </head>
    <body>
        <form:form modelAttribute="account" enctype="multipart/form-data" action="registration" method="post"
                   onsubmit="return validateFormWhenRegistration()">
            <div class="border-container">
                <h4>Регистрация</h4>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="name" type="text" class="form-control" placeholder="Имя"
                           value="${account.name == null ? '': account.name}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="surname" class="form-control" placeholder="Фамилия" type="text"
                           value="${account.surname == null ? '': account.surname}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="patronymic" class="form-control" placeholder="Отчество" type="text"
                           value="${account.patronymic == null ? '': account.patronymic}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-birthday-cake"></i> </span>
                    </div>
                    <input name="birthday" id="birthday" class="form-control" placeholder="birthday" type="date"
                           value="${account.birthday == null ? '' : account.birthday}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-home"></i> </span>
                    </div>
                    <input name="homeAddress" class="form-control" placeholder="Домашний адрес" type="text"
                           value="${account.homeAddress == null ? '' : account.homeAddress}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-briefcase"></i> </span>
                    </div>
                    <input name="workAddress" class="form-control" placeholder="Рабочий адрес" type="text"
                           value="${account.workAddress == null ? '' : account.workAddress}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-skype"></i> </span>
                    </div>
                    <input name="skype" class="form-control" placeholder="Skype" type="text"
                           value="${account.skype == null ? '' : account.skype}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-info"></i> </span>
                    </div>
                    <textarea name="additionalInfo" class="form-control" placeholder="Дополнительная информация о себе"
                              rows="3">${account.additionalInfo == null ? '' : account.additionalInfo}</textarea>
                </div>
                <div class="phones" id="phones">
                    <c:choose>
                        <%-- without phones --%>
                        <c:when test="${account.phones == null}">
                            <div class="phone">
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                    </div>
                                    <select class="input-select">
                                        <option value="личный">личный</option>
                                        <option value="домашний">домашний</option>
                                        <option value="рабочий">рабочий</option>
                                    </select>
                                    <input class="form-control" type="text" placeholder="Телефон">
                                    <input type="button" value="+" onclick="addPhoneWhenRegistering(this)"/>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="phone" items="${account.phones}">
                                <div class="phone">
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                        </div>
                                        <select class="input-select">
                                            <option value="личный" ${phone.type.equals("личный")?'selected':''}>
                                                личный
                                            </option>
                                            <option value="домашний"
                                                ${phone.type.equals("домашний")?'selected':''}>
                                                домашний
                                            </option>
                                            <option value="рабочий" ${phone.type.equals("рабочий")?'selected':''}>
                                                рабочий
                                            </option>
                                        </select>
                                        <input class="form-control" type="text" placeholder="Телефон"
                                               value="${phone.number}">
                                        <c:choose>
                                            <%-- not the last phone --%>
                                            <c:when test="${account.phones.indexOf(phone)!=account.phones.size()-1}">
                                                <input type="button" value="-"
                                                       onclick="delPhoneWhenRegistering(this)"/>
                                            </c:when>
                                            <%-- last phone --%>
                                            <c:otherwise>
                                                <input type="button" value="+" onclick="addPhoneWhenRegistering(this)"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="form-group input-group">
                    <input type="file" id="myfile" class="custom-file-input" accept="image/*,image/jpeg,image/png"
                           name="avatar"
                           onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])">
                    <label class="custom-file-label">Выберите аватар (не более 65КБ)</label>
                </div>
                <c:if test="${errorSizeAvatar!=null}">
                    <div class="form-group input-group" id="errorSizeAvatar">
                        <div class="alert alert-danger">
                            <p>${errorSizeAvatar}</p>
                        </div>
                    </div>
                </c:if>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                    </div>
                    <input name="email" class="form-control" placeholder="Почта" type="email"
                           value="${email == null ? '' : email}" required/>
                </div>
                <c:if test="${errorEmail!=null}">
                    <div class="form-group input-group" id="errorEmail">
                        <div class="alert alert-danger">
                            <p>${errorEmail}</p>
                        </div>
                    </div>
                </c:if>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input name="password" id="password" class="form-control" placeholder="Пароль" type="password"
                           required/>
                </div>
                <div class="form-group input-group">
                    <a id="showPassword" onclick="show_hide_password(this)">Показать пароль</a>
                </div>
                <button type="submit" class="btn">Зарегистрироваться</button>
                <p id="login"> У вас уже есть аккаунт? <a href="<c:url value="/login"/>">Войти</a></p>
            </div>
        </form:form>

    </body>

</html>