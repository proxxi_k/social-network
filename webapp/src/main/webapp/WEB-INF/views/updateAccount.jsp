<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="common/header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <title>Update Account</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">

        <script src="http://code.jquery.com/jquery-1.11.0.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/script.js"></script>

        <style>

            input:-webkit-autofill,
            input:-webkit-autofill:hover,
            input:-webkit-autofill:focus,
            input:-webkit-autofill:active {
                box-shadow: 0 0 0 30px #ffffff inset !important;
            }

            .form-control, .btn-save, .btn-cancel, .btn-load {
                box-shadow: none !important;
                outline: none !important;
            }

            * {
                box-sizing: border-box;
            }

            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            .border-container {
                border: 3px solid #f1f1f1;
                width: 34%;
                margin: 60px 33% 0 33%;
                padding-top: 10px;
            }

            h4 {
                text-align: center;
                margin: 10px 0 15px 0;
            }

            .input-group {
                display: flex;
                width: 90%;
                margin: 0 5% 15px 5%;
            }

            .form-control:focus {
                border: 2px solid dodgerblue;
            }

            .input-select:focus {
                border: 2px solid dodgerblue;
            }

            .input-select {
                border: 1px solid #c2c2c2;
            }

            .custom-file-label::after {
                content: "Обзор...";
            }

            .btn-save, .btn-load, .btn-cancel {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                border: none;
                cursor: pointer;
                width: 90%;
                margin: 0 5% 0 5%;
                border-radius: 5px;
            }

            .btn-load {
                margin-bottom: 0.5rem;
            }

            .btn-cancel {
                background-color: #ea8080;
            }

            .btn-save:hover, .btn-load:hover, .btn-cancel:hover {
                opacity: 0.8;
                color: white;
            }

            .incorrectNum p {
                width: 100%;
            }

            .alert-danger {
                width: 100%;
                height: 50px;
            }

            .alert {
                margin-bottom: 0;
            }

            hr {
                width: 90%;
            }

            .custom-file-input {
                /*z-index: 1;*/
            }

        </style>
    </head>
    <body>
        <%-- Form updating account info --%>
        <div class="border-container">
            <h4>Обновление информации об аккаунте</h4>
            <hr>
            <form enctype="multipart/form-data" action="/accounts/${account.id}/read" method="post"
                  onsubmit="return confirm('Загрузить профиль из файла?')">
                <div class="form-group input-group">
                    <input type="file" id="myFile" class="custom-file-input" accept="application/xml"
                           name="file"
                           onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])">
                    <label class="custom-file-label">Выберите файл</label>
                </div>
                <c:if test="${errorXmlFile!=null}">
                    <div class="form-group input-group" id="errorXmlFile">
                        <div class="alert alert-danger">
                            <p>${errorXmlFile}</p>
                        </div>
                    </div>
                </c:if>
                <button type="submit" class="btn-load">Загрузить профиль из файла</button>
            </form>
            <hr>
            <form:form modelAttribute="account" enctype="multipart/form-data" action="/accounts/${account.id}/update"
                       method="post" onsubmit="return validateFormWhenUpdating()">
                <input type="hidden" name="id" value="${account.id}">
                <input type="hidden" name="registrationDate" value="${account.registrationDate}">
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="name" class="form-control" placeholder="Имя" type="text" value="${account.name}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="surname" class="form-control" placeholder="Фамилия" type="text"
                           value="${account.surname}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="patronymic" class="form-control" placeholder="Отчество" type="text"
                           value="${account.patronymic}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-birthday-cake"></i> </span>
                    </div>
                    <input name="birthday" id="birthday" class="form-control" placeholder="birthday" type="date"
                           value="${account.birthday}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-home"></i> </span>
                    </div>
                    <input name="homeAddress" class="form-control" placeholder="Домашний адрес" type="text"
                           value="${account.homeAddress}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-briefcase"></i> </span>
                    </div>
                    <input name="workAddress" class="form-control" placeholder="Рабочий адрес" type="text"
                           value="${account.workAddress}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-skype"></i> </span>
                    </div>
                    <input name="skype" class="form-control" placeholder="Skype" type="text" value="${account.skype}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-info"></i> </span>
                    </div>
                    <textarea name="additionalInfo" class="form-control" placeholder="Дополнительная информация о себе"
                              rows="3">${account.additionalInfo}</textarea>
                </div>
                <div class="phones" id="phones">
                    <c:choose>
                        <%-- without phones --%>
                        <c:when test="${empty account.phones}">
                            <div class="phone">
                                <input type="hidden" class="id" value="0">
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                    </div>
                                    <select class="input-select">
                                        <option value="личный">личный</option>
                                        <option value="домашний">домашний</option>
                                        <option value="рабочий">рабочий</option>
                                    </select>
                                    <input class="form-control" type="text"
                                           placeholder="Телефон">
                                    <input type="button" value="+" onclick="addPhoneWhenUpdating(this)"/>
                                </div>
                            </div>
                        </c:when>
                        <%-- with one phone or multiple phones --%>
                        <c:otherwise>
                            <c:forEach var="phone" items="${account.phones}">
                                <div class="phone">
                                    <input type="hidden" class="id" value="${phone.id}">
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                        </div>
                                        <select class="input-select">
                                            <option value="личный" ${phone.type.equals("личный")?'selected':''}>
                                                личный
                                            </option>
                                            <option value="домашний"
                                                ${phone.type.equals("домашний")?'selected':''}>
                                                домашний
                                            </option>
                                            <option value="рабочий" ${phone.type.equals("рабочий")?'selected':''}>
                                                рабочий
                                            </option>
                                        </select>
                                        <input class="form-control" type="text" placeholder="Телефон"
                                               value="${phone.number}">
                                        <c:choose>
                                            <%-- one phone or last phone --%>
                                            <c:when test="${account.phones.size() == 1 ||
                                            account.phones.indexOf(phone) == account.phones.size()-1}">
                                                <input type="button" value="-" onclick="delPhoneWhenUpdating(this)"/>
                                                <input type="button" value="+" onclick="addPhoneWhenUpdating(this)"/>
                                            </c:when>
                                            <c:otherwise>
                                                <input type="button" value="-" onclick="delPhoneWhenUpdating(this)"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="form-group input-group">
                    <input type="file"
                           id="myAvatar"
                           class="custom-file-input"
                           accept="image/*,image/jpeg,image/png"
                           name="avatar"
                           onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])"
                    >
                    <label class="custom-file-label">Выберите аватар (не более 65КБ)</label>
                </div>
                <div class="form-group input-group">
                    <label>
                        <input type="checkbox" name="deleteAvatar"> Удалить аватар
                    </label>
                </div>
                <c:if test="${errorSizeAvatar!=null}">
                    <div class="form-group input-group" id="errorSizeAvatar">
                        <div class="alert alert-danger">
                            <p>${errorSizeAvatar}</p>
                        </div>
                    </div>
                </c:if>
                <button type="submit" class="btn-save">Сохранить</button>
            </form:form>
            <%-- Cancel button --%>
            <form action="/accounts/${account.id}" method="get">
                <button type="submit" class="btn-cancel">Отмена</button>
            </form>
        </div>

    </body>
</html>