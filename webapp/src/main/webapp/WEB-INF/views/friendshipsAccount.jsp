<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendshipTab" %>
<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.InternetPage" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <title>Friendships Account</title>
        <script src="${pageContext.request.contextPath}/resources/js/script.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>

            form {
                display: block;
                margin-top: 0em;
                margin-block-end: 0.2em;
            }

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .delButton {
                background: none !important;
                border: none;
                padding: 0 !important;
                color: blue;
                cursor: pointer;
                font-size: 14px;
            }

            * {
                text-decoration: none;
            }

            .controls {
                float: right;
                align-self: center;

            }

            .containerFriend, .containerRequestsFrom, .containerRequestsTo, .containerBlacklists {
                border: 1px solid #ccc;
                background-color: #eee;
                border-radius: 5px;
                margin: 10px 10px 10px 10px;
            }

            .containerFriend::after, .containerRequestsFrom::after, .containerRequestsTo::after,
            .containerBlacklists::after {
                content: "";
                clear: both;
                display: table;
            }

            .containerFriend img, .containerRequestsFrom img, .containerRequestsTo img, .containerBlacklists img {
                float: left;
                margin-left: 20px;
                border-radius: 50%;
                padding: 10px 0 10px 0;
            }

            .containerFriend span, .containerRequestsFrom span, .containerRequestsTo span, .containerBlacklists span {
                font-size: 20px;
                margin-right: 15px;
            }

            /* Стиль вкладок */
            .tab {
                overflow: hidden;
                border: 1px solid #ccc;
                background-color: #f1f1f1;
                width: 50%;
                margin: 0 auto;
            }

            /* Стиль кнопок внутри вкладок */
            .tab button {
                background-color: inherit;
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 10px 10px;
                transition: 0.3s;
            }

            /* Изменение цвета фона кнопок при наведении курсора мыши */
            .tab button:hover {
                background-color: #ddd;
            }


            /* Стиль содержимого вкладки */
            .tabcontent {
                display: none;
                border: 1px solid #ccc;
                width: 50%;
                margin: 0 auto;
                height: 650px;
                position: relative;
                padding-bottom: 50px;
            }

            .tabcontent a:active, /* активная/посещенная ссылка */
            .tabcontent a:hover, /* при наведении */
            .tabcontent a {
                text-decoration: none;
                color: #0000FF;
            }

            .friendName {
                display: flex;
                align-self: center;
            }

            .form-group {
                margin: 10px 10px 10px 10px;
            }

            .info {
                display: table;
                margin: 0 auto;
                font-size: 20px;
                padding: 10px 0 10px 0;
            }

            .next, .previous {
                position: absolute;
                bottom: 10px;
            }

            .previous {
                left: 35%;
            }

            .next {
                right: 35%;
            }

            /* Кнопки стиля */
            .btn {
                background-color: DodgerBlue; /* Синий фон */
                border: none; /* Удалить границы */
                color: white; /* Белый текст */
                padding: 12px 16px; /* Немного отступов */
                font-size: 16px; /* Установите размер шрифта */
                cursor: pointer; /* Указатель мыши при наведении */
            }

        </style>

    </head>
    <body>
        <%-- Page scope --%>
        <c:set var="friendshipTab" value="${friendshipTab}" scope="page"/>
        <c:set var="internetPage" value="<%=InternetPage.FRIENDSHIP_ACCOUNT_PAGE%>" scope="page"/>
        <c:set var="friend_tab" value="<%=FriendshipTab.FRIENDS%>" scope="page"/>
        <c:set var="request_from_tab" value="<%=FriendshipTab.REQUESTS_FROM%>" scope="page"/>
        <c:set var="request_to_tab" value="<%=FriendshipTab.REQUESTS_TO%>" scope="page"/>
        <c:set var="blacklist_tab" value="<%=FriendshipTab.BLACKLISTS%>" scope="page"/>

        <%-- Heading --%>
        <div class="info">
            <b>Список друзей пользователя:
                <a href=" <c:url value="/accounts/${account.id}"/>">${account.surname} ${account.name}</a></b>
        </div>

        <%--   Friendship tab bar--%>
        <c:choose>
            <c:when test="${account.id == loginAccount.id}">
                <div class="tab">
                    <button class="tablinks" onclick="openTab(event, 'friends')">Друзья</button>
                    <button class="tablinks" onclick="openTab(event, 'requestsFrom')">Заявки в друзья (исходящие)
                    </button>
                    <button class="tablinks" onclick="openTab(event, 'requestsTo')">Заявки в друзья (входящие)
                    </button>
                    <button class="tablinks" onclick="openTab(event, 'blacklists')">Черный список</button>
                </div>
            </c:when>
            <c:otherwise>
                <div class="tab">
                    <button class="tablinks" onclick="openTab(event, '${friend_tab}')">Друзья</button>
                </div>
            </c:otherwise>
        </c:choose>
        <%--   Friendship tab bar--%>
        <div id="friends" class="tabcontent"></div>
        <%-- Requests from tab --%>
        <div id="requestsFrom" class="tabcontent"></div>
        <%-- Requsts to tab --%>
        <div id="requestsTo" class="tabcontent"></div>
        <%-- Blacklists tab --%>
        <div id="blacklists" class="tabcontent"></div>

        <script>
            window.onload = openTab(event, '${friendshipTab}');
            let numPageFriend = 0;
            let numPageRequestFrom = 0;
            let numPageRequestTo = 0;
            let numPageBlacklist = 0;
            let maxNumPageFriend = ${maxNumPageFriend};
            let maxNumPageRequestFrom = ${maxNumPageRequestFrom};
            let maxNumPageRequestTo = ${maxNumPageRequestTo};
            let maxNumPageBlacklist = ${maxNumPageBlacklist};

            function printFriends(data) {
                if (data.length === 0) {
                    $("#friends").append(
                        '<div class="form-group">' +
                        '<div class="alert alert-danger">' +
                        '<p>Друзья отсутствуют</p>' +
                        '</div>' +
                        '</div>'
                    )
                } else {
                    for (let i = 0; i < data.length; i++) {
                        $("#friends").append(
                            '<div class="row containerFriend">' +
                            '<div class="col-md-2">' +
                            '<img alt="Аватар" style="width:70px" src="<c:url
                            value="/images/avatar/' + data[i].id + '"/>">' +
                            '</div>' +
                            '<div class="col-md-7 friendName">' +
                            '<a href="<c:url value="/accounts/' + data[i].id + '"/>">' + data[i].name + ' ' +
                            (data[i].surname != null ? data[i].surname : '') + '</a>' +
                            '</div>' +
                            '<div class="col-md-3 controls">' +
                            '<c:if test="${account.id == loginAccount.id}">' +
                            '<form action="/accounts/${loginAccount.id}/friendships/' + data[i].id + '/delete"method="post">' +
                            '<input type="hidden" name="internetPage" value="${internetPage}">' +
                            '<input type="hidden" name="friendshipTab" value="${friend_tab}">' +
                            '<button type="submit" class="delButton">Удалить из друзей</button>' +
                            '</form>' +
                            '<form action="/accounts/${loginAccount.id}/friendships/' + data[i].id + '/block" method = "post">' +
                            '<input type = "hidden" name = "internetPage" value = "${internetPage}">' +
                            '<input type = "hidden" name = "friendshipTab" value = "${friend_tab}">' +
                            '<button type = "submit" class= "delButton">Добавить в черный список</button>' +
                            '</form>' +
                            '</c:if>' +
                            '</div>' +
                            '</div>'
                        )
                    }
                }
                if (numPageFriend !== 0) {
                    $("#friends").append(
                        '<button type="submit" class="btn previous" id="prevFriendsButton">Назад</button>'
                    )
                }
                if (numPageFriend !== maxNumPageFriend) {
                    $("#friends").append(
                        '<button type="submit" class="btn next" id="nextFriendsButton">Вперед</button>'
                    )
                }
            }

            function printRequestsFrom(data) {
                if (data.length === 0) {
                    $("#requestsFrom").append(
                        '<div class="form-group">' +
                        '<div class="alert alert-danger">' +
                        '<p>Вы пока не отправили ни одной заявки в друзья</p>' +
                        '</div>' +
                        '</div>'
                    )
                } else {
                    for (let i = 0; i < data.length; i++) {
                        $("#requestsFrom").append(
                            '<div class="row containerRequestsFrom">' +
                            '<div class="col-md-2">' +
                            '<img alt="Аватар" style="width:70px" src="<c:url
                            value="/images/avatar/' + data[i].id + '"/>">' +
                            '</div>' +
                            '<div class="col-md-7 friendName">' +
                            '<a href="<c:url value="/accounts/' + data[i].id + '"/>">' + data[i].name + ' ' +
                            (data[i].surname != null ? data[i].surname : '') + '</a>' +
                            '</div>' +
                            '<div class="col-md-3 controls">' +
                            '<c:if test="${account.id == loginAccount.id}">' +
                            '<form action="/accounts/${loginAccount.id}/friendships/' + data[i].id + '/delete"method="post">' +
                            '<input type="hidden" name="internetPage" value="${internetPage}">' +
                            '<input type="hidden" name="friendshipTab" value="${request_from_tab}">' +
                            '<button type="submit" class="delButton">Отозвать заявку</button>' +
                            '</form>' +
                            '<form action="/accounts/${loginAccount.id}/friendships/' + data[i].id + '/block" method = "post">' +
                            '<input type = "hidden" name = "internetPage" value = "${internetPage}">' +
                            '<input type = "hidden" name = "friendshipTab" value = "${request_from_tab}">' +
                            '<button type = "submit" class= "delButton">Добавить в черный список</button>' +
                            '</form>' +
                            '</c:if>' +
                            '</div>' +
                            '</div>'
                        )
                    }
                }
                if (numPageRequestFrom !== 0) {
                    $("#requestsFrom").append(
                        '<button type="submit" class="btn previous" id="prevRequestsFromButton">Назад</button>'
                    )
                }
                if (numPageRequestFrom !== maxNumPageRequestFrom) {
                    $("#requestsFrom").append(
                        '<button type="submit" class="btn next" id="nextRequestsFromButton">Вперед</button>'
                    )
                }
            }

            function ajaxGetFriends(numPage) {
                $.ajax({
                    url: "<c:url value='/accounts/${account.id}/api/friendships/friends'/>",
                    data: {
                        numPage: numPage
                    },
                    success: function (data) {
                        printFriends(data);
                    }
                });
            }

            function printRequestsTo(data) {
                if (data.length === 0) {
                    $("#requestsTo").append(
                        '<div class="form-group">' +
                        '<div class="alert alert-danger">' +
                        '<p>У вас пока нет заявок в друзья</p>' +
                        '</div>' +
                        '</div>'
                    )
                } else {
                    for (let i = 0; i < data.length; i++) {
                        $("#requestsTo").append(
                            '<div class="row containerRequestsTo">' +
                            '<div class="col-md-2">' +
                            '<img alt="Аватар" style="width:70px" src="<c:url
                            value="/images/avatar/' + data[i].id + '"/>">' +
                            '</div>' +
                            '<div class="col-md-7 friendName">' +
                            '<a href="<c:url value="/accounts/' + data[i].id + '"/>">' + data[i].name + ' ' +
                            (data[i].surname != null ? data[i].surname : '') + '</a>' +
                            '</div>' +
                            '<div class="col-md-3 controls">' +
                            '<c:if test="${account.id == loginAccount.id}">' +
                            '<form action="/accounts/${loginAccount.id}/friendships/' + data[i].id + '/add "method="post">' +
                            '<input type="hidden" name="internetPage" value="${internetPage}">' +
                            '<input type="hidden" name="friendshipTab" value="${request_to_tab}">' +
                            '<button type="submit" class="delButton">Добавить в друзья</button>' +
                            '</form>' +
                            '<form action="/accounts/${loginAccount.id}/friendships/' + data[i].id + '/delete"method="post">' +
                            '<input type="hidden" name="internetPage" value="${internetPage}">' +
                            '<input type="hidden" name="friendshipTab" value="${request_to_tab}">' +
                            '<button type="submit" class="delButton">Отклонить заявку</button>' +
                            '</form>' +
                            '<form action="/accounts/${loginAccount.id}/friendships/' + data[i].id + '/block" method = "post">' +
                            '<input type = "hidden" name = "internetPage" value = "${internetPage}">' +
                            '<input type = "hidden" name = "friendshipTab" value = "${request_to_tab}">' +
                            '<button type = "submit" class= "delButton">Добавить в черный список</button>' +
                            '</form>' +
                            '</c:if>' +
                            '</div>' +
                            '</div>'
                        )
                    }
                }
                if (numPageRequestTo !== 0) {
                    $("#requestsTo").append(
                        '<button type="submit" class="btn previous" id="prevRequestsToButton">Назад</button>'
                    )
                }
                if (numPageRequestTo !== maxNumPageRequestTo) {
                    $("#requestsTo").append(
                        '<button type="submit" class="btn next" id="nextRequestsToButton">Вперед</button>'
                    )
                }
            }

            function printBlacklists(data) {
                if (data.length === 0) {
                    $("#blacklists").append(
                        '<div class="form-group">' +
                        '<div class="alert alert-danger">' +
                        '<p>Черный список пуст</p>' +
                        '</div>' +
                        '</div>'
                    )
                } else {
                    for (let i = 0; i < data.length; i++) {
                        $("#blacklists").append(
                            '<div class="row containerBlacklists">' +
                            '<div class="col-md-2">' +
                            '<img alt="Аватар" style="width:70px" src="<c:url
                            value="/images/avatar/' + data[i].id + '"/>">' +
                            '</div>' +
                            '<div class="col-md-7 friendName">' +
                            '<a href="<c:url value="/accounts/' + data[i].id + '"/>">' + data[i].name + ' ' +
                            (data[i].surname != null ? data[i].surname : '') + '</a>' +
                            '</div>' +
                            '<div class="col-md-3 controls">' +
                            '<c:if test="${account.id == loginAccount.id}">' +
                            '<form action="/accounts/${loginAccount.id}/friendships/' + data[i].id + '/delete"method="post">' +
                            '<input type="hidden" name="internetPage" value="${internetPage}">' +
                            '<input type="hidden" name="friendshipTab" value="${blacklist_tab}">' +
                            '<button type="submit" class="delButton">Удалить из черного списка</button>' +
                            '</form>' +
                            '</c:if>' +
                            '</div>' +
                            '</div>'
                        )
                    }
                }
                if (numPageBlacklist !== 0) {
                    $("#blacklists").append(
                        '<button type="submit" class="btn previous" id="prevBlacklistsButton">Назад</button>'
                    )
                }
                if (numPageBlacklist !== maxNumPageBlacklist) {
                    $("#blacklists").append(
                        '<button type="submit" class="btn next" id="nextBlacklistsButton">Вперед</button>'
                    )
                }
            }

            function ajaxGetFriends(numPage) {
                $.ajax({
                    url: "<c:url value='/accounts/${account.id}/api/friendships/friends'/>",
                    data: {
                        numPage: numPage
                    },
                    success: function (data) {
                        printFriends(data);
                    }
                });
            }

            function ajaxGetRequestsFrom(numPage) {
                $.ajax({
                    url: "<c:url value='/accounts/${account.id}/api/friendships/requests-from'/>",
                    data: {
                        numPage: numPage
                    },
                    success: function (data) {
                        printRequestsFrom(data);
                    }
                });
            }

            function ajaxGetRequestsTo(numPage) {
                $.ajax({
                    url: "<c:url value='/accounts/${account.id}/api/friendships/requests-to'/>",
                    data: {
                        numPage: numPage
                    },
                    success: function (data) {
                        printRequestsTo(data);
                    }
                });
            }

            function ajaxGetBlacklists(numPage) {
                $.ajax({
                    url: "<c:url value='/accounts/${account.id}/api/friendships/blacklists'/>",
                    data: {
                        numPage: numPage
                    },
                    success: function (data) {
                        printBlacklists(data);
                    }
                });
            }

            function removeFriendsAndButtons() {
                let groups = document.querySelectorAll("div.containerFriend");
                for (let i = 0; i < groups.length; i++) {
                    groups[i].remove();
                }
                if (document.getElementById('nextFriendsButton') != null) {
                    document.getElementById('nextFriendsButton').remove();
                }
                if (document.getElementById('prevFriendsButton') != null) {
                    document.getElementById('prevFriendsButton').remove();
                }
            }

            function removeRequestsFromAndButtons() {
                let groups = document.querySelectorAll("div.containerRequestsFrom");
                for (let i = 0; i < groups.length; i++) {
                    groups[i].remove();
                }
                if (document.getElementById('nextRequestsFromButton') != null) {
                    document.getElementById('nextRequestsFromButton').remove();
                }
                if (document.getElementById('prevRequestsFromButton') != null) {
                    document.getElementById('prevRequestsFromButton').remove();
                }
            }

            function removeRequestsToAndButtons() {
                let groups = document.querySelectorAll("div.containerRequestsTo");
                for (let i = 0; i < groups.length; i++) {
                    groups[i].remove();
                }
                if (document.getElementById('nextRequestsToButton') != null) {
                    document.getElementById('nextRequestsToButton').remove();
                }
                if (document.getElementById('prevRequestsToButton') != null) {
                    document.getElementById('prevRequestsToButton').remove();
                }
            }

            function removeBlacklistsAndButtons() {
                let groups = document.querySelectorAll("div.containerBlacklists");
                for (let i = 0; i < groups.length; i++) {
                    groups[i].remove();
                }
                if (document.getElementById('nextBlacklistsButton') != null) {
                    document.getElementById('nextBlacklistsButton').remove();
                }
                if (document.getElementById('prevBlacklistsButton') != null) {
                    document.getElementById('prevBlacklistsButton').remove();
                }
            }
        </script>

        <script>
            $(document).on('click', "#nextFriendsButton", function () {
                removeFriendsAndButtons();
                ajaxGetFriends(++numPageFriend);
            })
            $(document).on('click', "#prevFriendsButton", function () {
                removeFriendsAndButtons();
                ajaxGetFriends(--numPageFriend);
            })
            $(document).on('click', "#nextRequestsFromButton", function () {
                removeRequestsFromAndButtons();
                ajaxGetRequestsFrom(++numPageRequestFrom);
            })
            $(document).on('click', "#prevRequestsFromButton", function () {
                removeRequestsFromAndButtons();
                ajaxGetRequestsFrom(--numPageRequestFrom);
            })
            $(document).on('click', "#nextRequestsToButton", function () {
                removeRequestsToAndButtons();
                ajaxGetRequestsTo(++numPageRequestTo);
            })
            $(document).on('click', "#prevRequestsToButton", function () {
                removeRequestsToAndButtons();
                ajaxGetRequestsTo(--numPageRequestTo);
            })
            $(document).on('click', "#nextBlacklistsButton", function () {
                removeBlacklistsAndButtons();
                ajaxGetBlacklists(++numPageBlacklist);
            })
            $(document).on('click', "#prevBlacklistsButton", function () {
                removeBlacklistsAndButtons();
                ajaxGetBlacklists(--numPageBlacklist);
            })
        </script>

        <script>
            $(document).ready(function () {
                if (${account.id} ===
                ${loginAccount.id})
                {
                    ajaxGetFriends(numPageFriend);
                    ajaxGetRequestsFrom(numPageRequestFrom);
                    ajaxGetRequestsTo(numPageRequestTo);
                    ajaxGetBlacklists(numPageBlacklist);
                }
            else
                {
                    ajaxGetFriends(numPageFriend);
                }
            });
        </script>

    </body>
</html>
