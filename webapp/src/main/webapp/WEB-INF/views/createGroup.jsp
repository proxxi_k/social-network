<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="common/header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <title>Create Group</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">

        <script src="http://code.jquery.com/jquery-1.11.0.js"></script>

        <style>

            input:-webkit-autofill,
            input:-webkit-autofill:hover,
            input:-webkit-autofill:focus,
            input:-webkit-autofill:active {
                box-shadow: 0 0 0 30px #ffffff inset !important;
            }

            .form-control, .btn-save {
                box-shadow: none !important;
                outline: none !important;
            }

            * {
                box-sizing: border-box;
            }

            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            .border-container {
                border: 3px solid #f1f1f1;
                width: 34%;
                margin: 60px 33% 0 33%;
                padding-top: 10px;
            }

            h4 {
                text-align: center;
                margin: 10px 0 15px 0;
            }

            .input-group {
                display: flex;
                width: 90%;
                margin: 0 5% 15px 5%;
            }

            .form-control:focus {
                border: 2px solid dodgerblue;
            }

            .custom-file-label::after {
                content: "Обзор...";
            }

            .btn-save, .btn-cancel {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                border: none;
                cursor: pointer;
                width: 90%;
                margin: 0 5% 0 5%;
                border-radius: 5px;
            }

            .btn-cancel {
                background-color: #ea8080;
            }

            .btn-save:hover, .btn-cancel:hover {
                opacity: 0.8;
                color: white;
            }

            .incorrectNum p {
                width: 100%;
            }

            .alert-danger {
                width: 100%;
                height: 50px;
            }

            .alert {
                margin-bottom: 0;
            }

        </style>

    </head>
    <body>
        <%-- Form creating new group --%>
        <div class="border-container">
            <form:form modelAttribute="group" enctype="multipart/form-data" action="/groups/create" method="post">
                <h4>Создание новой группы</h4>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="name" type="text" class="form-control" placeholder="Имя группы" required
                           value="${group.name == null ? '': group.name}"/>
                </div>
                <c:if test="${errorName!=null}">
                    <div class="form-group input-group">
                        <div class="alert alert-danger">
                            <p>${errorName}</p>
                        </div>
                    </div>
                </c:if>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-info"></i> </span>
                    </div>
                    <textarea name="description" class="form-control" placeholder="Описание группы"
                              rows="3">${group.description == null ? '' : group.description}</textarea>
                </div>
                <div class="form-group input-group">
                    <input type="file" id="myfile" class="custom-file-input" accept="image/*,image/jpeg,image/png"
                           name="image"
                           onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])">
                    <label class="custom-file-label">Выберите изображение (не более 65КБ)</label>
                </div>
                <c:if test="${errorSizeImage!=null}">
                    <div class="form-group input-group">
                        <div class="alert alert-danger">
                            <p>${errorSizeImage}</p>
                        </div>
                    </div>
                </c:if>
                <button type="submit" class="btn-save">Создать группу</button>
            </form:form>

            <%-- Cancel button --%>
            <form action="/accounts/${loginAccount.id}/memberships" method="get">
                <input type="submit" class="btn-cancel" value="Отмена">
            </form>
        </div>

    </body>
</html>