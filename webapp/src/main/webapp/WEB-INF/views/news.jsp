<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">

        <title>All Accounts</title>

        <style>

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .containerNews {
                border: 1px solid #ccc;
                background-color: #eee;
                border-radius: 5px;
                margin: 10px 10px 10px 10px;
            }

            .containerNews::after {
                content: "";
                clear: both;
                display: table;
            }

            .friendName {
                display: flex;
                align-self: center;
            }

            .containerNews img {
                float: left;
                margin-left: 20px;
                border-radius: 50%;
                padding: 10px 0 10px 0;
            }

            .containerNews span {
                font-size: 20px;
                margin-right: 15px;
            }

            .next, .previous {
                position: absolute;
                bottom: 0;
            }

            .previous {
                left: 35%;
            }

            .next {
                right: 35%;
            }

            /* Кнопки стиля */
            .btn {
                background-color: DodgerBlue; /* Синий фон */
                border: none; /* Удалить границы */
                color: white; /* Белый текст */
                padding: 12px 16px; /* Немного отступов */
                font-size: 16px; /* Установите размер шрифта */
                cursor: pointer; /* Указатель мыши при наведении */
            }

            /* Более темный фон при наведении курсора мыши */
            .btn:hover {
                background-color: #56abfd;
                color: white;
            }

            .border-container {
                width: 50%;
                margin: 0 25% 0 25%;
                height: 680px;
                position: relative;
                padding-bottom: 50px;
            }

            .info {
                display: table;
                margin: 0 auto;
                font-size: 20px;
                padding: 10px 0 10px 0;
            }

            .sender {
                line-height: 1em;
                display: table;
                margin: 0.5em auto;
                text-align: center;
            }

            .imgMessage {
                border-radius: 50px;
                display: table;
                margin: 0 auto;
            }

            .textMessage {
                display: flex;
                align-self: center;
            }

            .col-md-2 {
                font-size: 12px;
            }

            .newsDate {
                align-self: center;
            }

        </style>

    </head>
    <body>
        <%-- Page scope --%>
        <c:set var="today" value="<%=new java.util.Date()%>"/>

        <div class="info">
            <b>Новости пользователя:
                <a href=" <c:url value="/accounts/${account.id}"/>">${account.surname} ${account.name}</a></b>
        </div>
        <div class="border-container">
            <div id="news">
            </div>
        </div>

        <script>
            let numPage = 0;
            let maxNumPage = ${maxNumPage};

            function printNews(data) {
                if (numPage === 0 && data.length === 0) {
                    $("#news").append(
                        '<div class="form-group">' +
                        '<div class="alert alert-danger">' +
                        '<p>У вас пока нет новостей</p>' +
                        '</div>' +
                        '</div> '
                    );
                } else {
                    for (let i = 0; i < data.length; i++) {
                        $("#news").append(
                            '<div class="row containerNews">' +
                            '<div class="col-md-2">' +
                            '<img class="imgMessage" alt="Аватар" style="width:50px" src="<c:url
                        value="/images/avatar/' + data[i].friendId + '"/>">' +
                            '<a class="sender" href="<c:url value="/accounts/' + data[i].friendId + '"/>">' + data[i].friendName + ' ' +
                            (data[i].friendSurname != null ? data[i].friendSurname : '') + '</a>' +
                            '</div>' +
                            '<div class="col-md-8 textMessage">' +
                            'опубликовал новый пост: "' + data[i].newsMessage + '"' +
                            '</div>' +
                            '<div class="col-md-2 newsDate">' +
                            formatDate(new Date(data[i].newsDate)) +
                            '</div>' +
                            '</div>'
                        )
                    }
                    if (numPage !== 0) {
                        $("#news").append(
                            '<button type="submit" class="btn previous" id="prevNewsButton">Назад</button>'
                        )
                    }
                    if (numPage !== maxNumPage) {
                        $("#news").append(
                            '<button type="submit" class="btn next" id="nextNewsButton">Вперед</button>'
                        )
                    }
                }
            }

            function removeNewsAndButtons() {
                let news = document.querySelectorAll("div.containerNews");
                for (let i = 0; i < news.length; i++) {
                    news[i].remove();
                }
                if (document.getElementById('nextNewsButton') != null) {
                    document.getElementById('nextNewsButton').remove();
                }
                if (document.getElementById('prevNewsButton') != null) {
                    document.getElementById('prevNewsButton').remove();
                }
            }

            function ajaxGetAllNews(numPage) {
                $.ajax({
                    url: "<c:url value='/accounts/${account.id}/news/api'/>",     // URL - сервлета
                    data: {                                                       // передаваемые сервлету данные
                        numPage: numPage
                    },
                    success: function (data) {
                        // обработка ответа от сервера
                        printNews(data);
                    }
                });
            }
        </script>

        <script>
            // вызов функции по завершению загрузки страницы
            $(document).ready(function () {
                ajaxGetAllNews(numPage);
            });
        </script>

        <script>
            $(document).on('click', "#nextNewsButton", function () {
                removeNewsAndButtons();
                ajaxGetAllNews(++numPage);
            })
        </script>

        <script>
            $(document).on('click', "#prevNewsButton", function () {
                removeNewsAndButtons();
                ajaxGetAllNews(--numPage);
            })
        </script>

        <script>
            function formatDate(date) {
                let dayOfMonth = date.getDate();
                let month = date.getMonth() + 1;
                let year = date.getFullYear();
                let hour = date.getHours();
                let minutes = date.getMinutes();
                let diffMs = new Date() - date;
                let diffSec = Math.floor(diffMs / 1000);
                let diffMin = Math.floor(diffSec / 60);
                let diffHour = Math.floor(diffMin / 60);
                let diffDay = Math.floor(diffHour / 24);

                // форматирование
                year = year.toString().slice(-2);
                month = month < 10 ? '0' + month : month;
                dayOfMonth = dayOfMonth < 10 ? '0' + dayOfMonth : dayOfMonth;
                hour = hour < 10 ? '0' + hour : hour;
                minutes = minutes < 10 ? '0' + minutes : minutes;

                if (diffSec < 1) {
                    return 'прямо сейчас';
                } else if (diffMin < 1) {
                    return diffSec + ' сек. назад'
                } else if (diffHour < 1) {
                    return diffMin + ' мин. назад'
                } else if (diffDay < 1) {
                    return 'Сегодня, ' + hour + ':' + minutes
                } else {
                    return dayOfMonth + '.' + month + '.' + year + ' ' + hour + ':' + minutes
                }
            }
        </script>

    </body>
</html>