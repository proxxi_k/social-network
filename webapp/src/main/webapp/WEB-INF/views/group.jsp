<%@ page import="com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="common/header.jsp" %>
<html>
    <head>
        <title>Group</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossorigin="anonymous"></script>
        <style>

            .form-control, .btn-save {
                box-shadow: none !important;
                outline: none !important;
            }

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            .form-control {
                margin-top: 10px;
            }

            .form-control:focus {
                border: 2px solid dodgerblue;
            }

            .side {
                margin-left: 2%;
            }

            .btn-secondary, .btn-secondary:focus, .btn-secondary:hover, .btn-secondary:active {
                color: blue;
                background-color: white;
                border: none;
                background: none !important;
                padding: 0 !important;
                cursor: pointer;
                font-size: 16px;
            }

            .dropdown {
                margin-top: 1em;
                position: relative;
                display: inline-block;
                margin-bottom: 1em;
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown a:hover {
                background-color: #ddd;
            }

            .delButton {
                background: none !important;
                border: none;
                padding: 0 !important;
                color: blue;
                cursor: pointer;
                font-size: 1rem;
            }

            /* Создайте три неравных столбца, которые плавают рядом друг с другом */
            .column {
                float: left;
                padding: 10px;
            }

            /* Левый и правый столбец */
            .column.side {
                width: 20%;
            }

            /* Средний столбец */
            .column.middle {
                width: 40%;
            }

            .btn-save {
                background-color: #4CAF50;
                color: white;
                padding: 8px;
                border: none;
                cursor: pointer;
                width: 30%;
                margin-top: 10px;
                border-radius: 5px;
            }

            /* Очистить float после столбцов */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            .login-container img {
                margin-right: 20px;
                margin-bottom: 20px;
            }

            .delButton {
                text-align: left;
            }

            body {
                font-family: Arial, Helvetica, sans-serif;
                line-height: 1.5rem;
            }

            .row.about {
                margin-top: 0;
                padding-top: 10px;
            }

            .aboutInfo {
                min-height: 272px;
            }

            b {
                font-size: 25px;
                color: rgb(66, 66, 66);
            }

            b.description {
                font-size: small;
            }

            .imgMessage {
                border-radius: 50px;
                display: table;
                margin: 0 auto;
            }

            .col-md-2 {
                font-size: 12px;
            }

            hr {
                margin: 0 0 5px 0;
            }

            .sender {
                line-height: 1em;
                display: table;
                margin: 0.5em auto;
                text-align: center;
            }

            .textMessage {
                display: flex;
                align-self: center;
            }

            .controlMessage {
                align-self: center;
            }

        </style>

    </head>
    <body>
        <%-- Page scope --%>
        <c:set var="internetPage" value="GROUP_PAGE" scope="page"/>
        <c:set var="today" value="<%=new java.util.Date()%>"/>
        <c:set var="creator" value="<%=MembershipStatus.CREATOR%>" scope="page"/>
        <c:set var="moderator" value="<%=MembershipStatus.MODERATOR%>" scope="page"/>
        <c:set var="user" value="<%=MembershipStatus.USER%>" scope="page"/>
        <c:set var="request" value="<%=MembershipStatus.REQUEST%>" scope="page"/>

        <div class="row justify-content-center">
            <div class="column side">
                <div class="login-container">
                    <img src="<c:url value="/images/group-image/${group.id}"/>" alt="Изображение группы"
                         style="width:150px">
                </div>
                <%-- Memberships menu --%>
                <c:choose>
                    <%-- Creator --%>
                    <c:when test="${membershipStatus == creator}">
                        <div class="dropdown">
                            <button class="btn btn-secondary" type="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">Владелец группы
                            </button>
                        </div>
                    </c:when>
                    <%-- Order/User/Moderator --%>
                    <c:when test="${membershipStatus == request || membershipStatus == user || membershipStatus ==
                    moderator}">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    ${membershipStatus == request ? "Заявка отправлена" :
                                            membershipStatus == user ? "Участник": "Модератор"}
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li>
                                    <form action="/groups/${group.id}/members/${loginAccount.id}/delete" method="post">
                                        <input type="hidden" name="internetPage" value="${internetPage}">
                                        <input type="submit" class="delButton"
                                               value="${membershipStatus == request ? "Отозвать заявку" :
                                               "Выйти из группы"}">
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </c:when>
                    <%-- No memberships --%>
                    <c:otherwise>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">Вступить в группу
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li>
                                    <form action="/groups/${group.id}/members/request" method="post">
                                        <input type="submit" class="delButton" value="Вступить в группу">
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </c:otherwise>
                </c:choose>

                <%--Common panel--%>
                <p><a href="<c:url value="/groups/${group.id}/members"/>">Участники</a></p>

                <c:if test="${membershipStatus == creator || membershipStatus == moderator || loginAccount.auth.admin}">
                    <hr>
                </c:if>

                <%--Moderator/Creator panel--%>
                <c:if test="${membershipStatus == moderator || membershipStatus == creator}">
                    <p><a href="<c:url value="/groups/${group.id}/update"/>">Редактировать информацию о
                        группе</a></p>
                </c:if>

                <%--Creator/Admin panel--%>
                <c:if test="${membershipStatus == creator || loginAccount.auth.admin}">
                    <form action="/groups/${group.id}/delete" method="post"
                          onsubmit="return confirm('Вы действительно хотите удалить группу?')">
                        <input type="hidden" name="internetPage" value="${internetPage}">
                        <button type="submit" class="delButton">Удалить группу</button>
                    </form>
                </c:if>
            </div>
            <div class="column middle">
                <%-- Group Info --%>
                <div class="aboutInfo">
                    <b><p>${group.name}</p></b>
                    <c:if test="${group.description!=null}">
                        <div class="row about">
                            <div class="col-md-3 about">
                                <b class="description"><i><label>Описание:</label></i></b>
                            </div>
                            <div class="col-md-9">
                                    ${group.description}
                            </div>
                        </div>
                    </c:if>
                    <div class="row about">
                        <div class="col-md-3 about">
                            <b class="description"><i><label>Дата создания:</label></i></b>
                        </div>
                        <div class="col-md-9">
                            ${group.creationDate}
                        </div>
                    </div>
                    <div class="row about">
                        <div class="col-md-3 about">
                            <b class="description"><i><label>Владелец группы:</label></i></b>
                        </div>
                        <div class="col-md-9">
                            <c:choose>
                                <c:when test="${group.creator!=null}">
                                    <a href="<c:url value="/accounts/${group.creator.id}"/>">${group.creator.name}
                                            ${group.creator.surname}</a>
                                </c:when>
                                <c:otherwise>
                                    <label>отсутствует</label>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <hr>
                <%-- Send Post --%>
                <%-- User/Moderator/Creator   --%>
                <div>
                    <c:if test="${membershipStatus == creator || membershipStatus == moderator ||
                    membershipStatus == user}">
                        <form action="/messages/${group.id}/send" method="post">
                            <input type="hidden" name="internetPage" value="${internetPage}">
                            <div class="input-container">
                                <textarea class="form-control" rows="3" name="messageBody"></textarea>
                                <button type="submit" class="btn-save">Опубликовать</button>
                            </div>
                        </form>
                    </c:if>
                    <c:if test="${not empty wallMessages}">
                        <hr>
                    </c:if>
                    <%-- Group Wall --%>
                    <c:forEach var="wallMessage" items="${wallMessages}">
                        <div class="row sendPost">
                            <div class="col-md-2">
                                <img class="imgMessage" src="<c:url value="/images/avatar/${wallMessage.sender.id}"/>"
                                     alt="Аватар"
                                     style="width:50px">
                                <a class="sender" href="<c:url value="/accounts/${wallMessage.sender.id}"/>">
                                        ${wallMessage.sender.surname} ${wallMessage.sender.name}
                                </a>
                            </div>
                            <div class="col-md-8 textMessage">
                                    ${wallMessage.message}
                            </div>
                            <div class="col-md-2 controlMessage">
                                <c:choose>
                                    <c:when test="${wallMessage.date.date  == today.date}">
                                        <fmt:formatDate type="time" value="${wallMessage.date}"
                                                        pattern="HH:mm:ss"/>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:formatDate type="time" value="${wallMessage.date}"
                                                        pattern="dd.MM.yyyy"/>
                                    </c:otherwise>
                                </c:choose>
                                    <%-- Moderator/Creator Control--%>
                                <c:if test="${membershipStatus == moderator || membershipStatus == creator}">
                                    <form action="/messages/${wallMessage.id}/delete" method="post">
                                        <input type="hidden" name="groupId" value="${group.id}">
                                        <input type="hidden" name="internetPage" value="${internetPage}">
                                        <button type="submit" class="delButton">Удалить</button>
                                    </form>
                                </c:if>
                            </div>
                        </div>
                        <hr>
                    </c:forEach>
                </div>
            </div>
        </div>

    </body>
</html>