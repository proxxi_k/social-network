<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <script src="${pageContext.request.contextPath}resources/js/script.js"></script>
        <style>

            input:-webkit-autofill,
            input:-webkit-autofill:hover,
            input:-webkit-autofill:focus,
            input:-webkit-autofill:active {
                box-shadow: 0 0 0 30px #ffffff inset !important;
            }

            button {
                box-shadow: none !important;
                outline: none !important;
            }

            * a:active, * a:hover, * a {
                text-decoration: none;
                color: blue;
            }

            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            label {
                margin-bottom: 0;
            }

            input[type=email], input[type=password], input[type=text] {
                width: 100%;
                height: 40px;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #cccccc;
                box-sizing: border-box;
            }

            input[type=email]:focus, input[type=password]:focus, input[type=text]:focus {
                outline: none;
                border: 2px solid dodgerblue;
            }

            button {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                cursor: pointer;
                width: 100%;
                border-radius: 5px;
                height: 50px;
            }

            button:hover {
                opacity: 0.8;
            }

            .img-container {
                text-align: center;
                margin-bottom: 20px;
            }

            img.avatar {
                width: 40%;
                border-radius: 50%;
            }

            .border-container {
                border: 3px solid #f1f1f1;
                width: 30%;
                margin: 10px 35% 0 35%;
            }

            .login-container {
                padding-top: 1%;
                width: 80%;
                margin: 0 10% 20px 10%;
            }

            #registration {
                float: right;
                margin-top: -10px;
            }

            .alert-danger {
                width: 100%;
                margin-bottom: 0;
                height: 50px;
            }

        </style>
    </head>
    <body>
        <%-- Form login --%>
        <form action="login" method="post">
            <div class="border-container">
                <div class="img-container">
                    <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default-avatar.jpg"
                         alt="Аватар">
                </div>
                <div class="login-container">
                    <label for="email"><b>Email</b></label>
                    <input type="email" id="email" placeholder="Введите email" name="email"
                           value="${email == null ? '' : email}"
                           required>
                    <label for="password"><b>Пароль</b></label>
                    <div class="password">
                        <input type="password" id="password" placeholder="Введите пароль"
                               name="password" required>
                        <a href="#" onclick="show_hide_password(this)">Показать пароль</a>
                    </div>
                    <c:if test="${errorLogin!=null}">
                        <div class="alert alert-danger">
                            <p>${errorLogin}</p>
                        </div>
                    </c:if>
                    <c:if test="${logout!=null}">
                        <div class="alert alert-danger">
                            <p>${logout}</p>
                        </div>
                    </c:if>
                    <button type="submit">Вход</button>
                    <label>
                        <input type="checkbox" name="remember-me"> Запомнить меня
                    </label>
                </div>
                <div class="login-container">
                    <label id="registration">
                        У вас еще нет аккаунта? <a href=" <c:url value="/accounts/registration"/>"> Регистрация</a>
                    </label>
                </div>
            </div>
        </form>
    </body>
</html>