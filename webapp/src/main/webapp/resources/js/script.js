// login.jsp/registrationAccount.jsp
function show_hide_password(target) {
    const input = document.getElementById('password');
    if (input.getAttribute('type') === 'password') {
        target.innerHTML = 'Скрыть пароль';
        input.setAttribute('type', 'text');
    } else {
        target.innerHTML = 'Показать пароль';
        input.setAttribute('type', 'password');
    }
    return false;
}

// updateAuth.jsp
function show_hide_passwords(target) {
    const currentPassword = document.getElementById('currentPassword');
    const newPassword = document.getElementById('newPassword');
    if (currentPassword.getAttribute('type') === 'password') {
        target.innerHTML = 'Скрыть пароли';
        currentPassword.setAttribute('type', 'text');
        newPassword.setAttribute('type', 'text');
    } else {
        target.innerHTML = 'Показать пароли';
        currentPassword.setAttribute('type', 'password');
        newPassword.setAttribute('type', 'password');
    }
    return false;
}

// registrationAccount.jsp
function addPhoneWhenRegistering(btn) {
    deleteErrorMsg('errorNumber');
    let phones = document.getElementById('phones');
    let re = /^(8|\+7)\d{10}$/;
    const number = btn.previousElementSibling.value;
    let isValid = re.test(number);
    if (isValid) {
        let newPhone = document.createElement('div');
        newPhone.classList.add("phone");
        newPhone.innerHTML = '' +
            '<div class="form-group input-group">' +
            '<div class="input-group-prepend">' +
            '<span class="input-group-text"> <i class="fa fa-phone"></i> </span>' +
            '</div>' +
            '<select class="input-select">' +
            '<option value="личный">личный</option>' +
            '<option value="домашний">домашний</option>' +
            '<option value="рабочий">рабочий</option>' +
            '</select>' +
            '<input class="form-control" type="text" placeholder="Телефон">' +
            '<input type="button" value="+" onclick="addPhoneWhenRegistering(this)"/>' +
            '</div>';
        phones.appendChild(newPhone);
        let prevPhone = newPhone.previousElementSibling;
        prevPhone.firstElementChild.lastElementChild.remove();
        let delButton = document.createElement('input');
        delButton.type = "button";
        delButton.value = "-";
        delButton.setAttribute("onclick", "delPhoneWhenRegistering(this)");
        prevPhone.firstElementChild.appendChild(delButton);
    } else {
        createPhoneError(btn.parentElement.parentElement);
    }
}

// registrationAccount.jsp
function delPhoneWhenRegistering(btn) {
    btn.parentElement.parentElement.remove();
}

// registrationAccount.jsp
function validateFormWhenRegistration() {
    // delete an error message
    deleteAllErrorMessages();
    // numbering of phone fields
    let phones = document.getElementById('phones');
    let phoneTypeInputs = phones.querySelectorAll("select.input-select");
    let phoneNumberInputs = phones.querySelectorAll("input.form-control");
    for (let i = 0; i < phoneNumberInputs.length; i++) {
        if (phoneNumberInputs[i].value !== '') {
            phoneTypeInputs[i].name = "phones[" + i + "].type";
            phoneNumberInputs[i].name = "phones[" + i + "].number";
        }
    }
    // phone validation
    let isValidatePhone = validatePhone(phoneNumberInputs);
    // disable the date of birth field when not filled in
    if (isValidatePhone === true) {
        let birthday = document.getElementById('birthday');
        if (birthday.value === '') birthday.disabled = true;
    }
    return isValidatePhone;
}

// updateAccount.jsp
function addPhoneWhenUpdating(btn) {
    deleteErrorMsg('errorNumber');
    let phones = document.getElementById('phones');
    let re = /^(8|\+7)\d{10}$/;
    let number;
    // one button add
    if (btn.parentElement.childElementCount !== 5) {
        number = btn.previousElementSibling.value;
        // two button delete and add
    } else {
        number = btn.previousElementSibling.previousElementSibling.value;
    }
    let isValid = re.test(number);
    if (isValid) {
        let newPhone = document.createElement('div');
        newPhone.classList.add("phone");
        newPhone.innerHTML = '' +
            '<input type="hidden" class="id" value="0">' +
            '<div class="form-group input-group">' +
            '<div class="input-group-prepend">' +
            '<span class="input-group-text"> <i class="fa fa-phone"></i> </span>' +
            '</div>' +
            '<select class="input-select" name="phone[type]">' +
            '<option value="личный">личный</option>' +
            '<option value="домашний">домашний</option>' +
            '<option value="рабочий">рабочий</option>' +
            '</select>' +
            '<input class="form-control" type="text" placeholder="Телефон" name="phone[number]">' +
            '<input type="button" value="-" onclick="delPhoneWhenUpdating(this)"/>' +
            '<input type="button" value="+" onclick="addPhoneWhenUpdating(this)"/>' +
            '</div>';
        phones.appendChild(newPhone);
        let prevPhone = newPhone.previousElementSibling;
        if (btn.parentElement.childElementCount !== 5) {
            prevPhone.lastElementChild.lastElementChild.remove();
        } else {
            prevPhone.lastElementChild.lastElementChild.remove();
            prevPhone.lastElementChild.lastElementChild.remove();
        }
        let delButton = document.createElement('input');
        delButton.type = "button";
        delButton.value = "-";
        delButton.setAttribute("onclick", "delPhoneWhenUpdating(this)");
        prevPhone.lastElementChild.appendChild(delButton);
    } else {
        createPhoneError(btn.parentElement.parentElement);
    }
}

// updateAccount.jsp
function delPhoneWhenUpdating(btn) {
    deleteErrorMsg('errorNumber');
    let currentPhone = btn.parentElement.parentElement;
    let prevPhone = currentPhone.previousElementSibling;
    let nextPhone = currentPhone.nextElementSibling;
    let phones = currentPhone.parentElement;
    let countItemsWithoutPhone = btn.parentElement.childElementCount;
    // one button delete
    if (countItemsWithoutPhone !== 5) {
        if (phones.childElementCount === 2 &&
            nextPhone.lastElementChild.lastElementChild.previousElementSibling.value === "") {
            nextPhone.lastElementChild.lastElementChild.previousElementSibling.remove();
        }
        currentPhone.remove();
        // two buttons delete and add
    } else if (phones.childElementCount === 1) {
        currentPhone.remove();
        let newPhone = document.createElement('div');
        newPhone.classList.add("phone");
        newPhone.innerHTML = '' +
            '<div class="form-group input-group">' +
            '<div class="input-group-prepend">' +
            '<span class="input-group-text"> <i class="fa fa-phone"></i> </span>' +
            '</div>' +
            '<select class="input-select" name="phone[type]">' +
            '<option value="личный">личный</option>' +
            '<option value="домашнмй">домашний</option>' +
            '<option value="рабочий">рабочий</option>' +
            '</select>' +
            '<input class="form-control" type="text" placeholder="Телефон" name="phone[number]">' +
            '<input type="button" value="+" onclick="addPhoneWhenUpdating(this)"/>' +
            '</div>';
        phones.appendChild(newPhone);
    } else {
        if (phones.childElementCount === 2 && prevPhone.lastElementChild.lastElementChild.previousElementSibling.value === "") {
            prevPhone.lastElementChild.lastElementChild.remove();
        }
        let addButton = document.createElement('input');
        addButton.type = "button";
        addButton.value = "+";
        addButton.setAttribute("onclick", "addPhoneWhenUpdating(this)");
        prevPhone.lastElementChild.appendChild(addButton);
        currentPhone.remove();
    }
}

// updateAccount.jsp
function validateFormWhenUpdating() {
    if (confirm('Сохранить изменения?')) {
        // delete an error message
        deleteAllErrorMessages();
        // numbering of phone fields
        let phones = document.getElementById('phones');
        let phoneIdInputs = phones.querySelectorAll("input.id");
        let phoneTypeInputs = phones.querySelectorAll("select.input-select");
        let phoneNumberInputs = phones.querySelectorAll("input.form-control");
        for (let i = 0; i < phoneNumberInputs.length; i++) {
            if (phoneNumberInputs[i].value !== '') {
                phoneIdInputs[i].name = "phones[" + i + "].id";
                phoneTypeInputs[i].name = "phones[" + i + "].type";
                phoneNumberInputs[i].name = "phones[" + i + "].number";
            }
        }
        // phone validation
        let isValidatePhone = validatePhone(phoneNumberInputs);
        // disable the date of birth field when not filled in
        if (isValidatePhone === true) {
            let birthday = document.getElementById('birthday');
            if (birthday.value === '') birthday.disabled = true;
        }
        return isValidatePhone;
    } else {
        return false
    }
}

function validatePhone(phoneNumberInputs) {
    let re = /^(8|\+7)\d{10}$/;
    for (let index = 0; index < phoneNumberInputs.length; index++) {
        if (phoneNumberInputs[index].value !== "") {
            if (!re.test(phoneNumberInputs[index].value)) {
                return createPhoneError(phoneNumberInputs[index].parentElement.parentElement);
            }
        }
    }
    return true;
}

function createPhoneError(phone) {
    let externalDiv = document.createElement('div');
    externalDiv.classList.add("form-group");
    externalDiv.classList.add("input-group");
    externalDiv.id = "errorNumber";
    phone.appendChild(externalDiv)
    let incorrectNumber = document.createElement('div');
    incorrectNumber.classList.add("alert");
    incorrectNumber.classList.add("alert-danger");
    incorrectNumber.innerHTML = "<p>Неправильно введен номер телефона</p>";
    externalDiv.appendChild(incorrectNumber);
    return false;
}

function deleteAllErrorMessages() {
    deleteErrorMsg('errorNumber');
    deleteErrorMsg('errorSizeAvatar');
    deleteErrorMsg('errorEmail');
}

function deleteErrorMsg(idElement) {
    let error = document.getElementById(idElement);
    if (error != null) {
        error.remove();
    }
}

//  friendshipsAccount.jsp, membershipsAccount.jsp, membersGroup.jsp
function openTab(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    if (evt) {
        evt.currentTarget.className += " active";
    }
}