package com.getjavajob.training.shabanovi.socialnetwork.service;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Membership;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MessageType;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.GroupDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MembershipDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Transactional(readOnly = true)
@Service
public class GroupService {

    private final GroupDao groupDao;
    private final MembershipDao membershipDao;
    private final MessageDao messageDao;

    @Autowired
    public GroupService(GroupDao groupDao, MembershipDao membershipDao, MessageDao messageDao) {
        this.groupDao = groupDao;
        this.membershipDao = membershipDao;
        this.messageDao = messageDao;
    }

    /* Group */

    @Transactional
    public Group createGroup(Group group) {
        group.setCreationDate(new Date(System.currentTimeMillis()));
        return groupDao.create(group);
    }

    public List<Group> getAllGroups(int numPage) {
        return groupDao.getAll(numPage);
    }

    public int getAllGroupsQty() {
        return groupDao.getAllGroupsQty();
    }

    public Group getGroupById(int groupId) {
        return groupDao.getById(groupId);
    }

    public Group getGroupByName(String name) {
        return groupDao.getByName(name);
    }

    public Group getGroupWithCreator(int groupId) {
        return groupDao.getWithCreatorById(groupId);
    }

    public List<Account> getOrders(int groupId) {
        return membershipDao.getRequests(groupId);
    }

    public List<Account> getUsers(int groupId) {
        return membershipDao.getUsers(groupId);
    }

    public List<Account> getModerators(int groupId) {
        return membershipDao.getModerators(groupId);
    }

    @Transactional
    public Group updateGroup(Group group) {
        return groupDao.update(group);
    }

    @Transactional
    public void deleteCreatedGroupByAccountId(int accountId) {
        List<Integer> createdGroupsId = groupDao.getCreatedGroupsIdByAccountId(accountId);
        for (int groupId : createdGroupsId) {
            groupDao.deleteById(groupId);
        }
    }

    @Transactional
    public void deleteGroupById(int groupId) {
        groupDao.deleteById(groupId);
    }

    public byte[] getImageById(int id) {
        return groupDao.getImageById(id);
    }

    public List<Group> searchGroupsWithPagination(String searchValue, int numPage) {
        return groupDao.searchByName(searchValue, numPage);
    }

    public List<Group> searchGroups(String searchValue) {
        return groupDao.searchByName(searchValue, 0);
    }

    public int getSearchGroupsQty(String searchValue) {
        return groupDao.getSearchGroupsQty(searchValue);
    }

    /* Memberships */

    @Transactional
    public boolean createMembershipRequest(int groupId, int accountId) {
        Membership membership = new Membership(accountId, groupId, false, false);
        membershipDao.create(membership);
        return true;
    }

    @Transactional
    public boolean acceptMembership(int groupId, int accountId) {
        Membership membership = membershipDao.getMembership(accountId, groupId);
        if (membership != null) {
            membership.setOrderApproved(true);
            membershipDao.create(membership);
            return true;
        }
        return false;
    }

    @Transactional
    public boolean deleteMembership(int groupId, int accountId) {
        return membershipDao.deleteMembership(accountId, groupId) != 0;
    }

    @Transactional
    public boolean assignCreator(int accountId, int creatorId, int groupId) {
        membershipDao.deleteMembership(accountId, groupId);
        Membership membership = new Membership(creatorId, groupId, true, true);
        membershipDao.create(membership);
        return groupDao.updateCreator(accountId, groupId) == 1;
    }

    @Transactional
    public boolean assignModerator(int groupId, int accountId) {
        return membershipDao.updateModeratorStatus(accountId, groupId, MembershipStatus.MODERATOR) != 0;
    }

    @Transactional
    public boolean downgradeToUser(int groupId, int accountId) {
        return membershipDao.updateModeratorStatus(accountId, groupId, MembershipStatus.USER) != 0;
    }

    /* Message */

    public List<Message> getWallMessages(int groupId) {
        return messageDao.getGroupWallMessages(groupId);
    }

    @Transactional
    public Message createGroupPost(Message message) {
        message.setDate(new Timestamp(System.currentTimeMillis()));
        message.setType(MessageType.GROUP_POST.getStatus());
        return messageDao.create(message);
    }

    @Transactional
    public void deleteGroupPost(int messageId) {
        messageDao.deleteById(messageId);
    }

}