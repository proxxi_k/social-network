package com.getjavajob.training.shabanovi.socialnetwork.service;

import com.getjavajob.training.shabanovi.socialnetwork.common.News;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Friendship;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Membership;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Phone;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MessageType;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AccountDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AuthDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.FriendshipDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.GroupDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MembershipDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MessageDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.NewsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Transactional(readOnly = true)
@Service
public class AccountService {

    private final AccountDao accountDao;
    private final AuthDao authDao;
    private final FriendshipDao friendshipDao;
    private final MembershipDao membershipDao;
    private final GroupDao groupDao;
    private final MessageDao messageDao;
    private final NewsDao newsDao;

    @Autowired
    public AccountService(AccountDao accountDao, AuthDao authDao, FriendshipDao friendshipDao,
                          MembershipDao membershipDao, GroupDao groupDao, MessageDao messageDao, NewsDao newsDao) {
        this.accountDao = accountDao;
        this.authDao = authDao;
        this.friendshipDao = friendshipDao;
        this.membershipDao = membershipDao;
        this.groupDao = groupDao;
        this.messageDao = messageDao;
        this.newsDao = newsDao;
    }

    /* Account */

    @Transactional
    public Account createAccount(String email, String encryptPassword, Account account) {
        if (account.getName() == null) {
            account.setName("Неизвестный");
        }
        account.setRegistrationDate(new Date(System.currentTimeMillis()));
        Auth auth = new Auth(account.getId(), email, encryptPassword);
        account.setAuth(auth);
        auth.setAccount(account);
        return accountDao.create(account);
    }

    public Account getAccountById(int accountId) {
        return accountDao.getById(accountId);
    }

    public byte[] getAvatarById(int id) {
        return accountDao.getAvatarById(id);
    }

    public List<Account> getAllAccounts(int numPage) {
        return accountDao.getAll(numPage);
    }

    public int getAllAccountsQty() {
        return accountDao.getAllAccountsQty();
    }

    @Transactional
    public Account updateAccount(Account account, Account updatedAccount) {
        if (account.getPhones() != null) {
            updatedAccount.getPhones().clear();
            for (Phone phone : account.getPhones()) {
                phone.setAccount(updatedAccount);
                updatedAccount.getPhones().add(phone);
            }
        } else {
            updatedAccount.setPhones(Collections.emptyList());
        }
        updatedAccount.setName(account.getName());
        updatedAccount.setSurname(account.getSurname());
        updatedAccount.setPatronymic(account.getPatronymic());
        updatedAccount.setBirthday(account.getBirthday());
        updatedAccount.setHomeAddress(account.getHomeAddress());
        updatedAccount.setWorkAddress(account.getWorkAddress());
        updatedAccount.setSkype(account.getSkype());
        updatedAccount.setAdditionalInfo(account.getAdditionalInfo());
        return accountDao.update(updatedAccount);
    }

    @Transactional
    public void deleteAccountById(int accountId) {
        friendshipDao.deleteFriendshipsByAccountId(accountId);
        messageDao.deleteAllMessagesByAccountId(accountId);
        accountDao.deleteById(accountId);
    }

    public boolean checkedCreatedGroups(int accountId) {
        return groupDao.getCountCreatedGroups(accountId) != 0;
    }

    public Account getAccountByEmail(String email) {
        return accountDao.getByEmail(email);
    }

    public Account getAccountWithPhonesById(int accountId) {
        return accountDao.getWithPhonesById(accountId);
    }

    public List<Account> searchAccountsWithPagination(String searchValue, int numPage) {
        return accountDao.searchBySurnameOrName(searchValue, numPage);
    }

    public List<Account> searchAccounts(String searchValue) {
        return accountDao.searchBySurnameOrName(searchValue, 0);
    }

    public int getSearchAccountsQty(String searchValue) {
        return accountDao.getSearchAccountsQty(searchValue);
    }

    /* Auth */

    public String getEmailByAccountId(int accountId) {
        return authDao.getEmailByAccountId(accountId);
    }

    public Auth getAuthByEmail(String email) {
        return authDao.getByEmail(email);
    }

    @Transactional
    public Auth updateAuth(Auth auth) {
        return authDao.update(auth);
    }

    @Transactional
    public Auth updateAuthEmailAndPassword(Auth auth, String newEmail, String newEncodePassword) {
        auth.setEmail(newEmail);
        auth.setPassword(newEncodePassword);
        return updateAuth(auth);
    }

    /* Friendships */

    public int getFriendsQty(int accountId) {
        return friendshipDao.getFriendsQty(accountId);
    }

    public int getRequestsFromQty(int accountId) {
        return friendshipDao.getRequestsFromQty(accountId);
    }

    public int getRequestsToQty(int accountId) {
        return friendshipDao.getRequestsToQty(accountId);
    }

    public int getBlacklistsQty(int accountId) {
        return friendshipDao.getBlacklistsQty(accountId);
    }

    public List<Integer> getFriendsId(int accountId) {
        return friendshipDao.getFriendsId(accountId);
    }

    public FriendStatus getFriendshipStatus(int accountId, int friendId) {
        Friendship friendship = friendshipDao.getFriendship(accountId, friendId);
        if (friendship != null) {
            return friendship.getStatus() == -1 ? FriendStatus.REQUEST_TO :
                    friendship.getStatus() == 0 ? FriendStatus.BLACKLIST : friendship.getStatus() == 1 ?
                            FriendStatus.REQUEST_FROM : FriendStatus.FRIEND;
        } else {
            friendship = friendshipDao.getFriendship(friendId, accountId);
            return friendship == null ? null : FriendStatus.SOMEONE_BLACKLIST;
        }
    }

    public List<Account> getFriendsWithPagination(int accountId, int numPage) {
        return friendshipDao.getFriends(accountId, numPage);
    }

    public List<Account> getRequestsFromWithPagination(int accountId, int numPage) {
        return friendshipDao.getRequestsFrom(accountId, numPage);
    }

    public List<Account> getRequestsToWithPagination(int accountId, int numPage) {
        return friendshipDao.getRequestsTo(accountId, numPage);
    }

    public List<Account> getBlacklistsWithPagination(int accountId, int numPage) {
        return friendshipDao.getBlacklists(accountId, numPage);
    }

    @Transactional
    public void createFriendshipRequest(int accountId, int friendId) {
        if (friendshipDao.getFriendship(accountId, friendId) == null) {
            Friendship friendship1 = new Friendship(accountId, friendId, FriendStatus.REQUEST_FROM.getStatus());
            Friendship friendship2 = new Friendship(friendId, accountId, FriendStatus.REQUEST_TO.getStatus());
            friendshipDao.create(friendship1);
            friendshipDao.create(friendship2);
        }
    }

    @Transactional
    public boolean acceptFriendship(int accountId, int friendId) {
        Friendship friendship = friendshipDao.getFriendship(accountId, friendId);
        if (friendship != null && friendship.getStatus() != 0) {
            return friendshipDao.updateStatus(accountId, friendId, FriendStatus.FRIEND) != 0;
        }
        return false;
    }

    @Transactional
    public void addToBlacklist(int accountId, int friendId) {
        Friendship primaryFriendship = friendshipDao.getFriendship(accountId, friendId);
        Friendship secondaryFriendship = friendshipDao.getFriendship(friendId, accountId);
        if (primaryFriendship != null && secondaryFriendship != null) {
            friendshipDao.deleteFriendship(accountId, friendId);
            friendshipDao.create(new Friendship(accountId, friendId, FriendStatus.BLACKLIST.getStatus()));
        } else if (primaryFriendship == null && secondaryFriendship == null) {
            friendshipDao.create(new Friendship(accountId, friendId, FriendStatus.BLACKLIST.getStatus()));
        }
    }

    @Transactional
    public boolean deleteFriendship(int accountId, int friendId) {
        Friendship friendship = friendshipDao.getFriendship(accountId, friendId);
        if (friendship != null) {
            return friendshipDao.deleteFriendship(accountId, friendId) != 0;
        }
        return false;
    }

    /* Memberships */

    public MembershipStatus getMembershipStatus(int accountId, int groupId) {
        Membership membership = membershipDao.getMembership(accountId, groupId);
        if (membership != null) {
            return membership.getModerator() ? MembershipStatus.MODERATOR : membership.getOrderApproved() ?
                    MembershipStatus.USER : MembershipStatus.REQUEST;
        } else {
            Group group = groupDao.getById(groupId);
            return group.getCreator().getId() == accountId ? MembershipStatus.CREATOR : null;
        }
    }

    public List<Group> getRequestGroups(int accountId) {
        return membershipDao.getRequestGroups(accountId);
    }

    public List<Group> getUserGroups(int accountId) {
        return membershipDao.getUserGroups(accountId);
    }

    public List<Group> getModeratorGroups(int accountId) {
        return membershipDao.getModeratorGroups(accountId);
    }

    public List<Group> getCreatedGroups(int accountId) {
        return groupDao.getCreatedGroups(accountId);
    }

    /* Message */

    @Transactional
    public Message sendAccountPost(Message message) {
        return createMassage(message, MessageType.ACCOUNT_POST);
    }

    @Transactional
    public Message sendPersonalMessage(Message message) {
        return createMassage(message, MessageType.PERSONAL);
    }

    public List<Message> getWallMessages(int accountId) {
        return messageDao.getAccountWallMessages(accountId);
    }

    public List<Message> getChat(int accountId, int interlocutorId) {
        return messageDao.getPersonalMessages(accountId, interlocutorId);
    }

    public List<Account> getInterlocutors(int accountId) {
        return messageDao.getAccountsInterlocutors(accountId);
    }

    @Transactional
    public void deleteMessage(int messageId) {
        messageDao.deleteById(messageId);
    }

    @Transactional
    public boolean deleteChat(int accountId, int interlocutorId) {
        return messageDao.deletePersonalMessages(accountId, interlocutorId) != 0;
    }

    private Message createMassage(Message message, MessageType messageType) {
        message.setDate(new Timestamp(System.currentTimeMillis()));
        message.setType(messageType.getStatus());
        return messageDao.create(message);
    }

    /* News */

    public List<News> getNews(int accountId, int numPage) {
        return newsDao.getByAccountId(accountId, numPage);
    }

    public List<News> getNews(Collection<Integer> newsId) {
        return newsDao.getByCollectionNewsId(newsId);
    }

    public Long getNewsQty(int accountId) {
        return newsDao.getByAccountIdQty(accountId);
    }

    public List<News> getNewsCache(int accountId, int sizeCache) {
        return newsDao.getCacheByAccountId(accountId, sizeCache);
    }

}