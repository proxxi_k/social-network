package com.getjavajob.training.shabanovi.socialnetwork.service;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class AccountDetailService implements UserDetailsService {

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Auth auth = accountService.getAuthByEmail(username);
        if (auth == null) {
            throw new UsernameNotFoundException(String.format("Account with email '%s' not found", username));
        }
        return new User(auth.getEmail(), auth.getPassword(),
                Collections.singleton(new SimpleGrantedAuthority(auth.getAdmin() ? "ROLE_ADMIN" : "ROLE_USER")));
    }

}