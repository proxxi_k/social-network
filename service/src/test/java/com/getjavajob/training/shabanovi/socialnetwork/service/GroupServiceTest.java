package com.getjavajob.training.shabanovi.socialnetwork.service;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Group;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Membership;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MembershipStatus;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.GroupDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MembershipDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MessageDao;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GroupServiceTest {

    @Mock
    GroupDao groupDao;
    @Mock
    MembershipDao membershipDao;
    @Mock
    MessageDao messageDao;
    @InjectMocks
    private GroupService groupService;

    @Test
    public void testCreateGroup_returnCreatedGroup() {
        Group group = new Group("name", "description", Date.valueOf("2021-10-01"));
        when(groupDao.create(group)).thenReturn(group);
        Group actual = groupService.createGroup(group);
        assertEquals(group, actual);
        verify(groupDao, times(1)).create(group);
        verify(groupDao, only()).create(group);
    }

    @Test
    public void testGetAllGroups_returnListAllGroups() {
        Group group1 = new Group("name1", "description1", Date.valueOf("2021-10-01"));
        Group group2 = new Group("name2", "description2", Date.valueOf("2021-10-01"));
        List<Group> groups = new ArrayList<>(Arrays.asList(group1, group2));
        when(groupDao.getAll(1)).thenReturn(groups);
        List<Group> actual = groupService.getAllGroups(1);
        assertEquals(groups, actual);
    }

    @Test
    public void testGetAllGroups_missingGroups_returnEmptyList() {
        when(groupDao.getAll(1)).thenReturn(new ArrayList<>());
        assertTrue(groupService.getAllGroups(1).isEmpty());
    }

    @Test
    public void testSearchGroups_returnListOfFoundGroups() {
        Group group1 = new Group("name1", "description1", Date.valueOf("2021-10-01"));
        Group group2 = new Group("name2", "description2", Date.valueOf("2021-10-01"));
        List<Group> groups = new ArrayList<>(Arrays.asList(group1, group2));
        when(groupDao.searchByName("name", 0)).thenReturn(groups);
        List<Group> actual = groupService.searchGroupsWithPagination("name", 0);
        assertEquals(groups, actual);
    }

    @Test
    public void testSearchGroups_missingSearchingResult_returnEmptyList() {
        when(groupDao.searchByName("а", 0)).thenReturn(new ArrayList<>());
        assertTrue(groupService.searchGroupsWithPagination("а", 0).isEmpty());
    }

    @Test
    public void testGetGroupById_returnGroup() {
        Group group = new Group("name", "description", Date.valueOf("2021-10-01"));
        when(groupDao.getById(1)).thenReturn(group);
        Group actual = groupService.getGroupById(1);
        assertEquals(group, actual);
    }

    @Test
    public void testGetGroupById_missingId_returnNull() {
        when(groupDao.getById(1)).thenReturn(null);
        Group actual = groupService.getGroupById(1);
        assertNull(actual);
    }

    @Test
    public void testGetGroupByName_returnGroup() {
        Group group = new Group("name", "description", Date.valueOf("2021-10-01"));
        when(groupDao.getByName("name")).thenReturn(group);
        Group actual = groupService.getGroupByName("name");
        assertEquals(group, actual);
    }

    @Test
    public void testGetGroupByName_missingName_returnNull() {
        when(groupDao.getByName("name")).thenReturn(null);
        Group actual = groupService.getGroupByName("name");
        assertNull(actual);
    }

    @Test
    public void testUpdateGroup_returnTrue() {
        Group group = new Group("newName", "newDescription", Date.valueOf("2021-10-01"));
        when(groupDao.update(group)).thenReturn(group);
        assertEquals(groupService.updateGroup(group), group);
    }

    @Test
    public void testCreateMembershipRequest_returnTrue() {
        Membership membership = new Membership(1, 2, false, false);
        when(membershipDao.create(membership)).thenReturn(membership);
        assertTrue(groupService.createMembershipRequest(2, 1));
    }

    @Test
    public void testAcceptMembership_existingMembership_ReturnTrue() {
        Membership membership = new Membership(1, 2, false, false);
        when(membershipDao.getMembership(1, 2)).thenReturn(membership);
        when(membershipDao.create(membership)).thenReturn(membership);
        assertTrue(groupService.acceptMembership(2, 1));
    }

    @Test
    public void testAcceptMembership_MissingMembership_ReturnFalse() {
        when(membershipDao.getMembership(1, 2)).thenReturn(null);
        assertFalse(groupService.acceptMembership(2, 1));
    }

    @Test
    public void testDeleteMembership_existingMembership_returnTrue() {
        when(membershipDao.deleteMembership(1, 2)).thenReturn(1);
        assertTrue(groupService.deleteMembership(2, 1));
    }

    @Test
    public void testDeleteMembership_missingMembership_returnFalse() {
        when(membershipDao.deleteMembership(1, 2)).thenReturn(0);
        assertFalse(groupService.deleteMembership(2, 1));
    }

    @Test
    public void testAssignModerator_existingMembership_returnTrue() {
        when(membershipDao.updateModeratorStatus(1, 2, MembershipStatus.MODERATOR)).thenReturn(1);
        assertTrue(groupService.assignModerator(2, 1));
    }

    @Test
    public void testAssignModerator_missingMembership_returnFalse() {
        when(membershipDao.updateModeratorStatus(1, 2, MembershipStatus.MODERATOR)).thenReturn(0);
        assertFalse(groupService.assignModerator(2, 1));
    }

    @Test
    public void testDowngradeToUser_existingMembership_returnTrue() {
        when(membershipDao.updateModeratorStatus(1, 2, MembershipStatus.USER)).thenReturn(1);
        assertTrue(groupService.downgradeToUser(2, 1));
    }

    @Test
    public void testDowngradeToUser_missingMembership_returnFalse() {
        when(membershipDao.updateModeratorStatus(1, 2, MembershipStatus.USER)).thenReturn(0);
        assertFalse(groupService.downgradeToUser(2, 1));
    }

    @Test
    public void testCreateGroupPost_returnTrue() {
        Message groupPost = new Message(1, null, 2, "groupPost", Timestamp.valueOf("2021-01-01 00:00:00"), 2);
        when(messageDao.create(groupPost)).thenReturn(groupPost);
        assertEquals(groupPost, groupService.createGroupPost(groupPost));
    }

    @Test
    public void testDeleteGroupPost() {
        groupService.deleteGroupPost(1);
        verify(messageDao, times(1)).deleteById(1);
        verify(messageDao, only()).deleteById(1);
    }

}