package com.getjavajob.training.shabanovi.socialnetwork.service;

import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Account;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Auth;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Friendship;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Message;
import com.getjavajob.training.shabanovi.socialnetwork.common.entity.Phone;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.FriendStatus;
import com.getjavajob.training.shabanovi.socialnetwork.common.enums.MessageType;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AccountDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.AuthDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.FriendshipDao;
import com.getjavajob.training.shabanovi.socialnetwork.dao.interfaces.MessageDao;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @Mock
    AccountDao accountDao;
    @Mock
    AuthDao authDao;
    @Mock
    FriendshipDao friendshipDao;
    @Mock
    MessageDao messageDao;
    @InjectMocks
    private AccountService accountService;

    @Test
    public void testCreateAccount_createAccountWithPhones() {
        Account account = new Account(1, "name", "surname", Date.valueOf("2021-10-01"));
        Phone phone1 = new Phone(1, "личный", "+7(999)000-00-00");
        Phone phone2 = new Phone(2, "рабочий", "+7(999)111-11-11");
        List<Phone> phones = new ArrayList<>(Arrays.asList(phone1, phone2));
        account.setPhones(phones);
        when(accountDao.create(account)).thenReturn(account);
        Account actual = accountService.createAccount("email@mail.ru", "password", account);
        assertEquals(account, actual);
        assertEquals(phones, actual.getPhones());
        verify(accountDao, times(1)).create(account);
        verify(accountDao, only()).create(account);
    }

    @Test
    public void testGetAccountById_returnAccountWithPhones() {
        Account account = new Account(1, "name", "surname", Date.valueOf("2021-10-01"));
        Phone phone1 = new Phone(1, "личный", "+7(999)000-00-00");
        Phone phone2 = new Phone(2, "рабочий", "+7(999)111-11-11");
        List<Phone> phones = new ArrayList<>(Arrays.asList(phone1, phone2));
        account.setPhones(phones);
        when(accountDao.getById(1)).thenReturn(account);
        Account actual = accountService.getAccountById(1);
        assertEquals(account, actual);
        assertEquals(account.getPhones(), actual.getPhones());
        verify(accountDao, times(1)).getById(1);
        verify(accountDao, only()).getById(1);
    }

    @Test
    public void testGetAccountById_returnAccountWithoutPhones() {
        Account account = new Account(1, "name", "surname", Date.valueOf("2021-10-01"));
        when(accountDao.getById(1)).thenReturn(account);
        Account actual = accountService.getAccountById(1);
        assertEquals(account, actual);
        assertNull(actual.getPhones());
        verify(accountDao, times(1)).getById(1);
        verify(accountDao, only()).getById(1);
    }

    @Test
    public void testGetAccountById_missingAccount_returnNull() {
        when(accountDao.getById(1)).thenReturn(null);
        Account actual = accountService.getAccountById(1);
        assertNull(actual);
        verify(accountDao, times(1)).getById(1);
        verify(accountDao, only()).getById(1);
    }

    @Test
    public void testGetAccountByEmail_existingEmail_returnAccount() {
        Account account = new Account(1, "Иван", "Иванов", Date.valueOf("2021-01-01"));
        when(accountDao.getByEmail("ivanov@yandex.ru")).thenReturn(account);
        Account actual = accountService.getAccountByEmail("ivanov@yandex.ru");
        assertEquals(account, actual);
        verify(accountDao, times(1)).getByEmail("ivanov@yandex.ru");
        verify(accountDao, only()).getByEmail("ivanov@yandex.ru");
    }

    @Test
    public void testGetAccountByEmail_missingEmail_returnNull() {
        when(accountDao.getByEmail("unknown@yandex.ru")).thenReturn(null);
        Account actual = accountService.getAccountByEmail("unknown@yandex.ru");
        assertNull(actual);
        verify(accountDao, times(1)).getByEmail("unknown@yandex.ru");
        verify(accountDao, only()).getByEmail("unknown@yandex.ru");
    }

    @Test
    public void testGetAccountWithPhonesById_existingAccountWithPhones_returnAccountAndPhones() {
        Account account = new Account(3, "Сергей", "Сергеев", Date.valueOf("2021-03-01"));
        Phone phone1 = new Phone(3, "личный", "+79993333333");
        Phone phone2 = new Phone(4, "рабочий", "+79994444444");
        Phone phone3 = new Phone(5, "домашний", "+79995555555");
        List<Phone> phones = new ArrayList<>(Arrays.asList(phone1, phone2, phone3));
        account.setPhones(phones);
        when(accountDao.getWithPhonesById(3)).thenReturn(account);
        Account actual = accountService.getAccountWithPhonesById(3);
        assertEquals(account, actual);
        assertEquals(phones, actual.getPhones());
        verify(accountDao, times(1)).getWithPhonesById(3);
        verify(accountDao, only()).getWithPhonesById(3);
    }

    @Test
    public void testGetAccountWithPhonesById_existingAccountWithoutPhones_returnAccountAndEmptyListPhones() {
        Account account = new Account(9, "Неизвестный", Date.valueOf("2021-09-01"));
        account.setPhones(new ArrayList<>());
        when(accountDao.getWithPhonesById(9)).thenReturn(account);
        Account actual = accountService.getAccountWithPhonesById(9);
        assertEquals(account, actual);
        assertTrue(actual.getPhones().isEmpty());
        verify(accountDao, times(1)).getWithPhonesById(9);
        verify(accountDao, only()).getWithPhonesById(9);
    }

    @Test
    public void testGetAccountWithPhonesById_missingAccount_returnNull() {
        when(accountDao.getWithPhonesById(10)).thenReturn(null);
        assertNull(accountService.getAccountWithPhonesById(10));
        verify(accountDao, times(1)).getWithPhonesById(10);
        verify(accountDao, only()).getWithPhonesById(10);
    }

    @Test
    public void testGetAllAccounts_existingAccounts_returnListAccounts() {
        List<Account> listAccounts = new ArrayList<>(Arrays.asList(
                new Account(9, "Неизвестный", Date.valueOf("2021-09-01")),
                new Account(5, "Александр", "Александров", Date.valueOf("2021-05-01")),
                new Account(4, "Владимир", "Банкетный", "Анатольевич",
                        Date.valueOf("1999-01-01"), "home address", "home address", "banketnyj", "additional info",
                        Date.valueOf("2021-04-01")),
                new Account(7, "Валера", "Валеров", Date.valueOf("2021-07-01")),
                new Account(1, "Иван", "Иванов", Date.valueOf("2021-01-01"))));
        when(accountDao.getAll(1)).thenReturn(listAccounts);
        assertEquals(listAccounts, accountService.getAllAccounts(1));
    }

    @Test
    public void testGetAllAccounts_missingAccounts_returnEmptyList() {
        when(accountDao.getAll(1)).thenReturn(new ArrayList<>());
        assertTrue(accountService.getAllAccounts(1).isEmpty());
    }

    @Test
    public void testUpdateAccount_updateAccountAndPhoneAndAddNewPhone_ReturnUpdatedAccountAndPhones() {
        Account account = new Account(1, "newName", "newSurname", Date.valueOf("2021-10-01"));
        Phone updatePhone = new Phone(3, "personal", "+7(999)000-00-00");
        Phone newPhone = new Phone(3, "personal", "+7(999)999-99-99");
        List<Phone> phones = new ArrayList<>(Arrays.asList(updatePhone, newPhone));
        account.setPhones(phones);
        when(accountDao.update(account)).thenReturn(account);
        Account actual = accountService.updateAccount(account, account);
        assertEquals(account, actual);
        assertEquals(phones, actual.getPhones());
        verify(accountDao, times(1)).update(account);
        verify(accountDao, only()).update(account);
    }

    @Test
    public void testDeleteAccountById_existingAccountWithPhonesAndFriendships_ReturnTrue() {
        when(friendshipDao.deleteFriendshipsByAccountId(3)).thenReturn(11);
        when(messageDao.deleteAllMessagesByAccountId(3)).thenReturn(2);
        accountService.deleteAccountById(3);
        verify(accountDao, times(1)).deleteById(3);
        verify(accountDao, only()).deleteById(3);
        verify(friendshipDao, times(1)).deleteFriendshipsByAccountId(3);
        verify(friendshipDao, only()).deleteFriendshipsByAccountId(3);
        verify(messageDao, times(1)).deleteAllMessagesByAccountId(3);
        verify(messageDao, only()).deleteAllMessagesByAccountId(3);
    }

    @Test
    public void testDeleteAccountById_missingAccount() {
        when(friendshipDao.deleteFriendshipsByAccountId(9)).thenReturn(0);
        when(messageDao.deleteAllMessagesByAccountId(9)).thenReturn(0);
        accountService.deleteAccountById(9);
        verify(accountDao, times(1)).deleteById(9);
        verify(accountDao, only()).deleteById(9);
        verify(friendshipDao, times(1)).deleteFriendshipsByAccountId(9);
        verify(friendshipDao, only()).deleteFriendshipsByAccountId(9);
        verify(messageDao, times(1)).deleteAllMessagesByAccountId(9);
        verify(messageDao, only()).deleteAllMessagesByAccountId(9);
    }

    @Test
    public void testSearchAccountsWithPagination_returnListOfFoundAccounts() {
        Account account1 = new Account(1, "name1", "surname1", Date.valueOf("2021-10-01"));
        Account account2 = new Account(2, "name2", "surname2", Date.valueOf("2021-10-01"));
        List<Account> accounts = new ArrayList<>(Arrays.asList(account1, account2));
        when(accountDao.searchBySurnameOrName("name", 1)).thenReturn(accounts);
        List<Account> actual = accountService.searchAccountsWithPagination("name", 1);
        assertEquals(accounts, actual);
    }

    @Test
    public void testSearchAccountsWithPagination_missingSearchingResult_returnEmptyList() {
        when(accountDao.searchBySurnameOrName("a", 1)).thenReturn(new ArrayList<>());
        assertTrue(accountService.searchAccountsWithPagination("a", 1).isEmpty());
    }

    @Test
    public void testSearchAccounts_returnListOfFoundAccounts() {
        Account account1 = new Account(1, "name1", "surname1", Date.valueOf("2021-10-01"));
        Account account2 = new Account(2, "name2", "surname2", Date.valueOf("2021-10-01"));
        List<Account> accounts = new ArrayList<>(Arrays.asList(account1, account2));
        when(accountDao.searchBySurnameOrName("name", 0)).thenReturn(accounts);
        List<Account> actual = accountService.searchAccounts("name");
        assertEquals(accounts, actual);
    }

    @Test
    public void testSearchAccounts_missingSearchingResult_returnEmptyList() {
        when(accountDao.searchBySurnameOrName("a", 0)).thenReturn(new ArrayList<>());
        assertTrue(accountService.searchAccounts("a").isEmpty());
    }

    @Test
    public void testGetAuthByEmail_returnAuth() {
        Auth auth = new Auth(1, "test@mail.ru", "1");
        when(authDao.getByEmail("test@mail.ru")).thenReturn(auth);
        Auth actual = accountService.getAuthByEmail("test@mail.ru");
        assertEquals(auth, actual);
        verify(authDao, times(1)).getByEmail("test@mail.ru");
        verify(authDao, only()).getByEmail("test@mail.ru");
    }

    @Test
    public void testGetAuthByEmail_missingId_returnNull() {
        when(authDao.getByEmail("test@mail.ru")).thenReturn(null);
        Auth actual = accountService.getAuthByEmail("test@mail.ru");
        assertNull(actual);
        verify(authDao, times(1)).getByEmail("test@mail.ru");
        verify(authDao, only()).getByEmail("test@mail.ru");
    }

    @Test
    public void testUpdateAuth_returnTrue() {
        Auth auth = new Auth(1, "newTest@mail.ru", "newPassword");
        when(authDao.update(auth)).thenReturn(auth);
        assertEquals(auth, accountService.updateAuth(auth));
        verify(authDao, times(1)).update(auth);
        verify(authDao, only()).update(auth);
    }

    @Test
    public void testCreateFriendshipRequest_missingFriendship_returnTrue() {
        Friendship friendship1 = new Friendship(1, 2, FriendStatus.REQUEST_FROM.getStatus());
        Friendship friendship2 = new Friendship(2, 1, FriendStatus.REQUEST_TO.getStatus());
        when(friendshipDao.create(friendship1)).thenReturn(friendship1);
        when(friendshipDao.create(friendship2)).thenReturn(friendship2);
        accountService.createFriendshipRequest(1, 2);
        verify(friendshipDao, times(1)).create(friendship1);
        verify(friendshipDao, times(1)).create(friendship2);
    }

    @Test
    public void testCreateFriendshipRequest_existingFriendship_returnFalse() {
        Friendship friendship = new Friendship(1, 2, FriendStatus.REQUEST_FROM.getStatus());
        when(friendshipDao.create(friendship)).thenReturn(null);
        accountService.createFriendshipRequest(1, 2);
        verify(friendshipDao, times(1)).create(friendship);
    }


    @Test
    public void testAcceptFriendship_existingOrders_returnTrue() {
        Friendship friendship = new Friendship(2, 1, 1);
        when(friendshipDao.getFriendship(2, 1)).thenReturn(friendship);
        when(friendshipDao.updateStatus(2, 1, FriendStatus.FRIEND)).thenReturn(1);
        assertTrue(accountService.acceptFriendship(2, 1));
    }

    @Test
    public void testAcceptFriendship_missingOrders_returnFalse() {
        when(friendshipDao.getFriendship(1, 2)).thenReturn(null);
        assertFalse(accountService.acceptFriendship(1, 2));
    }

    @Test
    public void testAcceptFriendship_friendInBlacklist_returnFalse() {
        Friendship friendship = new Friendship(1, 2, 0, 1);
        when(friendshipDao.getFriendship(2, 1)).thenReturn(friendship);
        assertFalse(accountService.acceptFriendship(2, 1));
    }

    @Test
    public void testAddToBlacklist_existingFriendship_returnTrue() {
        Friendship friendship1 = new Friendship(1, 2, FriendStatus.REQUEST_FROM.getStatus());
        Friendship friendship2 = new Friendship(2, 1, FriendStatus.REQUEST_TO.getStatus());
        when(friendshipDao.getFriendship(1, 2)).thenReturn(friendship1);
        when(friendshipDao.getFriendship(2, 1)).thenReturn(friendship2);
        when(friendshipDao.deleteFriendship(1, 2)).thenReturn(1);
        accountService.addToBlacklist(1, 2);
    }

    @Test
    public void testAddToBlacklist_missingFriendship_returnTrue() {
        when(friendshipDao.getFriendship(1, 2)).thenReturn(null);
        when(friendshipDao.getFriendship(2, 1)).thenReturn(null);
        accountService.addToBlacklist(1, 2);
    }

    @Test
    public void testDeleteFriendship_existingOrders_returnTrue() {
        Friendship friendship = new Friendship(1, 2, 1, 1);
        when(friendshipDao.getFriendship(2, 1)).thenReturn(friendship);
        when(friendshipDao.deleteFriendship(2, 1)).thenReturn(1);
        assertTrue(accountService.deleteFriendship(2, 1));
    }

    @Test
    public void testDeleteFriendship_missingOrders_returnFalse() {
        when(friendshipDao.getFriendship(1, 2)).thenReturn(null);
        assertFalse(accountService.deleteFriendship(1, 2));
    }

    @Test
    public void testCreateAccountPost_returnTrue() {
        Message accountPost = new Message(1, 2, null, "accountPost", Timestamp.valueOf("2021-01-01 00:00:00"),
                MessageType.ACCOUNT_POST.getStatus());
        when(messageDao.create(accountPost)).thenReturn(accountPost);
        assertEquals(accountPost, accountService.sendAccountPost(accountPost));
    }

    @Test
    public void testSendPersonalMessage_returnTrue() {
        Message personalMessage = new Message(1, 2, null, "personalMessage", Timestamp.valueOf("2021-01-01 " +
                "00:00:00"),
                MessageType.PERSONAL.getStatus());
        when(messageDao.create(personalMessage)).thenReturn(personalMessage);
        assertEquals(personalMessage, accountService.sendPersonalMessage(personalMessage));
    }

    @Test
    public void testGetChat_returnListMessages() {
        Message message1 = new Message(1, 2, null, "message1", Timestamp.valueOf("2021-01-01 00:00:00"), 0);
        Message message2 = new Message(2, 1, null, "message2", Timestamp.valueOf("2021-01-01 01:00:00"), 0);
        List<Message> chats = new ArrayList<>(Arrays.asList(message1, message2));
        when(messageDao.getPersonalMessages(1, 2)).thenReturn(chats);
        List<Message> actual = accountService.getChat(1, 2);
        assertEquals(chats, actual);
    }

    @Test
    public void testGetChat_missingChat_returnEmptyListMessages() {
        when(messageDao.getPersonalMessages(1, 2)).thenReturn(new ArrayList<>());
        List<Message> actual = accountService.getChat(1, 2);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testGetInterlocutors_returnListAccountInterlocutors() {
        Account interlocutor = new Account(1, "interlocutor", "interlocutor", Date.valueOf("2021-10-01"));
        List<Account> interlocutors = new ArrayList<>(Collections.singletonList(interlocutor));
        when(messageDao.getAccountsInterlocutors(2)).thenReturn(interlocutors);
        List<Account> actual = accountService.getInterlocutors(2);
        assertEquals(interlocutors, actual);
    }

    @Test
    public void testGetInterlocutors_missingInterlocutors_returnEmptyListAccountInterlocutors() {
        when(messageDao.getAccountsInterlocutors(2)).thenReturn(new ArrayList<>());
        List<Account> actual = accountService.getInterlocutors(2);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testDeleteMessage() {
        accountService.deleteMessage(1);
        verify(messageDao, times(1)).deleteById(1);
        verify(messageDao, only()).deleteById(1);
    }

    @Test
    public void testDeleteChat_existingChat_returnNumberOfDeletedMassages() {
        when(messageDao.deletePersonalMessages(1, 2)).thenReturn(5);
        assertTrue(accountService.deleteChat(1, 2));
    }

    @Test
    public void testDeleteChat_missingMessage_returnZero() {
        when(messageDao.deletePersonalMessages(1, 2)).thenReturn(0);
        assertFalse(accountService.deleteChat(1, 2));
    }

}